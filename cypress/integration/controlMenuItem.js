describe('Control Menu Item', function () {
  before(() => {
    cy.clearLocalStorageSnapshot()
    cy.clearLocalStorage()
    cy.signup()
    cy.saveLocalStorage()
  })
  beforeEach(() => {
    cy.restoreLocalStorage()
  })
  afterEach(() => {
    cy.saveLocalStorage()
  })
  it('should show/hide and delete with confirmation', function () {
    const itemBlock = cy.newMenuItem('Test Menu Item')
    itemBlock
      .then((itemBlock) => {
        cy.get(itemBlock).should('exist')
        cy.get(itemBlock).find('.icon-Eye').should('not.exist')
        cy.get(itemBlock).find('.icon-EyeCrossed').click()
        cy.get(itemBlock)
          .find('.icon-Eye')
          .should('exist')
          .click()
          .should('not.exist')
        cy.get(itemBlock).find('.icon-Bin').click()
        cy.confirmRemoval()
        return cy.get(itemBlock)
      })
      .should('not.exist')
    cy.contains('was successfully removed from your menu')
  })

  it('edit menu item', function () {
    const menuItemName = 'Test Menu Item to edit'
    const newMenuItemNameExtra = 'New Menu Item Extra'
    const newPrice = '45.5'
    const newDescription = 'New description for the item'

    const itemBlock = cy.newMenuItem(menuItemName)
    itemBlock.find('[class^="icon-Pencil"]').click()
    cy.get('#add-edit-item_name')
      .should('have.value', menuItemName)
      .clear()
      .type(newMenuItemNameExtra)
    cy.get('#add-edit-item_description').clear().type(newDescription)
    cy.get('#add-edit-item_price').clear().type(newPrice)
    cy.get('button').contains('Save').click()
    cy.onRestoratorMenuPage()
    cy.contains(newMenuItemNameExtra)
    cy.contains(newDescription)
    cy.contains(newPrice)
  })
})
