describe('Share Page', function () {
  it('should open login share page', function () {
    cy.login()
    cy.get('.icon-Share').click()
    cy.contains('Share')

    cy.get('a')
      .contains('Open menu page in a new tab')
      .should('have.attr', 'href')
      .and('include', 'menulike.com/')

    cy.get('p')
      .contains('Click to download')
      .parent('a')
      .then(($a) => {
        cy.get($a).should('have.attr', 'href').and('include', '.png')
        cy.get($a)
          .should('have.attr', 'download')
          .and('include', 'my-restaurant-qr-code.png')
      })

    cy.get('button').contains('Copy Link').click()
    cy.contains('Menu link copied to the clipboard')
    cy.get('button')
      .contains('Send QR to my email')
      .click()
      .then(() => {
        cy.contains('QR code sent to user@example.com')
      })

    cy.get('button')
      .contains('Send QR to my email')
      .click()
      .click()
      .then(() => {
        cy.contains('Too many requests like this. Try again later')
      })

    cy.get('i.icon-ArrowBack')
      .click()
      .then(() => {
        cy.onRestoratorMenuPage()
      })
  })
})
