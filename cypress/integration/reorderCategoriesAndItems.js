describe('Control Categories order', function () {
  before(() => {
    cy.clearLocalStorageSnapshot()
    cy.clearLocalStorage()
    cy.signup()
    cy.saveLocalStorage()
  })
  beforeEach(() => {
    cy.restoreLocalStorage()
  })
  afterEach(() => {
    cy.saveLocalStorage()
  })
  it('create items in different categories and reorder them', function () {
    cy.newMenuItem('Item 1', 'Cat 1', false)
    cy.newMenuItem('Item 2', 'Cat 2', false)
    cy.newMenuItem('Item 3', 'Cat 2', false)

    cy.get('i.icon-Pencil').click()
    cy.get('i.icon-Cross').should('not.exist')
    cy.get('button').contains('Reorder').click()

    cy.get('i.icon-ArrowDown').first().click()
    cy.get('button').contains('Save').click()
    cy.contains('Successfully updated')

    cy.back()

    cy.deleteMenuItem('Item 1')

    cy.get('i.icon-Pencil').click()
    cy.get('i.icon-Cross').should('exist').click()
    cy.confirmRemoval()
    cy.back()

    cy.get('i.icon-Pencil').click()
    cy.get('i.icon-CaretRight').click()

    cy.get('button').contains('Reorder').click()

    cy.get('i.icon-ArrowDown').first().click()
    cy.get('button').contains('Save').click()
    cy.contains('Successfully updated')

    cy.get('i.icon-Cross').should('exist')

    cy.get('i.icon-Cross').first().click()
    cy.confirmRemoval()
    cy.get('i.icon-Cross', { timeout: 2000 }).first().click()
    cy.confirmRemoval(false)
    cy.get('i.icon-Cross', { timeout: 2000 }).should('exist').click()
    cy.confirmRemoval()
    cy.back()
  })
})
