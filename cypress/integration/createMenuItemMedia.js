describe('Create menu item with Media', function () {
  before(() => {
    cy.clearLocalStorageSnapshot()
    cy.clearLocalStorage()
  })
  beforeEach(() => {
    cy.restoreLocalStorage()
  })
  afterEach(() => {
    cy.saveLocalStorage()
  })
  it('should prepare', function () {
    cy.signup()
  })
  it('should fail client validation', function () {
    cy.get('.icon-Plus').click()
    var files = []
    cy.fixture('files/menuItems/Failing.png', 'base64').then((content) => {
      files['image.png'] = content
    })
    for (var i = 0; i < 10; i++) {
      cy.fixture('files/menuItems/1.png', 'base64').then((content) => {
        files['image' + Math.random() + '.png'] = content
      })
    }
    var button = cy.get('button').contains('+').parents('button')
    button.upload(files)
    button.should('not.exist')
    cy.contains('You can upload up to 10 files for a menu item')
    for (var i = 0; i < 8; i++) {
      cy.get('.icon-Cross').last().parents('button').click()
    }
    cy.get('button').contains('+').parents('button').should('exist')
  })

  it('should fail small media', function () {
    var testNumber = parseInt(Math.random() * 100000, 10)
    cy.get('#add-edit-item_name').type('Test Item' + testNumber)
    cy.get('#add-edit-item_price').type(5.67)
    cy.get('#add-edit-item_description').type('Some description')
    cy.setMenuCategory()

    cy.get('button').contains('Save').click()
    cy.contains('Some files were not uploaded - try again or remove them')
    cy.contains('The file must be at least 10 kilobytes')
    cy.get('#add-edit-item_menu_category_id').should(
      'contain.value',
      'Test category'
    )
    cy.get('.icon-Cross').first().parents('button').click()
  })

  it('should succeed', function () {
    var extra_files = []
    cy.fixture('files/menuItems/1.png', 'base64').then((content) => {
      extra_files['image' + Math.random() + '.png'] = content
    })
    cy.get('button').contains('+').parents('button').upload(extra_files)
    cy.wait(500)
    cy.get('button').contains('Save').click()
    cy.onRestoratorMenuPage()
  })
})
