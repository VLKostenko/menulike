describe('Edit menu Item', function () {
  before(() => {
    cy.clearLocalStorageSnapshot()
    cy.clearLocalStorage()
    cy.signup()
    cy.saveLocalStorage()
  })
  beforeEach(() => {
    cy.restoreLocalStorage()
  })
  afterEach(() => {
    cy.saveLocalStorage()
  })
  it('visit empty menu', function () {
    cy.publicMenu()
    cy.contains('This menu is still empty', { timeout: 2000 })
  })
  it('create and edit menu item', function () {
    cy.visit('/place/menu')
    const items = ['Item 1', 'Item 2', 'Item 3']
    items.forEach(function (item) {
      cy.newMenuItem(item, 'Category XYZ', false)
    })
    cy.publicMenu()
    items.forEach(function (item) {
      cy.contains(item)
    })
  })
  it('hide items', function () {
    cy.visit('/place/menu')
    cy.get('.icon-EyeCrossed').each(($el) => {
      $el.click()
    })

    cy.get('.icon-Share', { timeout: 2000 }).click()
    cy.publicMenu()
    cy.contains('This menu is still empty', { timeout: 2000 })
    cy.visit('/')
  })
  it('see restaurant information', function () {
    cy.publicMenu()
    cy.get('a').contains('Test Resto').click()
    cy.contains('Test Resto')
    cy.contains('Test description')
    cy.contains('Test address')
    cy.contains('Test City')
    cy.contains('tester+')
  })
})
