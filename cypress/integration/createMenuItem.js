describe('Create menu Item', function () {
  before(() => {
    cy.clearLocalStorageSnapshot()
    cy.clearLocalStorage()
  })
  beforeEach(() => {
    cy.restoreLocalStorage()
  })
  afterEach(() => {
    cy.saveLocalStorage()
  })
  it('should prepare', function () {
    cy.signup()
  })

  it('fail client validation', function () {
    cy.get('.icon-Plus').click()
    cy.get('button').contains('Save').parents('button').should('be.disabled')
    cy.get('#add-edit-item_name').click()
    cy.get('#add-edit-item_price').click()
    cy.get('#add-edit-item_description').click()
    cy.contains('Name is required')
    cy.contains('Price is required')
  })
  it('pass client validation', function () {
    cy.get('#add-edit-item_name').type('a')
    cy.get('#add-edit-item_price').type('-1')
    cy.setMenuCategory()

    cy.get('button').contains('Save').parents('button').should('be.enabled')
    cy.get('button').contains('Save').click()
    cy.contains('The name must be at least 3 characters')
    cy.contains('The price must be at least 0')
  })
  it('succeed', function () {
    var testNumber = parseInt(Math.random() * 100000, 10)
    cy.get('#add-edit-item_name')
      .clear()
      .type('Test Item' + testNumber)
    cy.get('#add-edit-item_price').clear().type(5.67)
    cy.get('button').contains('Save').click()
    cy.onRestoratorMenuPage()
  })
})
