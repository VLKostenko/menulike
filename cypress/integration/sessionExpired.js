describe('Session expired', function () {
  it('should open login page with error', function () {
    cy.visit('/places/login/00000000-0000-0000-0000-000000000001/000001')
    cy.onRestoratorMenuPage()
    cy.reloadWait()
    cy.onRestoratorMenuPage()
    cy.setLocalStorage('placePin', '000003')
    cy.reloadWait()
    cy.contains('Place or PIN to access it is invalid')
  })
})
