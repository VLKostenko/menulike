describe('Create account', function () {
  it('should display categories that are scrolled to', function () {
    cy.login()
    cy.wait(5000)
    const checks = ['Mains', 'Alcohol', 'Soft Drinks']
    checks.forEach(function (item) {
      cy.get('p').contains(item).isNotWithinViewport()
      cy.get('span.MuiTab-wrapper').contains(item).click()
      cy.wait(2000)
      cy.get('p').contains(item).isWithinViewport()
    })
  })
})
