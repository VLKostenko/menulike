describe('Control Menu Item', function () {
  before(() => {
    cy.clearLocalStorageSnapshot()
    cy.clearLocalStorage()
    cy.signup()
    cy.saveLocalStorage()
  })
  beforeEach(() => {
    cy.restoreLocalStorage()
  })
  afterEach(() => {
    cy.saveLocalStorage()
  })
  it('should change currency', function () {
    cy.viewport(1000, 1000)
    const itemBlock = cy.newMenuItem('Test Menu Item for change currency')
    itemBlock.contains('₽')
    cy.get('.icon-Store').click()
    cy.contains('Information')
    cy.get('#edit-place_currency').select('USD $')
    cy.get('#edit-place_save').click()
    cy.get('.icon-Cloche').click()
    cy.contains('$')
  })
})
