describe('Scroll menu', function () {
  before(() => {
    cy.clearLocalStorageSnapshot()
    cy.clearLocalStorage()
    cy.signup()
    cy.saveLocalStorage()
  })
  beforeEach(() => {
    cy.restoreLocalStorage()
  })
  afterEach(() => {
    cy.saveLocalStorage()
  })

  it('to the item just created', function () {
    cy.viewport(1000, 1000)

    for (var i = 1; i <= 3; i++) {
      cy.newMenuItem('Test ' + i.toString() + ' Menu Item')
    }

    cy.wait(2000)
    cy.get('p')
      .contains('Test ' + (i - 1).toString() + ' Menu Item')
      .isWithinViewport()
  })
  it('to the item when go back', function () {
    cy.viewport(1000, 1000)
    const menuItemName = 'Test menu Item Name to scroll to on go back'
    const itemBlock = cy.newMenuItem(menuItemName)
    itemBlock.find('[class^="icon-Pencil"]').click()
    cy.get('.icon-ArrowBack').click()
    cy.onRestoratorMenuPage()
    cy.wait(2000)
    cy.get('p').contains(menuItemName).isWithinViewport()
  })
  it('to the edited item', function () {
    cy.viewport(1000, 1000)
    const menuItemName = 'Test menu Item Name to scroll to on edit'
    const itemBlock = cy.newMenuItem(menuItemName)
    itemBlock.find('[class^="icon-Pencil"]').click()
    cy.get('button').contains('Save').click()
    cy.onRestoratorMenuPage()
    cy.wait(2000)
    cy.get('p').contains(menuItemName).isWithinViewport()
  })
})
