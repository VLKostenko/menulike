describe('check 404', function () {
  it('should show 404 and ', function () {
    cy.visit('not-exists')
    cy.contains('404')
    cy.url().should('eq', Cypress.config().baseUrl + '/not-exists')
  })
})
