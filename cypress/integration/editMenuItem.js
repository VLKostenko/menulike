describe('Edit menu Item', function () {
  before(() => {
    cy.clearLocalStorageSnapshot()
    cy.clearLocalStorage()
    cy.signup()
    cy.saveLocalStorage()
  })
  beforeEach(() => {
    cy.restoreLocalStorage()
  })
  afterEach(() => {
    cy.saveLocalStorage()
  })
  it('create and edit menu item', function () {
    var itemName = 'Item 1'
    var itemNameMod = 'Item 1 Modified'
    cy.newMenuItem(itemName, 'Cat 1', false).find('.icon-Pencil-1').click()
    cy.reload()
    cy.get('#add-edit-item_name')
      .should('have.value', itemName)
      .clear()
      .type(itemNameMod)
    cy.get('button').contains('Save').click()
    cy.wait(2000)
    cy.onRestoratorMenuPage()
    cy.contains(itemNameMod)
  })
})
