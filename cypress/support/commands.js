// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import 'cypress-localstorage-commands'
import 'cypress-file-upload'

Cypress.Commands.add('reloadWait', (forceReload = false, waitTime = 1000) => {
  cy.reload(forceReload).wait(waitTime)
})

Cypress.Commands.add('onRestoratorMenuPage', () => {
  cy.get('#place-page-container_title').should('be.visible')
  cy.get('.icon-Pencil').should('be.visible')
})
Cypress.Commands.add(
  'login',
  (username = 'user@example.com', pin = '000001') => {
    cy.visit('/')
    cy.get('span').contains('Login').click()
    cy.get('input[name=email]').type(username)
    cy.get('input[name=placePin]').clear().type(pin)
    cy.get('button').contains('Login').click()
    cy.wait(2000)
    cy.onRestoratorMenuPage()
  }
)

Cypress.Commands.add('signup', () => {
  var testNumber = parseInt(Math.random() * 100000, 10)
  cy.visit('/')
  cy.get('span').contains('Create Menu').click()
  cy.get('input[name=email]').type(
    'tester+' + testNumber + '@staging.menulike.com'
  )
  cy.get('div.selected-flag').click()
  cy.get('span').contains('Russia').click()
  cy.get('input[name=phone_number]').type(testNumber)
  cy.get('button').contains('Continue').click()
  cy.get('input[name=name]').type('Test Resto ' + testNumber)
  cy.get('input[name=city]').type('Test City ' + testNumber)
  cy.get('input[name=address]').type('Test address' + testNumber)
  cy.get('input[name=description]').type('Test description' + testNumber)
  cy.get('span').contains('Create Restaurant').click()
  cy.onRestoratorMenuPage()
})

Cypress.Commands.add(
  'setMenuCategory',
  (categotyName = 'Test category' + parseInt(Math.random() * 100000, 10)) => {
    //cy.get('.icon-Plus').click()
    cy.get('#add-edit-item_menu_category_id').click()
    cy.get('#menu-item_category').type(categotyName)

    cy.get('body').then((body) => {
      if (body.find("button:contains('Create Category')").length > 0) {
        cy.get('button').contains('Create Category').click()
      } else if (
        body.find("span:contains('" + categotyName + "')").length > 0
      ) {
        cy.get('span').contains(categotyName).click()
      }
    })

    cy.get('#add-edit-item_menu_category_id').should(
      'contain.value',
      categotyName
    )
  }
)
Cypress.Commands.add('back', () => {
  cy.get('.icon-ArrowBack').click()
})
Cypress.Commands.add('publicMenu', () => {
  cy.visit('/place/menu')
  cy.get('.icon-Share').click()
  cy.wait(1500)
  cy.get('a', { timeout: 2000 })
    .contains('Open menu page in a new tab', { timeout: 300 })
    .should('have.attr', 'href')
    .then((href) => {
      cy.visit(href.split(/[/]+/).pop())
    })
})

Cypress.Commands.add('confirmRemoval', (expectSuccess = true) => {
  cy.get('button')
    .contains('remove', { matchCase: false, timeout: 300 })
    .click()
  if (expectSuccess) {
    cy.contains('was successfully removed', {
      matchCase: false,
      timeout: 2000
    })
  }
  cy.wait(1000)
})
Cypress.Commands.add('deleteMenuItem', (itemName) => {
  cy.get('p')
    .contains(itemName)
    .parents('div[name^="item"]')
    .find('.icon-Bin')
    .click()
  cy.confirmRemoval()
  cy.contains('was successfully removed from your menu')
})

Cypress.Commands.add(
  'newMenuItem',
  (
    itemName = 'Test Item ' + parseInt(Math.random() * 100000, 10),
    categoryName = 'Category for ' +
      itemName.substring(0, itemName.length - 2) +
      '**',
    withImage = true
  ) => {
    cy.get('.icon-Plus').click()
    if (withImage) {
      var files = []
      for (var i = 1; i <= 1; i++) {
        cy.fixture('files/menuItems/' + i.toString() + '.png', 'base64').then(
          (content) => {
            files['image' + Math.random() + '.png'] = content
          }
        )
      }
      var button = cy.get('button').contains('+').parents('button')
      button.upload(files)
    }

    cy.get('#add-edit-item_name').type(itemName)
    cy.get('#add-edit-item_price').type(parseFloat(Math.random() * 100, 10))
    cy.get('#add-edit-item_description').type('Description for ' + itemName)
    cy.setMenuCategory(categoryName)
    cy.get('button').contains('Save').click()
    cy.wait(2000)
    cy.onRestoratorMenuPage()
    cy.contains(itemName)
    return cy.get('p').contains(itemName).parents('div[name^="item"]')
  }
)

Cypress.Commands.add(
  'upload',
  {
    prevSubject: 'element'
  },
  (subject, files) => {
    function b64toBlob(b64Data, contentType = '', sliceSize = 512) {
      const byteCharacters = atob(b64Data)
      const byteArrays = []
      for (
        let offset = 0;
        offset < byteCharacters.length;
        offset += sliceSize
      ) {
        const slice = byteCharacters.slice(offset, offset + sliceSize)
        const byteNumbers = new Array(slice.length)
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i)
        }
        const byteArray = new Uint8Array(byteNumbers)
        byteArrays.push(byteArray)
      }
      const blob = new Blob(byteArrays, { type: contentType })
      return blob
    }

    cy.window().then((window) => {
      var testFiles = []
      for (var key in files) {
        var filename = key
        var file = files[key]
        var blob = b64toBlob(file, '', 512)
        testFiles.push(new window.File([blob], filename))
      }

      cy.wrap(subject).trigger('drop', {
        dataTransfer: { files: testFiles, types: ['Files'] }
      })
    })
  }
)

Cypress.Commands.add(
  'isNotWithinViewport',
  { prevSubject: true },
  (subject) => {
    const bottom = Cypress.$(cy.state('window')).height()
    const rect = subject[0].getBoundingClientRect()

    expect(rect.top).to.be.greaterThan(bottom)
    expect(rect.bottom).to.be.greaterThan(bottom)
    expect(rect.top).to.be.greaterThan(bottom)
    expect(rect.bottom).to.be.greaterThan(bottom)

    return subject
  }
)

Cypress.Commands.add('isWithinViewport', { prevSubject: true }, (subject) => {
  const bottom = Cypress.$(cy.state('window')).height()
  const rect = subject[0].getBoundingClientRect()

  expect(rect.top).not.to.be.greaterThan(bottom)
  expect(rect.bottom).not.to.be.greaterThan(bottom)
  expect(rect.top).not.to.be.greaterThan(bottom)
  expect(rect.bottom).not.to.be.greaterThan(bottom)

  return subject
})
