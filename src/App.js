import React, { Suspense } from 'react'
import { ThemeProvider } from '@material-ui/core/styles'
import { Redirect, Route, Switch } from 'react-router-dom'
import Unauthorized from 'components/modules/Unauthorized'
import Authorized from 'components/modules/Authorized'
import LoadingOverlay from 'components/progresses/LoadingOverlay'
import VisitorMenu from './pages/VisitorMenu'
import StaticPages from './components/modules/StaticPages'
import VisitorPlaceInfo from './pages/VisitorPlaceInfo'
import ApplicationLanguage from './pages/ApplicationLanguage'
import Subscription from './components/modules/Subscription'
import useGeolocation from './hooks/useGeolocation'
import useGoogleAnalytics from './hooks/useGoogleAnalytics'
import createTheme from './theme'
import './i18n'
import 'styles/index.sass'

const theme = createTheme()

function App() {
  useGeolocation()
  useGoogleAnalytics()
  return (
    <Suspense fallback={<LoadingOverlay independent language />}>
      <ThemeProvider theme={theme}>
        <main>
          <LoadingOverlay />
          <Switch>
            <Route path="/places" component={Unauthorized} />
            <Route path="/place" component={Authorized} />
            <Route path="/language" component={ApplicationLanguage} />
            <Route path="/static" component={StaticPages} />
            <Route path="/subscription" component={Subscription} />
            <Route exact path="/:slug" component={VisitorMenu} />
            <Route path="/:slug/info" component={VisitorPlaceInfo} />
            <Redirect to="/places" />
          </Switch>
        </main>
      </ThemeProvider>
    </Suspense>
  )
}

export default App
