import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import Backend from 'i18next-http-backend'
import { DEFAULT_LANGUAGE, getCurrentLanguage } from './utils/countries'

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    backend: {
      loadPath: '/locales/{{lng}}/{{ns}}.json'
    },
    lng: getCurrentLanguage(),
    fallbackLng: DEFAULT_LANGUAGE,
    debug: true,
    defaultNS: 'common',
    ns: 'common',
    keySeparator: false,
    react: {
      useSuspense: true
    },
    interpolation: {
      escapeValue: false
    }
  })

export default i18n
