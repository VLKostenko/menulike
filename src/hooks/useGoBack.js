import { useMemo } from 'react'
import { useHistory } from 'react-router-dom'

function useGoBack() {
  const { goBack } = useHistory()

  return useMemo(() => {
    return goBack
  }, [goBack])
}

export default useGoBack
