import { useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import ReactGA from 'react-ga'
import { GA_TRACKING_ID } from '../config'

ReactGA.initialize(GA_TRACKING_ID, {
  // debug: true
})

function useGoogleAnalytics() {
  const { pathname } = useLocation()

  useEffect(() => {
    ReactGA.set({ page: pathname })
    ReactGA.pageview(pathname)
  }, [pathname])
}

export default useGoogleAnalytics
