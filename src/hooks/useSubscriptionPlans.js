import { useEffect, useMemo } from 'react'
import { createSelector } from 'reselect'
import { useDispatch, useSelector } from 'react-redux'
import sortBy from 'lodash.sortby'
import {
  fetchSubscriptionPlansSelector,
  subscriptionPlansSelector
} from 'modules/selectors/subscription'
import { fetchSubscriptionPlans } from 'modules/actions/subscription'
import { isFalsy } from 'utils/common'

const getFetchPlansStatus = createSelector(
  fetchSubscriptionPlansSelector,
  ({ isSuccess }) => ({ isSuccess })
)

const getPlans = createSelector(subscriptionPlansSelector, (plans) => {
  return sortBy(plans, 'priority')
})

function useSubscriptionPlans() {
  const dispatch = useDispatch()
  const { isSuccess } = useSelector(getFetchPlansStatus)
  const plans = useSelector(getPlans)

  useEffect(() => {
    if (isFalsy(isSuccess)) {
      dispatch(fetchSubscriptionPlans())
    }
  }, [dispatch, isSuccess])

  return useMemo(() => {
    return plans
  }, [plans])
}

export default useSubscriptionPlans
