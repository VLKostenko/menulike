import { useEffect, useMemo } from 'react'
import { createSelector } from 'reselect'
import { useDispatch, useSelector } from 'react-redux'
import {
  fetchPaymentsInfoSelector,
  paymentsInfoSelector
} from 'modules/selectors/payments'
import { isFalsy } from 'utils/common'
import { fetchPaymentsInfo } from 'modules/actions/payments'
import { getCardVariant } from '../utils/subscription'

const getFetchInfoStatus = createSelector(
  fetchPaymentsInfoSelector,
  ({ isSuccess, isFetching }) => ({ isSuccess, isFetching })
)

const getPaymentsInfo = createSelector(
  paymentsInfoSelector,
  ({ genericTrial, defaultPaymentMethod, subscription }) => {
    const isActiveTrial = genericTrial.active
    const isActiveSubscription = subscription.active
    return {
      genericTrial,
      defaultPaymentMethod,
      subscription,
      isActiveTrial,
      isActiveSubscription,
      variant: getCardVariant(isActiveTrial, isActiveSubscription)
    }
  }
)

function usePaymentsInfo() {
  const dispatch = useDispatch()
  const { isSuccess, isFetching } = useSelector(getFetchInfoStatus)
  const info = useSelector(getPaymentsInfo)

  useEffect(() => {
    if (isFalsy(isSuccess, isFetching)) {
      dispatch(fetchPaymentsInfo())
    }
  }, [isSuccess, isFetching, dispatch])

  return useMemo(() => {
    return info
  }, [info])
}

export default usePaymentsInfo
