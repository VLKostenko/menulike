import { memo } from 'react'
import MuiListItem from '@material-ui/core/ListItem'
import withStyles from '@material-ui/core/styles/withStyles'

const ListItem = withStyles({
  root: {
    paddingLeft: 32,
    paddingRight: 32
  }
})(MuiListItem)

export default memo(ListItem)
