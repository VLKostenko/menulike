import React from 'react'
import Grid from '@material-ui/core/Grid'
import classNames from 'classnames'
import makeStyles from '@material-ui/core/styles/makeStyles'
import BackButton from '../buttons/BackButton'
import Language from '../language/Language'
import PlacePageContainer from '../containers/PlacePageContainer'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

const useStyles = makeStyles(() => ({
  root: {},
  logo: {
    fontSize: 118,
    marginBottom: 30,
    marginTop: 40
  },

  contentFrame: {
    border: 'none',
    width: '100%',
    padding: 20
  }
}))

function StaticPage({ title, children }) {
  const { t } = useTranslation('static_pages')
  const classes = useStyles()

  return (
    <PlacePageContainer
      title={title ? title : t('document')}
      footer={false}
      backgroundColor="primary"
      headerLeftComponent={<BackButton relative />}
      headerRightComponent={<Language />}
    >
      <Grid
        container
        justify="center"
        alignItems="center"
        direction="column"
        className={classes.root}
      >
        <i className={classNames('icon-Logo', classes.logo)} />
        <div className={classes.contentFrame}>{children}</div>
      </Grid>
    </PlacePageContainer>
  )
}

StaticPage.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node
}

export default StaticPage
