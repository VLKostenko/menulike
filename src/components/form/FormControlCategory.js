import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { Grid } from '@material-ui/core'
import makeStyles from '@material-ui/core/styles/makeStyles'
import classNames from 'classnames'
import FormControlMuiInput from './FormControlMuiInput'

const useStyles = makeStyles(() => ({
  container: {
    position: 'relative'
  },
  icon: {
    fontSize: 10,
    position: 'absolute',
    right: 0,
    top: 10,
    cursor: 'pointer'
  },
  categories: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%'
  }
}))

function FormControlCategory({
  id,
  name,
  value,
  error,
  touched,
  onChange,
  onBlur,
  onClick,
  placeholder,
  containerClassName
}) {
  const classes = useStyles()
  return (
    <Grid
      container
      className={classNames(classes.container, containerClassName)}
      onClick={onClick}
    >
      <FormControlMuiInput
        id={id}
        name={name}
        value={value}
        error={error}
        touched={touched}
        onChange={onChange}
        onBlur={onBlur}
        placeholder={placeholder}
        readOnly
      />
      <i className={classNames('icon-CaretDown', classes.icon)} />
    </Grid>
  )
}

FormControlCategory.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  error: PropTypes.string,
  touched: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func,
  onBlur: PropTypes.func,
  placeholder: PropTypes.string,
  containerClassName: PropTypes.string
}

export default memo(FormControlCategory)
