import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { getLabelByName } from '../../utils/forms'
import { isFalsy } from '../../utils/common'

const useStyles = makeStyles(({ palette }) => ({
  root: {
    position: 'absolute',
    bottom: -17,
    fontSize: 11,
    color: palette.error.main
  }
}))

function FormError({ isError, error, name }) {
  const { t } = useTranslation('forms')
  const classes = useStyles()

  if (isFalsy(isError)) {
    return null
  }

  return (
    <Typography className={classes.root}>
      {t(error, { label: t(getLabelByName(name)) })}
    </Typography>
  )
}

FormError.propTypes = {
  isError: PropTypes.bool.isRequired,
  error: PropTypes.string,
  name: PropTypes.string
}

export default memo(FormError)
