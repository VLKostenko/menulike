import React from 'react'

const style = {
  position: 'absolute',
  left: -99999
}

function HiddenSubmit() {
  return <input type="submit" style={style} />
}

export default HiddenSubmit
