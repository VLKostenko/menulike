import React, { memo } from 'react'
import PropTypes from 'prop-types'
import Switch from '@material-ui/core/Switch'
import Grid from '@material-ui/core/Grid'
import InputLabel from '@material-ui/core/InputLabel'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(({ shape }) => ({
  switchRoot: {
    height: 32,
    overflow: 'visible',
    padding: '10px 20px 10px 0'
  },
  switchBase: {
    top: -3,
    left: -9,
    '&.Mui-checked': {
      color: '#000',
      '& + .MuiSwitch-track': {
        backgroundColor: '#838383',
        opacity: 1
      }
    }
  },
  label: {
    ...shape.formControlMuiLabel,
    color: '#000',
    marginBottom: 0
  }
}))

function FormControlSwitch({
  id,
  name,
  label,
  containerClassName,
  value,
  onChange
}) {
  const { t } = useTranslation('forms')
  const classes = useStyles()
  return (
    <Grid container className={containerClassName} alignItems="center">
      <Switch
        id={id}
        checked={value}
        onChange={onChange}
        inputProps={{
          name
        }}
        classes={{
          root: classes.switchRoot,
          switchBase: classes.switchBase
        }}
      />
      <InputLabel htmlFor={id} className={classes.label}>
        {t(label)}
      </InputLabel>
    </Grid>
  )
}

FormControlSwitch.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  containerClassName: PropTypes.string
}

export default memo(FormControlSwitch)
