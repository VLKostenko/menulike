import React, { useMemo, memo } from 'react'
import PropTypes from 'prop-types'
import makeStyles from '@material-ui/core/styles/makeStyles'
import InputBase from '@material-ui/core/InputBase'
import Grid from '@material-ui/core/Grid'
import classNames from 'classnames'
import { concat, isTruthy } from 'utils/common'
import { transition, ms } from 'utils/styles'
import { useTranslation } from 'react-i18next'
import FormError from './FormError'

const useStyles = makeStyles(({ palette, shape, transitions, maxWidth }) => ({
  container: {
    maxWidth,
    width: '100%',
    position: 'relative'
  },
  inputRoot: {
    width: '100%'
  },
  input: {
    borderColor: palette.formControl.border.primary,
    transition: transition('all', ms(transitions.primary)),
    ...shape.formControl,

    '-webkit-appearance': 'none',

    '&:focus': {
      boxShadow: concat(
        '0 0 0 0.2em ',
        palette.formControl.focused.shadow.primary
      )
    }
  },
  inputError: {
    borderColor: palette.error.main,
    '&:focus': {
      boxShadow: concat(
        '0 0 0 0.2em ',
        palette.formControl.error.shadow.primary
      )
    }
  }
}))

function FormControlInput({
  name,
  type,
  value,
  error,
  touched,
  placeholder,
  onChange,
  onBlur,
  containerClassName,
  ...props
}) {
  const { t } = useTranslation('forms')
  const classes = useStyles()
  const isError = useMemo(() => isTruthy(error, touched), [error, touched])

  return (
    <Grid
      container
      justify="center"
      className={classNames(classes.container, containerClassName)}
    >
      <InputBase
        name={name}
        type={type}
        value={value}
        placeholder={t(placeholder)}
        onChange={onChange}
        onBlur={onBlur}
        classes={{
          root: classes.inputRoot,
          input: classNames(classes.input, {
            [classes.inputError]: isError
          })
        }}
        {...props}
      />
      <FormError isError={isError} name={name} error={error} />
    </Grid>
  )
}

FormControlInput.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  label: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.string,
  touched: PropTypes.bool,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  containerClassName: PropTypes.string
}

export default memo(FormControlInput)
