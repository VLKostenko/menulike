import React, { memo, useMemo } from 'react'
import PropTypes from 'prop-types'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import classNames from 'classnames'
import { isEqual, isTruthy, takeTruth } from 'utils/common'
import { important } from 'utils/styles'
import { DESCRIPTION } from '../../constants/forms'
import { useTranslation } from 'react-i18next'
import FormError from './FormError'

const useStyles = makeStyles(({ shape, palette }) => ({
  container: {
    position: 'relative'
  },
  inputRoot: {
    ...shape.formControlMui
  },
  inputRootError: {
    '& > div': {
      '&::after': {
        borderBottomColor: palette.error.main
      },
      '&::before': {
        borderBottomColor: palette.error.main
      }
    },
    '& label': {
      color: important(palette.error.main)
    }
  },
  labelFocused: {
    ...shape.formControlMuiLabelFocused
  }
}))

function FormControlMuiInput({
  id,
  name,
  label,
  placeholder,
  value,
  error,
  touched,
  type,
  onChange,
  onBlur,
  containerClassName,
  inputRootClassName,
  translatedPlaceholder,
  readOnly,
  ...props
}) {
  const { t } = useTranslation('forms')
  const classes = useStyles()
  const isError = useMemo(() => isTruthy(error, touched), [error, touched])

  const isMultiline = useMemo(() => {
    return isEqual(name, DESCRIPTION)
  }, [name])

  const inputProps = useMemo(() => {
    return {
      id,
      type,
      readOnly
    }
  }, [id, type, readOnly])

  const inputLabelProps = useMemo(() => {
    return {
      shrink: true,
      classes: {
        focused: classes.labelFocused
      }
    }
  }, [classes])

  return (
    <Grid
      container
      direction="column"
      className={classNames(classes.container, containerClassName)}
    >
      <FormError isError={isError} error={error} name={name} />
      <TextField
        name={name}
        label={label ? t(label) : ''}
        value={value}
        placeholder={takeTruth(translatedPlaceholder, t(placeholder))}
        InputLabelProps={inputLabelProps}
        InputProps={inputProps}
        multiline={isMultiline}
        classes={{
          root: classNames(classes.inputRoot, inputRootClassName, {
            [classes.inputRootError]: isError
          })
        }}
        onChange={onChange}
        onBlur={onBlur}
        {...props}
      />
    </Grid>
  )
}

FormControlMuiInput.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  containerClassName: PropTypes.string,
  inputRootClassName: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  error: PropTypes.string,
  touched: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  translatedPlaceholder: PropTypes.string,
  readOnly: PropTypes.bool
}

export default memo(FormControlMuiInput)
