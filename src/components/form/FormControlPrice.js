import React, { memo, useMemo } from 'react'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import { createSelector } from 'reselect'
import makeStyles from '@material-ui/core/styles/makeStyles'
import FormControlMuiInput from './FormControlMuiInput'
import { placeDataSelector } from 'modules/selectors/place'
import { concat, length, multiply } from 'utils/common'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles(({ fontFamily }) => ({
  inputRoot: {
    position: 'relative',
    '&::before': {
      content: ({ currencySymbol }) => {
        return concat('"', currencySymbol, '"')
      },
      position: 'absolute',
      zIndex: 1,
      left: 0,
      top: 6,
      fontFamily: fontFamily.primary,
      display: 'flex',
      justifyContent: 'center',
      width: ({ currencySymbol }) => {
        return multiply(length(currencySymbol), 15)
      }
    },
    '& input': {
      paddingLeft: ({ currencySymbol }) => {
        return multiply(length(currencySymbol), 15)
      }
    }
  }
}))

const getCurrencySymbol = createSelector(
  placeDataSelector,
  ({ currency_symbol, currency }) => ({
    currencySymbol: currency_symbol,
    currency
  })
)

function FormControlPrice({
  id,
  name,
  value,
  error,
  touched,
  onChange,
  onBlur,
  containerClassName
}) {
  const { t } = useTranslation('forms')
  const { currencySymbol, currency } = useSelector(getCurrencySymbol)
  const classes = useStyles({ currencySymbol })

  const placeholder = useMemo(() => {
    return concat(t('forms:placeholder_price', { currency }))
  }, [currency, t])

  return (
    <FormControlMuiInput
      id={id}
      translatedPlaceholder={placeholder}
      name={name}
      value={value}
      error={error}
      touched={touched}
      onChange={onChange}
      onBlur={onBlur}
      inputRootClassName={classes.inputRoot}
      containerClassName={containerClassName}
    />
  )
}

FormControlPrice.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  error: PropTypes.string,
  touched: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  containerClassName: PropTypes.string
}

export default memo(FormControlPrice)
