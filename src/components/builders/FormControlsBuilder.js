import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import FormControlInput from 'components/form/FormControlInput'
import FormControlTel from 'components/form/FormControlTel'
import FormControlSelect from 'components/form/FormControlSelect'
import FormControlPrice from 'components/form/FormControlPrice'
import FormControlSwitch from 'components/form/FormControlSwitch'
import FormControlCategory from 'components/form/FormControlCategory'
import { assign, concat, isFalsy, map, takeTruth } from 'utils/common'
import { createInitialOverrides } from 'utils/forms'
import {
  PHONE_CODE,
  TYPE_CATEGORY,
  TYPE_PHONE,
  TYPE_PRICE,
  TYPE_SELECT,
  TYPE_SWITCHER
} from 'constants/forms'

function FormControlsBuilder({
  groupName,
  controls,
  values,
  errors,
  touched,
  onChange,
  onBlur,
  formControlInputProps,
  formControlCategoryProps,
  inputTextComponent: InputTextComponent,
  inputTelComponent: InputTelComponent,
  selectComponent: SelectComponent,
  overrides
}) {
  const { placeholders, disabledLabels } = useMemo(() => {
    return assign(createInitialOverrides(), overrides)
  }, [overrides])

  return map(controls, ({ name, type, placeholder, options, label }, index) => {
    switch (type) {
      case TYPE_PHONE:
        return (
          <InputTelComponent
            id={concat(groupName, '_', name)}
            key={index}
            name={name}
            value={values[name]}
            phoneCodeValue={values[PHONE_CODE]}
            error={errors[name]}
            touched={touched[name]}
            placeholder={takeTruth(placeholders[name], placeholder)}
            onChange={onChange}
            onBlur={onBlur}
            label={isFalsy(disabledLabels) ? label : ''}
            {...formControlInputProps}
          />
        )
      case TYPE_SELECT:
        return (
          <SelectComponent
            id={concat(groupName, '_', name)}
            key={index}
            name={name}
            value={values[name]}
            options={options}
            onChange={onChange}
            placeholder={takeTruth(placeholders[name], placeholder)}
            label={isFalsy(disabledLabels) ? label : ''}
            {...formControlInputProps}
          />
        )
      case TYPE_PRICE:
        return (
          <FormControlPrice
            id={concat(groupName, '_', name)}
            key={index}
            name={name}
            value={values[name]}
            error={errors[name]}
            touched={touched[name]}
            onChange={onChange}
            onBlur={onBlur}
            {...formControlInputProps}
          />
        )
      case TYPE_SWITCHER:
        return (
          <FormControlSwitch
            key={index}
            id={concat(groupName, '_', name)}
            name={name}
            label={label}
            value={values[name]}
            onChange={onChange}
            {...formControlInputProps}
          />
        )
      case TYPE_CATEGORY:
        return (
          <FormControlCategory
            key={index}
            name={name}
            id={concat(groupName, '_', name)}
            error={errors[name]}
            touched={touched[name]}
            placeholder={takeTruth(placeholders[name], placeholder)}
            onChange={onChange}
            onBlur={onBlur}
            {...formControlInputProps}
            {...formControlCategoryProps}
          />
        )
      default:
        return (
          <InputTextComponent
            id={concat(groupName, '_', name)}
            key={index}
            name={name}
            type={type}
            value={values[name]}
            error={errors[name]}
            touched={touched[name]}
            onChange={onChange}
            onBlur={onBlur}
            placeholder={takeTruth(placeholders[name], placeholder)}
            label={isFalsy(disabledLabels) ? label : ''}
            {...formControlInputProps}
          />
        )
    }
  })
}

FormControlsBuilder.propTypes = {
  groupName: PropTypes.string.isRequired,
  controls: PropTypes.array.isRequired,
  values: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  touched: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  formControlInputProps: PropTypes.object,
  formControlCategoryProps: PropTypes.object,
  inputTextComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  inputTelComponent: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
  overrides: PropTypes.object
}

FormControlsBuilder.defaultProps = {
  controls: [],
  values: {},
  errors: {},
  touched: {},
  onChange: (f) => f,
  onBlur: (f) => f,
  formControlInputProps: {},
  formControlCategoryProps: {},
  inputTextComponent: FormControlInput,
  inputTelComponent: FormControlTel,
  selectComponent: FormControlSelect,
  overrides: {}
}

export default FormControlsBuilder
