import React, { memo } from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import { useTranslation } from 'react-i18next'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles({
  agreementText: {
    fontSize: 10,
    color: '#808080',
    marginBottom: 9
  },
  hangText: {
    color: '#000'
  },
  link: {
    color: 'inherit'
  }
})

function Agreement() {
  const { t } = useTranslation('agreement')
  const classes = useStyles()
  return (
    <Grid container direction="column" alignItems="center">
      <Typography className={classes.agreementText}>
        {t('agreement:agreement_text')}
        <Link to="/static/terms" className={classes.link}>
          {t('agreement:link_terms')}
        </Link>
      </Typography>
      <Typography
        className={classNames(classes.agreementText, classes.hangText)}
      >
        {t('agreement:need_hang_text')}
        <Link to="/static/faq" className={classes.link}>
          {t('agreement:link_faq')}
        </Link>
        {t('agreement:or')}
        <a href="mailto:support@menulike.com" className={classes.link}>
          {t('agreement:link_support')}
        </a>
      </Typography>
    </Grid>
  )
}

export default memo(Agreement)
