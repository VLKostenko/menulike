import React, { useEffect } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { createSelector } from 'reselect'
import { useDispatch, useSelector } from 'react-redux'
import classNames from 'classnames'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import BackButton from 'components/buttons/BackButton'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import Language from 'components/language/Language'
import Agreement from 'components/typographies/Agreement'
import SubscriptionPlans from 'pages/SubscriptionPlans'
import SubscriptionCompare from 'pages/SubscriptionCompare'
import { fetchSubscriptionPlansSelector } from 'modules/selectors/subscription'
import { fetchSubscriptionPlans } from 'modules/actions/subscription'
import { isFalsy } from 'utils/common'

const useStyles = makeStyles({
  root: {
    padding: '60px 0'
  },
  content: {
    padding: '0 20px'
  },
  logo: {
    fontSize: 117,
    marginBottom: 20
  }
})

const getFetchPlansStatus = createSelector(
  fetchSubscriptionPlansSelector,
  ({ isSuccess }) => ({ isSuccess })
)

function Subscription() {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { isSuccess } = useSelector(getFetchPlansStatus)

  useEffect(() => {
    if (isFalsy(isSuccess)) {
      dispatch(fetchSubscriptionPlans())
    }
  }, [dispatch, isSuccess])

  return (
    <PlacePageContainer
      headerLeftComponent={<BackButton relative />}
      headerRightComponent={<Language />}
      headerColor="secondary"
      footer={false}
    >
      <Grid container direction="column" className={classes.root}>
        <Grid
          container
          direction="column"
          alignItems="center"
          className={classes.content}
        >
          <i className={classNames('icon-Logo', classes.logo)} />
          <Switch>
            <Route path="/subscription/plans" component={SubscriptionPlans} />
            <Route
              path="/subscription/compare"
              component={SubscriptionCompare}
            />
            <Redirect to="/subscription/plans" />
          </Switch>
          <Agreement />
        </Grid>
      </Grid>
    </PlacePageContainer>
  )
}

export default Subscription
