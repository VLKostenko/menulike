import React from 'react'
import { Route, Switch } from 'react-router-dom'
import TermsAndConditions from '../../pages/staticPages/TermsAndConditions'
import NotFound from '../../pages/NotFound'

function StaticPages() {
  return (
    <Switch>
      <Route path="/static/terms" component={TermsAndConditions} />
      <Route path="/static/:wrong" component={NotFound} />
    </Switch>
  )
}

export default StaticPages
