import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import AuthorizedWrapper from 'components/wrappers/AuthorizedWrapper'
import PlaceMenu from 'pages/PlaceMenu'
import PlaceShare from 'pages/PlaceShare'
import EditPlace from 'pages/EditPlace'
import AddEditItem from 'pages/AddEditItem'
import PlaceCategories from 'pages/PlaceCategories'
import EditMenuItems from 'pages/EditMenuItems'
import SubscriptionManager from 'pages/SubscriptionManager'
import useAuth from 'hooks/useAuth'
import usePaymentsInfo from 'hooks/usePaymentsInfo'

function Authorized() {
  useAuth()
  usePaymentsInfo()
  return (
    <AuthorizedWrapper>
      <Switch>
        <Route
          path={['/place/menu/item/add', '/place/menu/item/edit/:id']}
          component={AddEditItem}
        />
        <Route path="/place/menu" component={PlaceMenu} />
        <Route path="/place/share" component={PlaceShare} />
        <Route path="/place/categories" component={PlaceCategories} />
        <Route path="/place/menu-items" component={EditMenuItems} />
        <Route path="/place/edit" component={EditPlace} />
        <Route
          path="/place/subscription/manager"
          component={SubscriptionManager}
        />
        <Redirect to="/place/menu" />
      </Switch>
    </AuthorizedWrapper>
  )
}

export default Authorized
