import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import classNames from 'classnames'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import CircularProgressWrapper from '../progresses/CircularProgressWrapper'
import Modal from './Modal'

const useStyles = makeStyles({
  button: {
    width: 75
  },
  buttonPrimary: {
    fontWeight: 'bold',
    marginRight: 10
  }
})

function RemoveModal({
  isOpen,
  title,
  isFetching,
  handleRemoveClick,
  handleCancelClick,
  children
}) {
  const { t } = useTranslation('buttons')
  const classes = useStyles()
  return (
    <Modal
      open={isOpen}
      title={title}
      actions={
        <Grid>
          <Button
            className={classNames(classes.button, classes.buttonPrimary)}
            onClick={handleRemoveClick}
          >
            <CircularProgressWrapper isLoading={isFetching} dark>
              {t('buttons:remove')}
            </CircularProgressWrapper>
          </Button>
          <Button onClick={handleCancelClick} className={classes.button}>
            {t('buttons:cancel')}
          </Button>
        </Grid>
      }
    >
      {children}
    </Modal>
  )
}

RemoveModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  isFetching: PropTypes.bool.isRequired,
  handleRemoveClick: PropTypes.func.isRequired,
  handleCancelClick: PropTypes.func.isRequired,
  children: PropTypes.node
}

RemoveModal.defaultProps = {
  isOpen: false,
  title: '',
  isFetching: false,
  handleRemoveClick: (f) => f,
  handleCancelClick: (f) => f
}

export default memo(RemoveModal)
