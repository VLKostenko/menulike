import React, { memo } from 'react'
import PropTypes from 'prop-types'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { percent } from 'utils/styles'

const useStyles = makeStyles({
  paper: {
    width: percent(100),
    margin: 14,
    padding: '20px 22px'
  },
  title: {
    textTransform: 'uppercase',
    marginBottom: 25,
    padding: 0,
    '& h2': {
      fontSize: 18
    }
  },
  content: {
    padding: 0,
    marginBottom: 20
  },
  actions: {
    padding: 0
  }
})

function Modal({ open, handleClose, title, actions, children }) {
  const classes = useStyles()
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      disableAutoFocus
      disableBackdropClick
      classes={{
        paper: classes.paper
      }}
    >
      <DialogTitle className={classes.title}>{title}</DialogTitle>
      <DialogContent className={classes.content}>{children}</DialogContent>
      <DialogActions className={classes.actions}>{actions}</DialogActions>
    </Dialog>
  )
}

Modal.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  actions: PropTypes.node,
  children: PropTypes.node,
  handleClose: PropTypes.func
}

Modal.defaultProps = {
  handleClose: (f) => f
}

export default memo(Modal)
