import React, { memo, useMemo } from 'react'
import PropTypes from 'prop-types'
import { Element } from 'react-scroll'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Info from './menuItemCard/Info'
import MediaCarousel from './menuItemCard/MediaCarousel'
import InactiveContainer from './menuItemCard/InactivePlaceholder'
import { createScrollItemName } from 'utils/menuView'

const useStyles = makeStyles(() => ({
  root: {
    marginBottom: 10,
    borderRadius: 8,
    overflow: 'hidden',
    position: 'relative',
    background: '#fff'
  }
}))

function MenuItemCard({ id, item, categoryName, categoryId }) {
  const classes = useStyles()
  const { active, medias } = item

  const scrollName = useMemo(() => {
    return createScrollItemName(id)
  }, [id])

  return (
    <Grid
      container
      direction="column"
      className={classes.root}
      component={Element}
      name={scrollName}
    >
      <InactiveContainer active={active} />
      <MediaCarousel medias={medias} />
      <Info
        id={id}
        item={item}
        categoryId={categoryId}
        categoryName={categoryName}
      />
    </Grid>
  )
}

MenuItemCard.propTypes = {
  id: PropTypes.string.isRequired,
  item: PropTypes.object.isRequired,
  categoryId: PropTypes.string.isRequired,
  categoryName: PropTypes.string.isRequired
}

export default memo(MenuItemCard)
