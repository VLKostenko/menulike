import React, { memo, useCallback, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import { createSelector } from 'reselect'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import ActionButtons from './info/ActionButtons'
import { toPrice } from 'utils/price'
import { concat } from 'utils/common'
import { placeDataSelector } from 'modules/selectors/place'
import { fetchToggleActivity } from 'modules/actions/place'
import { openRemoveModal } from 'modules/actions/menuItem'
import { convertActivityStatus } from 'utils/menuItem'
import { isVisitorSelector } from '../../../../modules/selectors/app'
import VisitorActionButtons from './info/VisitorActionButtons'

const useStyles = makeStyles(({ palette, fontFamily }) => ({
  infoContainer: {
    padding: '16px 16px 10px',
    background: palette.background.secondary
  },
  title: {
    fontFamily: fontFamily.secondary,
    fontSize: 24,
    marginBottom: 10
  },
  subTitle: {
    fontSize: 14,
    color: '#595959',
    marginBottom: 10,
    whiteSpace: 'pre-line'
  },
  price: {
    fontSize: 16,
    fontWeight: 500
  },
  actionsContainer: {
    zIndex: 3
  }
}))

const getCurrencySymbol = createSelector(
  placeDataSelector,
  ({ currency_symbol }) => ({
    currencySymbol: currency_symbol
  })
)

const getIsVisitor = createSelector(isVisitorSelector, (isVisitor) => isVisitor)

function Info({ id, item, categoryId }) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { currencySymbol } = useSelector(getCurrencySymbol)
  const isVisitor = useSelector(getIsVisitor)
  const editLink = useMemo(() => concat('/place/menu/item/edit/', id), [id])

  const { price, description, active, name, toggleActivity } = item

  const handleToggleActivityClick = useCallback(() => {
    dispatch(fetchToggleActivity(id, categoryId, convertActivityStatus(active)))
  }, [dispatch, id, active, categoryId])

  const handleRemoveClick = useCallback(() => {
    dispatch(openRemoveModal(id, name, categoryId))
  }, [dispatch, id, name, categoryId])

  const renderActionButtons = useMemo(() => {
    if (isVisitor) {
      return <VisitorActionButtons />
    }

    return (
      <ActionButtons
        active={active}
        editLink={editLink}
        handleToggleActivityClick={handleToggleActivityClick}
        handleRemoveClick={handleRemoveClick}
        toggleActivity={toggleActivity}
      />
    )
  }, [
    isVisitor,
    active,
    editLink,
    handleToggleActivityClick,
    handleRemoveClick,
    toggleActivity
  ])

  return (
    <Grid container direction="column" className={classes.infoContainer}>
      <Typography className={classes.title}>{name}</Typography>
      <Typography className={classes.subTitle}>{description}</Typography>
      <Grid
        container
        justify="space-between"
        alignItems="center"
        className={classes.actionsContainer}
      >
        <Typography className={classes.price}>
          {currencySymbol}
          {toPrice(price)}
        </Typography>

        {renderActionButtons}
      </Grid>
    </Grid>
  )
}

Info.propTypes = {
  id: PropTypes.string,
  item: PropTypes.object,
  categoryName: PropTypes.string,
  categoryId: PropTypes.string
}

Info.defaultProps = {
  id: '',
  item: {},
  categoryName: '',
  categoryId: ''
}

export default memo(Info)
