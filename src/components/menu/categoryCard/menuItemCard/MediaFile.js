import React from 'react'
import PropTypes from 'prop-types'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { backgroundImage, percent } from 'utils/styles'

const useStyles = makeStyles({
  root: {
    paddingTop: percent(100),
    backgroundImage: ({ url }) => backgroundImage(url),
    backgroundPosition: 'center center',
    backgroundSize: 'cover'
  }
})

function MediaFile({ url }) {
  const classes = useStyles({ url })
  return <div className={classes.root} />
}

MediaFile.propTypes = {
  url: PropTypes.string
}

export default MediaFile
