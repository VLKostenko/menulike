import React from 'react'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import IconButton from 'components/buttons/IconButton'

const useStyles = makeStyles({
  button: {
    fontSize: 12,
    border: '1px solid #000',
    width: 32,
    height: 32,
    padding: 0,
    '& i': {
      '&::before': {
        color: '#000'
      }
    }
  }
})

function VisitorActionButtons() {
  const classes = useStyles()

  if (true) {
    return null
  }

  return (
    <Grid>
      <IconButton className={classes.button}>
        <i className="icon-Plus" />
      </IconButton>
    </Grid>
  )
}

export default VisitorActionButtons
