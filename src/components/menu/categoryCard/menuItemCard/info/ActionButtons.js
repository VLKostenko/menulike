import React, { memo, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import makeStyles from '@material-ui/core/styles/makeStyles'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import Tooltip from '@material-ui/core/Tooltip'
import IconButton from 'components/buttons/IconButton'
import IconButtonProgress from 'components/buttons/IconButtonProgress'

const useStyles = makeStyles({
  button: {
    width: 48,
    height: 48,
    position: 'relative',
    '&::after': {
      content: 'none',
      display: 'block',
      position: 'absolute',
      right: -5,
      height: 20,
      width: 1,
      background: '#959595'
    },
    '&:not(:last-child)': {
      marginRight: 10,
      '&::after': {
        content: '""'
      }
    }
  }
})

function ActionButtons({
  active,
  editLink,
  handleToggleActivityClick,
  handleRemoveClick,
  toggleActivity
}) {
  const { t } = useTranslation(['tooltips', 'buttons'])
  const classes = useStyles()
  const renderActiveIcon = useMemo(() => {
    if (active) {
      return <i className="icon-EyeCrossed" />
    }
    return <i className="icon-Eye" />
  }, [active])

  const { isFetching } = toggleActivity

  return (
    <Grid item>
      <Tooltip title={t('tooltips:remove_menu_item')}>
        <IconButton onClick={handleRemoveClick} className={classes.button}>
          <i className="icon-Bin" />
        </IconButton>
      </Tooltip>
      <IconButtonProgress
        isFetching={isFetching}
        onClick={handleToggleActivityClick}
        tooltip={t('tooltips:hide_menu_item')}
        className={classes.button}
      >
        {renderActiveIcon}
      </IconButtonProgress>
      <Tooltip title={t('tooltips:edit_menu_item')}>
        <IconButton component={Link} to={editLink} className={classes.button}>
          <i className="icon-Pencil-1" />
        </IconButton>
      </Tooltip>
    </Grid>
  )
}

ActionButtons.propTypes = {
  active: PropTypes.bool,
  editLink: PropTypes.string,
  handleToggleActivityClick: PropTypes.func,
  handleRemoveClick: PropTypes.func,
  toggleActivity: PropTypes.object
}

ActionButtons.defaultProps = {
  handleToggleActivityClick: (f) => f,
  handleRemoveClick: (f) => f,
  toggleActivity: {}
}

export default memo(ActionButtons)
