import React, { memo } from 'react'
import PropTypes from 'prop-types'
import SlickSlider from 'react-slick'
import makeStyles from '@material-ui/core/styles/makeStyles'
import MediaFile from './MediaFile'
import { isEmpty, map } from 'utils/common'
import { important, percent } from 'utils/styles'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

const useStyles = makeStyles(({ breakpoints }) => ({
  root: {
    width: percent(100),
    paddingTop: percent(100),
    position: 'relative'
  },
  mediaContainer: {
    width: percent(100),
    position: 'absolute',
    top: 0,
    left: 0,
    '& .slick-arrow': {
      zIndex: 2,
      '&::before': {
        color: '#000'
      },
      [breakpoints.only('xs')]: {
        display: important('none')
      }
    },
    '& .slick-next': {
      right: 10
    },
    '& .slick-prev': {
      left: 10
    },
    '& .slick-list': {
      zIndex: 1
    },
    '& .slick-dots li': {
      margin: 0
    }
  }
  // emptyPlaceholder: {
  //   width: percent(100),
  //   paddingTop: percent(100),
  //   position: 'relative',
  //   '&::before': {
  //     content: '"No medias selected"',
  //     position: 'absolute',
  //     top: 0,
  //     left: 0,
  //     width: percent(100),
  //     height: percent(100),
  //     background: 'white',
  //     display: 'flex',
  //     justifyContent: 'center',
  //     alignItems: 'center'
  //   }
  // }
}))

const settings = {
  dots: true,
  arrows: true
}

function MediaCarousel({ medias }) {
  const classes = useStyles()

  if (isEmpty(medias)) {
    return null
  }

  return (
    <div className={classes.root}>
      <SlickSlider {...settings} className={classes.mediaContainer}>
        {map(medias, ({ id, url }) => (
          <MediaFile key={id} url={url} />
        ))}
      </SlickSlider>
    </div>
  )
}

MediaCarousel.propTypes = {
  medias: PropTypes.array
}

export default memo(MediaCarousel)
