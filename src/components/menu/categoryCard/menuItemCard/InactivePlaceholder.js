import React, { memo } from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { percent } from 'utils/styles'

const useStyles = makeStyles({
  root: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: percent(100),
    height: percent(100),
    background: '#fff',
    zIndex: 2,
    opacity: 0.8,
    padding: 5,
    fontSize: 70,
    pointerEvents: 'none'
  }
})

function InactiveContainer({ active }) {
  const classes = useStyles()

  if (active) {
    return null
  }

  return (
    <Grid container justify="flex-end" className={classes.root}>
      <i className="icon-EyeCrossed" />
    </Grid>
  )
}

InactiveContainer.propTypes = {
  active: PropTypes.bool
}

export default memo(InactiveContainer)
