import React, { memo, useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { createSelector } from 'reselect'
import { useDispatch, useSelector } from 'react-redux'
import Typography from '@material-ui/core/Typography'
import RemoveModal from 'components/modals/RemoveModal'
import { removeModalSelector } from 'modules/selectors/menuItem'
import { closeRemoveModal, fetchRemoveItem } from 'modules/actions/menuItem'

const getRemoveModal = createSelector(
  removeModalSelector,
  ({ isOpen, itemId, itemName, categoryId, isFetching }) => ({
    isOpen,
    itemId,
    itemName,
    categoryId,
    isFetching
  })
)

function RemoveItemModal() {
  const { t } = useTranslation(['menu_item', 'buttons'])
  const dispatch = useDispatch()
  const { isOpen, itemId, itemName, categoryId, isFetching } = useSelector(
    getRemoveModal
  )

  const handleCancelClick = useCallback(() => {
    dispatch(closeRemoveModal())
  }, [dispatch])

  const handleRemoveClick = useCallback(() => {
    dispatch(fetchRemoveItem(itemId, categoryId))
  }, [dispatch, itemId, categoryId])

  return (
    <RemoveModal
      isOpen={isOpen}
      isFetching={isFetching}
      title={t('menu_item:remove_modal_title')}
      handleRemoveClick={handleRemoveClick}
      handleCancelClick={handleCancelClick}
    >
      <Typography>{t('menu_item:remove_modal_text', { itemName })}</Typography>
    </RemoveModal>
  )
}

export default memo(RemoveItemModal)
