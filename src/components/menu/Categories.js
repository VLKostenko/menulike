import React, { useState, useCallback, useEffect, memo } from 'react'
import PropTypes from 'prop-types'
import { Link, scroller, Events } from 'react-scroll'
import { useLocation } from 'react-router-dom'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import AppBar from '@material-ui/core/AppBar'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { assign, ellipsis, head, isFalsy, isTruthy, map } from 'utils/common'
import {
  createScrollItemName,
  getCategoryId,
  getHashValue
} from 'utils/menuView'
import { important, rgba } from 'utils/styles'
import { createSelector } from 'reselect'
import { languageSelector } from '../../modules/selectors/app'
import { useSelector } from 'react-redux'

const useStyles = makeStyles(({ palette }) => ({
  root: {
    background: palette.background.secondary,
    zIndex: 10,
    boxShadow: 'none',
    top: 50
  },
  tabs: {
    padding: '9px 0 0 0',
    '& .MuiTabs-indicator': {
      display: 'none'
    },
    '& .MuiTabs-scrollable': {
      paddingBottom: 9,
      marginBottom: important(0),
      '&::-webkit-scrollbar': {
        WebkitAppearance: 'none',
        display: 'block',
        height: 2
      },
      '&::-webkit-scrollbar-thumb': {
        background: '#000'
      },
      '&::-webkit-scrollbar-track': {
        background: rgba(0, 0, 0, 0.2)
      }
    }
  },
  tab: {
    height: 40,
    minHeight: 'auto',
    color: '#000',
    opacity: 1,
    width: 'fit-content',
    minWidth: 'auto',
    padding: '0 16px',
    borderRadius: 20,
    textTransform: 'none',
    margin: '0 5px',
    '&.Mui-selected': {
      backgroundColor: '#000',
      color: '#fff',
      '&:hover': {
        backgroundColor: '#000'
      }
    }
  }
}))

const getLanguage = createSelector(
  languageSelector,
  ({ isFetching: isLanguageFetching }) => ({ isLanguageFetching })
)

const scrollConfig = {
  offset: -110,
  smooth: true,
  spy: true,
  isDynamic: true
}

function Categories({ categories }) {
  const classes = useStyles()
  const { hash } = useLocation()
  const { isLanguageFetching } = useSelector(getLanguage)
  const [activeTab, setActiveTab] = useState(getCategoryId(head(categories)))
  const [forceScroll, setForceScroll] = useState(false)

  useEffect(() => {
    Events.scrollEvent.register('begin', () => setForceScroll(true))
    Events.scrollEvent.register('end', () => setForceScroll(false))
    return () => {
      Events.scrollEvent.remove('begin')
      Events.scrollEvent.remove('end')
    }
  }, [setForceScroll])

  const handleChange = useCallback(
    (_, id) => {
      setActiveTab(id)
    },
    [setActiveTab]
  )

  const handleAutomaticChange = useCallback(
    (id) => {
      if (isFalsy(forceScroll)) {
        setActiveTab(id)
      }
    },
    [setActiveTab, forceScroll]
  )

  useEffect(() => {
    if (isTruthy(hash, isFalsy(isLanguageFetching))) {
      scroller.scrollTo(
        createScrollItemName(getHashValue(hash)),
        assign(scrollConfig, { delay: 1000 })
      )
    }
  }, [hash, isLanguageFetching])

  return (
    <AppBar position="fixed" className={classes.root}>
      <Tabs
        value={activeTab}
        onChange={handleChange}
        variant="scrollable"
        className={classes.tabs}
        scrollButtons="off"
      >
        {map(categories, ({ id, name }) => (
          <Tab
            key={id}
            value={id}
            label={ellipsis(name, 25)}
            className={classes.tab}
            component={Link}
            to={id}
            onSetActive={handleAutomaticChange}
            {...scrollConfig}
          />
        ))}
      </Tabs>
    </AppBar>
  )
}

Categories.propTypes = {
  categories: PropTypes.array
}

Categories.defaultProps = {
  categories: []
}

export default memo(Categories)
