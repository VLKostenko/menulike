import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import { Element } from 'react-scroll'
import sortBy from 'lodash.sortby'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { percent } from 'utils/styles'
import MenuItemCard from './categoryCard/MenuItemCard'
import { map } from 'utils/common'
import { createScrollCategoryName } from 'utils/menuView'

const useStyles = makeStyles(({ palette }) => ({
  header: {
    padding: '20px 50px 20px 0',
    position: 'relative'
  },
  divider: {
    width: percent(100),
    bottom: 'auto'
  },
  title: {
    fontSize: 24,
    fontWeight: 500,
    position: 'relative',
    left: 17,
    padding: '0px 7px',
    backgroundColor: palette.background.primary,
    wordBreak: 'break-all'
  }
}))

function CategoryCard({ name, id, menuItems }) {
  const classes = useStyles()

  const scrollName = useMemo(() => {
    return createScrollCategoryName(id)
  }, [id])

  const sortedMenuItems = useMemo(() => {
    return sortBy(menuItems, 'priority')
  }, [menuItems])

  return (
    <Grid container direction="column" component={Element} name={scrollName}>
      <Grid
        container
        alignItems="center"
        component="header"
        className={classes.header}
      >
        <Divider className={classes.divider} absolute />
        <Typography className={classes.title}>{name}</Typography>
      </Grid>
      {map(sortedMenuItems, ({ id, menu_category_id: categoryId, ...item }) => (
        <MenuItemCard
          key={id}
          id={id}
          categoryId={categoryId}
          categoryName={name}
          item={item}
        />
      ))}
    </Grid>
  )
}

CategoryCard.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  menuItems: PropTypes.array.isRequired
}

export default CategoryCard
