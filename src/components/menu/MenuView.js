import React, { Fragment, memo } from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import CategoryCard from './CategoryCard'
import { map } from 'utils/common'

const useStyles = makeStyles({
  root: {
    paddingBottom: 60,
    paddingTop: 58
  }
})

function MenuView({ categories }) {
  const classes = useStyles()
  return (
    <Fragment>
      <Grid container direction="column" className={classes.root}>
        {map(categories, ({ name, id, menu_items: menuItems }) => (
          <CategoryCard key={id} id={id} name={name} menuItems={menuItems} />
        ))}
      </Grid>
    </Fragment>
  )
}

MenuView.propTypes = {
  categories: PropTypes.array
}

MenuView.defaultProps = {
  categories: []
}

export default memo(MenuView)
