import React, { memo } from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import HeaderButton from 'components/containers/placePageContainer/header/HeaderButton'
import { getCurrentLanguageTitle } from 'utils/countries'
import { useTranslation } from 'react-i18next'

const useStyles = makeStyles({
  button: {
    '& i': {
      fontSize: 8,
      '&::before': {
        color: '#000'
      }
    }
  },
  absolute: {
    position: 'absolute',
    top: 10,
    right: 10
  },
  text: {
    marginRight: 3
  }
})

function Language({ absolute }) {
  const { i18n } = useTranslation()
  const classes = useStyles()

  return (
    <HeaderButton
      className={classNames(classes.button, {
        [classes.absolute]: absolute
      })}
      component={Link}
      to="/language"
    >
      <Typography className={classes.text}>
        {getCurrentLanguageTitle(i18n.language)}
      </Typography>
      <i className="icon-CaretDown" />
    </HeaderButton>
  )
}

Language.propTypes = {
  absolute: PropTypes.bool
}

export default memo(Language)
