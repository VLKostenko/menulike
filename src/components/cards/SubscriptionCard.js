import React, { memo, useMemo } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import getSymbol from 'currency-symbol-map'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import { px } from 'utils/styles'
import { concat } from 'utils/common'
import { useTranslation } from 'react-i18next'
import { calculatePrice, calculateTermTitle } from '../../utils/subscription'

const useStyles = makeStyles(({ shape }) => ({
  root: {
    ...shape.cards.subscription.root,
    marginBottom: 12,
    '&:last-child': {
      marginBottom: 0
    }
  },
  active: {
    border: '1px solid #000',
    borderTopLeftRadius: 0,
    position: 'relative',
    '&::before': {
      content: '""',
      position: 'absolute',
      width: 0,
      height: 0,
      borderStyle: 'solid',
      borderWidth: '34px 34px 0 0',
      borderColor: '#000000 transparent transparent transparent',
      '-webkit-transform': 'rotate(360deg)'
    },
    '&::after': {
      content: '"\\e90f"',
      fontFamily: '"icomoon" !important',
      position: 'absolute',
      top: 0,
      left: 0,
      color: '#fff',
      fontSize: 20
    }
  },
  priceContainer: {
    width: 133,
    padding: '15px 0'
  },
  priceText: {
    ...shape.cards.subscription.priceText
  },
  textThin: {
    ...shape.cards.subscription.textThin
  },
  infoContainer: {
    padding: 10,
    width: 'calc(100% - 134px)'
  },
  tagContainer: {
    marginBottom: 5
  },
  expiry: {
    width: 'max-content',
    border: '1px solid #000',
    fontSize: 12,
    borderRadius: 12,
    padding: '0 15px'
  },
  infoText: {
    fontSize: 11,
    lineHeight: px(11),
    color: '#686868',
    verticalAlign: 'middle'
  },
  divider: {
    ...shape.cards.subscription.divider
  }
}))

function SubscriptionCard({
  name,
  price,
  code,
  term,
  termsCount,
  description,
  currency,
  rootClassName,
  active
}) {
  const { t } = useTranslation('subscription_plans')
  const classes = useStyles()

  const renderPrice = useMemo(() => {
    return concat(getSymbol(currency), calculatePrice(price, term, termsCount))
  }, [currency, price, term, termsCount])

  const renderTermTitle = useMemo(() => {
    return t(calculateTermTitle(term, termsCount, code), { termsCount })
  }, [term, termsCount, code, t])

  return (
    <Grid
      container
      className={classNames(classes.root, rootClassName, {
        [classes.active]: active
      })}
    >
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className={classes.priceContainer}
      >
        <Typography className={classes.priceText}>{name}</Typography>
        <Typography className={classes.priceText}>
          {renderPrice}
          <span className={classes.textThin}>
            {t('subscription_plans:term')}
          </span>
        </Typography>
      </Grid>
      <Divider orientation="vertical" className={classes.divider} />
      <Grid
        container
        direction="column"
        justify="space-between"
        className={classes.infoContainer}
      >
        <Grid container justify="flex-end" className={classes.tagContainer}>
          <Typography className={classes.expiry} align="center">
            {renderTermTitle}
          </Typography>
        </Grid>
        <Grid container justify="flex-start">
          <Typography className={classes.infoText}>{description}</Typography>
        </Grid>
      </Grid>
    </Grid>
  )
}

SubscriptionCard.propTypes = {
  name: PropTypes.string,
  code: PropTypes.string,
  price: PropTypes.string,
  term: PropTypes.string,
  termsCount: PropTypes.number,
  description: PropTypes.string,
  currency: PropTypes.string,
  rootClassName: PropTypes.string,
  active: PropTypes.bool
}

export default memo(SubscriptionCard)
