import React, { memo, useMemo } from 'react'
import classNames from 'classnames'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import FabWarning from 'components/fabs/FabWarning'
import usePaymentsInfo from 'hooks/usePaymentsInfo'
import { dateToView } from 'utils/date'

const useStyles = makeStyles({
  root: {
    background: '#fff',
    padding: 6,
    borderRadius: 32,
    marginBottom: 11
  },
  info: {
    width: 'calc(100% - 44px)'
  },
  title: {
    fontSize: 14
  },
  bold: {
    fontWeight: 500
  },
  subTitle: {
    fontSize: 11
  },
  link: {
    textDecoration: 'underline',
    color: '#000'
  }
})

function SubscriptionInfoCard() {
  const { t } = useTranslation('subscription_manager')
  const classes = useStyles()
  const { genericTrial, variant } = usePaymentsInfo()

  const renderTitle = useMemo(() => {
    switch (variant) {
      case 'success':
        return (
          <Typography className={classes.title}>
            {t('subscription_manager:info_card_success_title')}
            <span className={classes.bold}>"Resto+" </span>
          </Typography>
        )
      case 'warning':
        return (
          <Typography className={classes.title}>
            {t('subscription_manager:info_card_warning_title')}
            <span className={classes.bold}>
              {dateToView(genericTrial.ends_at)}
            </span>
          </Typography>
        )
      case 'error':
        return (
          <Typography className={classes.title}>
            {t('subscription_manager:info_card_error_title')}
          </Typography>
        )
      default:
        return null
    }
  }, [variant, classes, t, genericTrial])

  const renderSubtitle = useMemo(() => {
    switch (variant) {
      case 'success':
        return (
          <Typography
            className={classNames(classes.subTitle, classes.link)}
            component={Link}
            to="/place/subscription/manager"
          >
            {t('subscription_manager:manage_subscription')}
          </Typography>
        )
      default:
        return (
          <Typography
            className={classNames(classes.subTitle, classes.link)}
            component={Link}
            to="/place/subscription/manager"
          >
            {t('subscription_manager:info_card_subtitle')}
          </Typography>
        )
    }
  }, [variant, classes, t])

  return (
    <Grid container className={classes.root}>
      <FabWarning variant={variant} />
      <Grid
        container
        className={classes.info}
        direction="column"
        justify="center"
        alignItems="center"
      >
        {renderTitle}
        {renderSubtitle}
      </Grid>
    </Grid>
  )
}

export default memo(SubscriptionInfoCard)
