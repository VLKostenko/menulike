import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import getSymbol from 'currency-symbol-map'
import PropTypes from 'prop-types'
import { ScrollSyncPane } from 'react-scroll-sync'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import { concat, includes, map } from 'utils/common'
import { calculatePrice } from 'utils/subscription'
import { percent, rgba } from 'utils/styles'
import { getId } from 'utils/object'

const useStyles = makeStyles(({ shape, palette }) => ({
  root: {
    ...shape.cards.subscription.root,
    marginBottom: 12
  },
  priceContainer: {
    width: 91,
    minWidth: 91,
    padding: '15px 0'
  },
  priceText: {
    ...shape.cards.subscription.priceText
  },
  textThin: {
    ...shape.cards.subscription.textThin
  },
  divider: {
    ...shape.cards.subscription.divider
  },
  featuresContainer: {
    overflow: 'auto',
    background: palette.background.primary,
    '&::-webkit-scrollbar': {
      display: 'none'
    }
  },
  featuresWrapper: {
    display: 'flex'
  },
  feature: {
    width: 100,
    minWidth: 100,
    height: percent(100),
    fontSize: 23,
    background: rgba(255, 255, 255, 0.5),
    '&:nth-child(2n)': {
      background: rgba(255, 255, 255, 0.3)
    }
  }
}))

function SubscriptionFeaturesCard({
  name,
  price,
  term,
  termsCount,
  currency,
  allFeatures,
  features
}) {
  const { t } = useTranslation('subscription_plans')
  const classes = useStyles()

  const featuresIds = useMemo(() => {
    return map(features, getId)
  }, [features])

  const renderPrice = useMemo(() => {
    return concat(getSymbol(currency), calculatePrice(price, term, termsCount))
  }, [currency, price, term, termsCount])

  return (
    <Grid container wrap="nowrap" className={classes.root}>
      <Grid
        container
        justify="center"
        alignItems="center"
        className={classes.priceContainer}
      >
        <Typography className={classes.priceText}>{name}</Typography>
        <Typography className={classes.priceText}>
          {renderPrice}
          <span className={classes.textThin}>
            {t('subscription_plans:term')}
          </span>
        </Typography>
      </Grid>
      <Divider orientation="vertical" className={classes.divider} />
      <ScrollSyncPane>
        <Grid container className={classes.featuresContainer} wrap="nowrap">
          <div className={classes.featuresWrapper}>
            {map(allFeatures, ({ id }) => (
              <Grid
                container
                justify="center"
                alignItems="center"
                key={id}
                className={classes.feature}
              >
                {includes(featuresIds, id) && <i className="icon-Done" />}
              </Grid>
            ))}
          </div>
        </Grid>
      </ScrollSyncPane>
    </Grid>
  )
}

SubscriptionFeaturesCard.propTypes = {
  name: PropTypes.string,
  price: PropTypes.string,
  term: PropTypes.string,
  termsCount: PropTypes.number,
  currency: PropTypes.string,
  allFeatures: PropTypes.array,
  features: PropTypes.array
}

SubscriptionFeaturesCard.defaultProps = {
  setScrollPosition: (f) => f
}

export default SubscriptionFeaturesCard
