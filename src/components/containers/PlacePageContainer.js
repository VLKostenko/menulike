import React from 'react'
import Div100vh from 'react-div-100vh'
import PropTypes from 'prop-types'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import classNames from 'classnames'
import Header from './placePageContainer/Header'
import Footer from './placePageContainer/Footer'
import { percent } from 'utils/styles'
import { DIV_100_VH_STYLES } from 'constants/div100vh'
import DocumentTitle from '../document/DocumentTitle'

const useStyles = makeStyles(({ palette, shape }) => ({
  root: {
    display: 'flex',
    width: percent(100),
    height: percent(100)
  },
  container: {
    flex: 1,
    background: ({ backgroundColor }) => palette.background[backgroundColor]
  },
  wrapper: {
    flex: 1,
    ...shape.container
  },
  content: {
    width: percent(100),
    maxWidth: percent(100),
    zIndex: ({ contentZIndex }) => contentZIndex,
    paddingTop: 50
  }
}))

function PlacePageContainer({
  headerLeftComponent,
  headerRightComponent,
  footerActiveLeft,
  footerActiveRight,
  title,
  footer,
  headerDivider,
  headerColor,
  backgroundColor,
  rootClassName,
  contentContainerId,
  contentZIndex,
  headerContent,
  headerTitleComponent,
  children
}) {
  const classes = useStyles({ backgroundColor, footer, contentZIndex })

  return (
    <Grid
      container
      className={classNames(classes.root, rootClassName)}
      component={Div100vh}
      style={DIV_100_VH_STYLES}
    >
      <DocumentTitle />
      <Grid
        container
        className={classes.container}
        direction="column"
        alignItems="center"
      >
        <Grid
          container
          className={classes.wrapper}
          direction="column"
          alignItems="center"
          wrap="nowrap"
        >
          <Header
            title={title}
            leftComponent={headerLeftComponent}
            rightComponent={headerRightComponent}
            divider={headerDivider}
            backgroundColor={headerColor}
            content={headerContent}
            titleComponent={headerTitleComponent}
          />
          <div className={classes.content} id={contentContainerId}>
            {children}
          </div>
          {footer && (
            <Footer
              activeLeft={footerActiveLeft}
              activeRight={footerActiveRight}
            />
          )}
        </Grid>
      </Grid>
    </Grid>
  )
}

PlacePageContainer.propTypes = {
  headerLeftComponent: PropTypes.node,
  headerRightComponent: PropTypes.node,
  title: PropTypes.string,
  children: PropTypes.node,
  footerActiveLeft: PropTypes.bool,
  footerActiveRight: PropTypes.bool,
  footer: PropTypes.bool,
  headerDivider: PropTypes.bool,
  headerColor: PropTypes.oneOf(['primary', 'secondary']),
  backgroundColor: PropTypes.oneOf(['primary', 'secondary', 'third']),
  rootClassName: PropTypes.string,
  contentContainerId: PropTypes.string,
  headerContent: PropTypes.node,
  headerTitleComponent: PropTypes.node,
  contentZIndex: PropTypes.number
}

PlacePageContainer.defaultProps = {
  headerLeftComponent: <div />,
  headerRightComponent: <div />,
  footer: true,
  headerDivider: false,
  headerColor: 'primary',
  backgroundColor: 'primary',
  contentZIndex: 1
}

export default PlacePageContainer
