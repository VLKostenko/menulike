import React, { Fragment, useMemo } from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { px } from 'utils/styles'

const useStyles = makeStyles(({ palette }) => ({
  root: {
    position: 'fixed',
    top: 0,
    zIndex: 10
  },
  header: {
    height: 50,
    background: ({ backgroundColor }) =>
      palette.headerBackground[backgroundColor],
    padding: '0 5px',
    borderBottom: ({ divider }) =>
      divider ? '1px solid rgba(0, 0, 0, 0.1)' : ''
  },
  headerTitle: {
    fontWeight: 500,
    fontSize: 17,
    lineHeight: px(20)
  }
}))

function Header({
  rightComponent,
  leftComponent,
  title,
  divider,
  backgroundColor,
  content,
  titleComponent
}) {
  const classes = useStyles({ divider, backgroundColor })

  const renderTitle = useMemo(() => {
    if (titleComponent) {
      return titleComponent
    }

    return (
      <Typography
        className={classes.headerTitle}
        align="center"
        id="place-page-container_title"
      >
        {title}
      </Typography>
    )
  }, [titleComponent, title, classes])

  const renderContent = useMemo(() => {
    if (content) {
      return content
    }

    return (
      <Fragment>
        <Grid item xs={3} container justify="flex-start">
          {leftComponent}
        </Grid>
        <Grid item xs={6} container justify="center">
          {renderTitle}
        </Grid>
        <Grid item xs={3} container justify="flex-end">
          {rightComponent}
        </Grid>
      </Fragment>
    )
  }, [content, leftComponent, rightComponent, renderTitle])

  return (
    <Grid container direction="column" className={classes.root}>
      <Grid container className={classes.header} alignItems="center">
        {renderContent}
      </Grid>
    </Grid>
  )
}

Header.propTypes = {
  rightComponent: PropTypes.node,
  leftComponent: PropTypes.node,
  title: PropTypes.string,
  divider: PropTypes.bool,
  backgroundColor: PropTypes.string,
  content: PropTypes.node,
  titleComponent: PropTypes.node
}

export default Header
