import Button from '@material-ui/core/Button'
import withStyles from '@material-ui/core/styles/withStyles'
import { px } from 'utils/styles'

const HeaderButton = withStyles({
  root: {
    textTransform: 'none',
    fontWeight: 400,
    fontSize: 16,
    lineHeight: px(19),
    margin: 5
  }
})(Button)

export default HeaderButton
