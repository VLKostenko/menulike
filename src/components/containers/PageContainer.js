import React from 'react'
import PropTypes from 'prop-types'
import Div100vh from 'react-div-100vh'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import { DIV_100_VH_STYLES } from 'constants/div100vh'
import DocumentTitle from '../document/DocumentTitle'

const useStyles = makeStyles(({ palette }) => ({
  root: {
    backgroundColor: palette.background.primary,
    flex: 1,
    overflow: 'auto'
  }
}))

function PageContainer({ children }) {
  const classes = useStyles()
  return (
    <Grid
      container
      className={classes.root}
      direction="column"
      component={Div100vh}
      style={DIV_100_VH_STYLES}
    >
      <DocumentTitle />
      {children}
    </Grid>
  )
}

PageContainer.propTypes = {
  children: PropTypes.node
}

export default PageContainer
