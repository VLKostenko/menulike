import PropTypes from 'prop-types'
import { concat } from '../../utils/common'
import { DEFAULT_TITLE } from '../../config'

function DocumentTitle({ title }) {
  document.title = concat(title ? concat(title, ' - ') : '', DEFAULT_TITLE)
  return null
}

DocumentTitle.propTypes = {
  title: PropTypes.string
}

export default DocumentTitle
