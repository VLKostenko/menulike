import React from 'react'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles(({ shape, palette }) => ({
  root: {
    ...shape.fab,
    background: palette.error.main,
    fontSize: 14
  }
}))

function FabFailure() {
  const classes = useStyles()
  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <i className="icon-Cross" />
    </Grid>
  )
}

export default FabFailure
