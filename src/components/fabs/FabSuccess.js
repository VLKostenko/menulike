import React from 'react'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles(({ shape }) => ({
  root: {
    ...shape.fab,
    background: '#000'
  }
}))

function FabSuccess() {
  const classes = useStyles()
  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <i className="icon-Done" />
    </Grid>
  )
}

export default FabSuccess
