import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import IconButton from 'components/buttons/IconButton'

const useStyles = makeStyles(({ shape, palette }) => ({
  root: {
    ...shape.fab,
    background: palette.error.main,
    cursor: 'pointer'
  },
  buttonRoot: {
    color: '#fff',
    padding: 8
  }
}))

function FabRefresh({ onClick }) {
  const classes = useStyles()
  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <IconButton className={classes.buttonRoot} onClick={onClick}>
        <i className="icon-Refresh" />
      </IconButton>
    </Grid>
  )
}

FabRefresh.propTypes = {
  onClick: PropTypes.func
}

FabRefresh.defaultProps = {
  onClick: (f) => f
}

export default FabRefresh
