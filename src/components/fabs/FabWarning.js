import React, { memo } from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { percent } from 'utils/styles'

const useStyles = makeStyles(({ palette }) => ({
  root: {
    width: 44,
    height: 44,
    background: ({ variant }) => palette.fab.warning.background[variant],
    color: '#fff',
    borderRadius: percent(50),
    fontSize: 20
  }
}))

function FabWarning({ variant }) {
  const classes = useStyles({ variant })
  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <i className="icon-Alert" />
    </Grid>
  )
}

FabWarning.propTypes = {
  variant: PropTypes.oneOf(['success', 'warning', 'error'])
}

FabWarning.defaultProps = {
  variant: 'success'
}

export default memo(FabWarning)
