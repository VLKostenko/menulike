import React, { useMemo } from 'react'
import PropTypes from 'prop-types'
import IconButton from './IconButton'
import CircularProgressWrapper from 'components/progresses/CircularProgressWrapper'
import Tooltip from '@material-ui/core/Tooltip'
import { isFalsy } from 'utils/common'

function IconButtonProgress({ isFetching, children, tooltip, ...props }) {
  const tooltipState = useMemo(() => {
    return {
      text: tooltip ? tooltip : '',
      disabled: isFalsy(tooltip)
    }
  }, [tooltip])

  return (
    <Tooltip
      title={tooltipState.text}
      disableHoverListener={tooltipState.disabled}
    >
      <IconButton {...props}>
        <CircularProgressWrapper isLoading={isFetching} dark>
          {children}
        </CircularProgressWrapper>
      </IconButton>
    </Tooltip>
  )
}

IconButtonProgress.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  tooltip: PropTypes.string
}

export default IconButtonProgress
