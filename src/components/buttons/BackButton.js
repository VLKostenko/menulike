import React, { useCallback } from 'react'
import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'

import makeStyles from '@material-ui/core/styles/makeStyles'
import classNames from 'classnames'

import IconButton from './IconButton'

const useStyles = makeStyles({
  root: {
    position: 'absolute',
    top: 7,
    left: 7,
    fontSize: 19,
    width: 48,
    height: 48
  },
  rootRelative: {
    position: 'relative',
    top: 'auto',
    left: 'auto'
  }
})

function BackButton({ relative, component, ...props }) {
  const classes = useStyles()
  const { goBack } = useHistory()

  const onClick = useCallback(() => {
    goBack()
  }, [goBack])

  return (
    <IconButton
      component={component ? component : undefined}
      onClick={onClick}
      className={classNames(classes.root, {
        [classes.rootRelative]: relative
      })}
      {...props}
    >
      <i className="icon-ArrowBack" />
    </IconButton>
  )
}

BackButton.propTypes = {
  to: PropTypes.string,
  relative: PropTypes.bool,
  component: PropTypes.oneOfType([PropTypes.object, PropTypes.bool])
}

BackButton.defaultProps = {
  relative: false
}

export default BackButton
