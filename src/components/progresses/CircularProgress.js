import MuiCircularProgress from '@material-ui/core/CircularProgress'
import withStyles from '@material-ui/core/styles/withStyles'

const CircularProgress = withStyles({
  root: {
    color: '#000'
  }
})(MuiCircularProgress)

export default CircularProgress
