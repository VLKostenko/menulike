import React, { useEffect } from 'react'
import ReactDOM from 'react-dom'
import Div100vh from 'react-div-100vh'
import PropTypes from 'prop-types'
import { createSelector } from 'reselect'
import { useDispatch, useSelector } from 'react-redux'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import { geolocationSelector } from 'modules/selectors/app'
import { vw } from 'utils/styles'
import { isFalsy } from 'utils/common'
import {
  placeByIdSelector,
  placeBySlugSelector
} from 'modules/selectors/places'
import { fetchLanguage, languageSuccess } from '../../modules/actions/app'

const useStyles = makeStyles(({ palette, breakpoints }) => ({
  root: {
    top: 0,
    left: 0,
    position: 'fixed',
    zIndex: 1000,
    width: vw(100),
    background: palette.background.primary,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: {
    fontSize: 200,
    fontWeight: 500,
    [breakpoints.only('xs')]: {
      fontSize: vw(30)
    }
  }
}))

const domNode = document.body

const getGeolocation = createSelector(
  geolocationSelector,
  ({ isFetching }) => ({ isGeolocationFetching: isFetching })
)

const getPlaceById = createSelector(placeByIdSelector, ({ isFetching }) => ({
  isPlaceByIdFetching: isFetching
}))

const getPlaceBySlug = createSelector(
  placeBySlugSelector,
  ({ isFetching }) => ({
    isPlaceBySlugFetching: isFetching
  })
)

function LoadingOverlay({ independent, language }) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const { isGeolocationFetching } = useSelector(getGeolocation)
  const { isPlaceByIdFetching } = useSelector(getPlaceById)
  const { isPlaceBySlugFetching } = useSelector(getPlaceBySlug)

  useEffect(() => {
    if (language) {
      dispatch(fetchLanguage())
      return () => {
        dispatch(languageSuccess())
      }
    }
  }, [language, dispatch])

  if (
    isFalsy(
      independent,
      isGeolocationFetching,
      isPlaceByIdFetching,
      isPlaceBySlugFetching
    )
  ) {
    return null
  }

  return ReactDOM.createPortal(
    <Div100vh className={classes.root}>
      <Typography className={classes.text}>
        <i className="icon-ServingDish" />
      </Typography>
    </Div100vh>,
    domNode
  )
}

LoadingOverlay.propTypes = {
  independent: PropTypes.bool,
  language: PropTypes.bool
}

LoadingOverlay.defaultProps = {
  independent: false,
  language: false
}

export default LoadingOverlay
