import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { fork } from 'redux-saga/effects'
import createSagaMiddleware from 'redux-saga'

import app from 'modules/reducers/app'
import phone from 'modules/reducers/phone'
import places from 'modules/reducers/places'
import place from 'modules/reducers/place'
import menuItem from 'modules/reducers/menuItem'
import categories from 'modules/reducers/categories'
import subscription from 'modules/reducers/subscription'
import payments from 'modules/reducers/payments'

import placesWatcher from 'modules/sagas/places'
import appWatcher from 'modules/sagas/app'
import placeWatcher from 'modules/sagas/place'
import menuItemWatcher from 'modules/sagas/menuItem'
import categoriesWatcher from 'modules/sagas/categories'
import subscriptionWatcher from 'modules/sagas/subscription'
import paymentsWatcher from 'modules/sagas/payments'

const rootReducer = combineReducers({
  app,
  place,
  phone,
  places,
  payments,
  menuItem,
  categories,
  subscription
})

function* rootSaga() {
  yield fork(appWatcher)
  yield fork(placeWatcher)
  yield fork(placesWatcher)
  yield fork(menuItemWatcher)
  yield fork(paymentsWatcher)
  yield fork(categoriesWatcher)
  yield fork(subscriptionWatcher)
}

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
)

sagaMiddleware.run(rootSaga)

export default store
