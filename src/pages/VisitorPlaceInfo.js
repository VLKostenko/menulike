import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import { useTranslation } from 'react-i18next'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import BackButton from 'components/buttons/BackButton'
import InfoCard from './visitorPlaceInfo/InfoCard'
import { placeDataSelector } from 'modules/selectors/place'
import { concat, isFalsy } from 'utils/common'
import { fetchPlaceBySlug } from 'modules/actions/places'
import { percent, px } from 'utils/styles'
import { placeBySlugSelector } from 'modules/selectors/places'
import NotFound from './NotFound'
import { parsePhoneNumberFromString } from 'libphonenumber-js'

const useStyles = makeStyles({
  root: {
    paddingTop: 15,
    paddingBottom: 30
  },
  description: {
    width: percent(100),
    marginBottom: 15,
    fontSize: 16,
    lineHeight: px(20),
    padding: '0 10px'
  },
  qr: {
    marginBottom: 30
  },
  details: {
    padding: '0 10px'
  }
})

const getPlaceBySlug = createSelector(
  placeBySlugSelector,
  ({ isFetching, isSuccess, isFailure }) => ({
    isFetching,
    isSuccess,
    isFailure
  })
)

const getPlaceData = createSelector(
  placeDataSelector,
  ({
    name,
    slug,
    description,
    qr_code_url: qrCodeUrl,
    phone_code: phoneCode,
    phone_number: phoneNumber,
    email,
    address,
    city
  }) => ({
    name,
    slug,
    description,
    qrCodeUrl,
    phone: phoneNumber
      ? parsePhoneNumberFromString(
          concat('+', phoneCode, phoneNumber)
        ).formatInternational()
      : null,
    email,
    address: concat(address, ', ', city)
  })
)

function VisitorPlaceInfo() {
  const { t } = useTranslation('visitor_place_info')
  const { slug: paramsSlug } = useParams()
  const classes = useStyles()
  const dispatch = useDispatch()
  const { isFailure } = useSelector(getPlaceBySlug)
  const {
    name,
    slug,
    description,
    qrCodeUrl,
    phone,
    email,
    address
  } = useSelector(getPlaceData)

  useEffect(() => {
    if (isFalsy(slug)) {
      dispatch(fetchPlaceBySlug(paramsSlug))
    }
  }, [slug, paramsSlug, dispatch])

  if (isFailure) {
    return <NotFound />
  }

  return (
    <PlacePageContainer
      title={name}
      footer={false}
      backgroundColor="third"
      headerLeftComponent={<BackButton relative to={concat('/', slug)} />}
    >
      <Grid container className={classes.root}>
        <Typography align="center" className={classes.description}>
          {description}
        </Typography>
        <Grid container justify="center" className={classes.qr}>
          <img src={qrCodeUrl} alt="QR" width="230" height="230" />
        </Grid>
        <Grid container className={classes.details}>
          <InfoCard
            title={t('visitor_place_info:email')}
            description={email}
            link={concat('mailto:', email)}
          />
          <InfoCard
            title={t('visitor_place_info:phone')}
            description={phone}
            link={concat('tel:', phone)}
          />
          <InfoCard
            title={t('visitor_place_info:address')}
            description={address}
            link={concat('https://www.google.com/maps?q=', encodeURI(address))}
          />
        </Grid>
      </Grid>
    </PlacePageContainer>
  )
}

export default VisitorPlaceInfo
