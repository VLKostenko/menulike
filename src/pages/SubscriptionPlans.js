import React from 'react'
import { useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import { createSelector } from 'reselect'
import sortBy from 'lodash.sortby'
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import SubscriptionCard from 'components/cards/SubscriptionCard'
import ButtonPrimary from 'components/buttons/ButtonPrimary'
import { subscriptionPlansSelector } from 'modules/selectors/subscription'
import { map } from '../utils/common'

const useStyles = makeStyles({
  title: {
    fontSize: 14,
    marginBottom: 17
  },
  cardsContainer: {
    marginBottom: 24
  },
  button: {
    marginBottom: 20
  }
})

const getPlans = createSelector(subscriptionPlansSelector, (plans) => {
  return sortBy(plans, 'priority')
})

function SubscriptionPlans() {
  const { t } = useTranslation(['subscription_plans', 'buttons'])
  const classes = useStyles()
  const plans = useSelector(getPlans)

  return (
    <Grid container direction="column" alignItems="center">
      <Typography className={classes.title}>
        {t('subscription_plans:title')}
      </Typography>
      <Grid container direction="column" className={classes.cardsContainer}>
        {map(
          plans,
          ({
            id,
            name,
            price,
            term,
            terms_count: termsCount,
            description,
            currency,
            code
          }) => (
            <SubscriptionCard
              key={id}
              code={code}
              name={name}
              price={price}
              term={term}
              termsCount={termsCount}
              description={description}
              currency={currency}
            />
          )
        )}
      </Grid>
      <ButtonPrimary
        className={classes.button}
        component={Link}
        to="/subscription/compare"
      >
        {t('buttons:compare_plans')}
      </ButtonPrimary>
    </Grid>
  )
}

export default SubscriptionPlans
