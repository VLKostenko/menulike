import React, { memo, useCallback } from 'react'
import { useTranslation } from 'react-i18next'
import { createSelector } from 'reselect'
import { useDispatch, useSelector } from 'react-redux'
import Typography from '@material-ui/core/Typography'
import { removeModalSelector } from 'modules/selectors/categories'
import {
  closeRemoveModal,
  fetchRemoveCategory
} from 'modules/actions/categories'
import RemoveModal from 'components/modals/RemoveModal'

const getRemoveModal = createSelector(
  removeModalSelector,
  ({ isOpen, categoryId, categoryName, isFetching }) => ({
    isOpen,
    categoryId,
    categoryName,
    isFetching
  })
)

function RemoveCategoryModal() {
  const { t } = useTranslation('categories')
  const dispatch = useDispatch()
  const { isOpen, categoryId, categoryName, isFetching } = useSelector(
    getRemoveModal
  )

  const handleCancelClick = useCallback(() => {
    dispatch(closeRemoveModal())
  }, [dispatch])

  const handleRemoveClick = useCallback(() => {
    dispatch(fetchRemoveCategory(categoryId))
  }, [dispatch, categoryId])

  return (
    <RemoveModal
      isOpen={isOpen}
      isFetching={isFetching}
      title={t('categories:remove_modal_title')}
      handleCancelClick={handleCancelClick}
      handleRemoveClick={handleRemoveClick}
    >
      <Typography>
        {t('categories:remove_modal_text', { categoryName })}
      </Typography>
    </RemoveModal>
  )
}

export default memo(RemoveCategoryModal)
