import React, { memo, useCallback, useEffect, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import InputBase from '@material-ui/core/InputBase'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import ListItem from '@material-ui/core/ListItem'
import makeStyles from '@material-ui/core/styles/makeStyles'
import IconButton from 'components/buttons/IconButton'
import { decrement, increment, length } from 'utils/common'
import { useDispatch } from 'react-redux'
import {
  fetchEditCategory,
  openRemoveModal,
  setCategoryEditItems,
  setEditCategoryActive
} from 'modules/actions/categories'

const useStyles = makeStyles(({ shape }) => ({
  root: {
    ...shape.listItem.root
  },
  side: {
    ...shape.listItem.side
  },
  leftButton: {
    ...shape.listItem.leftButton
  },
  reorderButton: {
    ...shape.listItem.reorderButton
  },
  text: {
    ...shape.listItem.text
  },
  counter: {
    ...shape.listItem.counter
  },
  rightButton: {
    ...shape.listItem.rightButton
  },
  input: {
    ...shape.listItem.input
  },
  buttonHidden: {
    ...shape.listItem.buttonHidden
  }
}))

function CategoryListItem({
  id,
  name,
  priority,
  menuItems,
  isEdit,
  isReorder,
  triggerSave,
  isFirst,
  isLast,
  index,
  handleReorder
}) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const [newName, setNewName] = useState(name)

  const countValue = useMemo(() => length(menuItems), [menuItems])

  const handleRemoveClick = useCallback(() => {
    dispatch(openRemoveModal(id, name))
  }, [dispatch, id, name])

  useEffect(() => {
    if (triggerSave) {
      dispatch(fetchEditCategory(id, { name: newName, priority }))
    }
  }, [dispatch, id, newName, priority, triggerSave])

  const toggleOnEdit = useCallback(() => {
    dispatch(setEditCategoryActive(id))
  }, [dispatch, id])

  const handleNewNameChange = useCallback(
    ({ target }) => {
      setNewName(target.value)
    },
    [setNewName]
  )

  const handleReorderUp = useCallback(() => {
    handleReorder(index, decrement(index))
  }, [index, handleReorder])

  const handleReorderDown = useCallback(() => {
    handleReorder(index, increment(index))
  }, [index, handleReorder])

  const onCaretRightClick = useCallback(() => {
    dispatch(setCategoryEditItems(id))
  }, [dispatch, id])

  const renderRightButton = useMemo(() => {
    if (countValue) {
      return (
        <IconButton
          component={Link}
          to="/place/menu-items"
          className={classes.rightButton}
          onClick={onCaretRightClick}
        >
          <i className="icon-CaretRight" />
        </IconButton>
      )
    }

    return (
      <IconButton className={classes.rightButton} onClick={handleRemoveClick}>
        <i className="icon-Cross" />
      </IconButton>
    )
  }, [countValue, classes, handleRemoveClick, onCaretRightClick])

  if (isEdit) {
    return (
      <ListItem className={classes.root}>
        <InputBase
          type="text"
          className={classes.input}
          value={newName}
          onChange={handleNewNameChange}
          autoFocus
          fullWidth
        />
      </ListItem>
    )
  }

  if (isReorder) {
    return (
      <ListItem className={classes.root}>
        <Grid className={classes.side}>
          <IconButton
            disableRipple
            className={classNames(classes.reorderButton, {
              [classes.buttonHidden]: isFirst
            })}
            onClick={handleReorderUp}
          >
            <i className="icon-ArrowUp" />
          </IconButton>
          <Typography className={classes.text}>{name}</Typography>
        </Grid>
        <Grid className={classes.side}>
          <IconButton
            disableRipple
            className={classNames(classes.reorderButton, {
              [classes.buttonHidden]: isLast
            })}
            onClick={handleReorderDown}
          >
            <i className="icon-ArrowDown" />
          </IconButton>
        </Grid>
      </ListItem>
    )
  }

  return (
    <ListItem className={classes.root}>
      <Grid className={classes.side}>
        <IconButton className={classes.leftButton} onClick={toggleOnEdit}>
          <i className="icon-Pencil" />
        </IconButton>
        <Typography className={classes.text}>{name}</Typography>
      </Grid>
      <Grid className={classes.side}>
        <Typography className={classNames(classes.text, classes.counter)}>
          {countValue}
        </Typography>
        {renderRightButton}
      </Grid>
    </ListItem>
  )
}

CategoryListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  priority: PropTypes.number.isRequired,
  menuItems: PropTypes.array.isRequired,
  isEdit: PropTypes.bool.isRequired,
  isReorder: PropTypes.bool.isRequired,
  editName: PropTypes.string,
  onNameChange: PropTypes.func,
  triggerSave: PropTypes.bool,
  isFirst: PropTypes.bool,
  isLast: PropTypes.bool,
  index: PropTypes.number,
  handleReorder: PropTypes.func
}

CategoryListItem.defaultProps = {
  menuItems: [],
  onNameChange: (f) => f,
  handleReorder: (f) => f
}

export default memo(CategoryListItem)
