import React from 'react'
import classNames from 'classnames'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import PageContainer from 'components/containers/PageContainer'
import ButtonPrimary from 'components/buttons/ButtonPrimary'
import { rgba } from 'utils/styles'
import Language from '../components/language/Language'
import BackButton from '../components/buttons/BackButton'

const useStyles = makeStyles(({ shape }) => ({
  root: {
    marginTop: 70,
    padding: '0 10px',
    position: 'relative',
    margin: '0 auto',
    ...shape.container
  },
  logo: {
    fontSize: 118,
    marginBottom: 28
  },
  title: {
    fontSize: 30,
    fontWeight: 600,
    marginBottom: 20
  },
  text404: {
    fontSize: 100,
    fontWeight: 600,
    color: '#bfbfbf',
    marginBottom: 30
  },
  text: {
    fontSize: 18,
    color: rgba(0, 0, 0, 0.6),
    whiteSpace: 'pre',
    marginBottom: 30
  }
}))

function NotFound() {
  const { t } = useTranslation(['not_found', 'buttons'])
  const classes = useStyles()
  return (
    <PageContainer>
      <BackButton />
      <Language absolute rootClassName={classes.language} />
      <Grid
        container
        justify="center"
        alignItems="center"
        direction="column"
        className={classes.root}
      >
        <i className={classNames('icon-Logo', classes.logo)} />
        <Typography className={classes.title}>
          {t('not_found:title')}
        </Typography>
        <Typography className={classes.text404}>404</Typography>
        <Typography className={classes.text} align="center">
          {t('not_found:text')}
        </Typography>
        <ButtonPrimary component={Link} to="/places/welcome">
          {t('buttons:go_home')}
        </ButtonPrimary>
      </Grid>
    </PageContainer>
  )
}

export default NotFound
