import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import sortBy from 'lodash.sortby'
import { createSelector } from 'reselect'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import { Typography } from '@material-ui/core'
import Snackbar from 'components/notifications/Snackbar'
import BackButton from 'components/buttons/BackButton'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import HeaderButton from 'components/containers/placePageContainer/header/HeaderButton'
import CategoryListItem from './placeCategories/CategoryListItem'
import RemoveModal from './placeCategories/RemoveModal'
import { placeDataSelector } from 'modules/selectors/place'
import {
  editCategorySelector,
  removeModalSelector,
  reorderCategoriesSelector
} from 'modules/selectors/categories'
import {
  clearEditCategory,
  clearRemoveModal,
  clearReorder,
  fetchCategoriesReorder,
  setReorderActive,
  triggerEditSave
} from 'modules/actions/categories'
import {
  assign,
  decrement,
  isEmpty,
  isEqual,
  isSomeTruthy,
  isTruthy,
  length,
  lessThan,
  map,
  swap
} from 'utils/common'
import CircularProgressWrapper from 'components/progresses/CircularProgressWrapper'
import { getCategoryId } from 'utils/menuView'
import { MENU_ITEMS } from 'constants/forms'

const useStyles = makeStyles(({ shape }) => ({
  root: {
    ...shape.container,
    padding: '0 20px 70px 20px'
  },
  list: {
    padding: 0
  },
  empty: {
    textAlign: 'center',
    padding: '250px 0'
  }
}))

function parseMenuItem({ id }) {
  return { id }
}

const getMenuCategories = createSelector(
  placeDataSelector,
  ({ menu_categories: menuCategories }) => {
    const parsedCategories = map(menuCategories, (category) => {
      return assign(category, {
        [MENU_ITEMS]: map(category[MENU_ITEMS], parseMenuItem)
      })
    })
    return sortBy(parsedCategories, 'priority')
  }
)

const getRemoveStatus = createSelector(
  removeModalSelector,
  ({ isSuccess, categoryName }) => ({ isSuccess, categoryName })
)

const getEditCategory = createSelector(
  editCategorySelector,
  ({
    isActive: isEditActive,
    categoryId: editCategoryId,
    isSaveClicked,
    isFetching: isEditFetching,
    isSuccess: isEditSuccess
  }) => ({
    isEditActive,
    editCategoryId,
    isSaveClicked,
    isEditFetching,
    isEditSuccess
  })
)

const getReorderCategories = createSelector(
  reorderCategoriesSelector,
  ({
    isActive: isReorderActive,
    isFetching: isReorderFetching,
    isSuccess: isReorderSuccess
  }) => ({
    isReorderActive,
    isReorderFetching,
    isReorderSuccess
  })
)

function PlaceCategories() {
  const classes = useStyles()
  const { t } = useTranslation(['edit_menu_categories', 'buttons', 'alerts'])
  const dispatch = useDispatch()
  const menuCategories = useSelector(getMenuCategories)
  const { isSuccess, categoryName } = useSelector(getRemoveStatus)
  const {
    isEditActive,
    editCategoryId,
    isSaveClicked,
    isEditFetching,
    isEditSuccess
  } = useSelector(getEditCategory)
  const { isReorderActive, isReorderFetching, isReorderSuccess } = useSelector(
    getReorderCategories
  )
  const [categoriesList, setCategoriesList] = useState([])
  const [snackbar, setSnackbar] = useState({
    show: false,
    message: ''
  })

  useEffect(() => {
    setCategoriesList(menuCategories)
  }, [menuCategories])

  useEffect(() => {
    if (isSomeTruthy(isEditSuccess, isReorderSuccess)) {
      setSnackbar({
        show: true,
        message: t('alerts:successfully_updated')
      })
      if (isEditSuccess) {
        dispatch(clearEditCategory())
      }
      if (isReorderSuccess) {
        dispatch(clearReorder())
      }
    }
  }, [isEditSuccess, isReorderSuccess, setSnackbar, dispatch, t])

  useEffect(() => {
    if (isSuccess) {
      setSnackbar({
        show: true,
        message: t('alerts:menu_category_remove_success', { categoryName })
      })
      dispatch(clearRemoveModal())
    }
  }, [isSuccess, categoryName, setSnackbar, t, dispatch])

  const handleSnackbarClose = useCallback(() => {
    setSnackbar((prevState) => assign(prevState, { show: false }))
  }, [setSnackbar])

  const handleCancelClick = useCallback(() => {
    if (isEditActive) {
      dispatch(clearEditCategory())
    }
    if (isReorderActive) {
      setCategoriesList(menuCategories)
      dispatch(clearReorder())
    }
  }, [dispatch, isReorderActive, isEditActive, menuCategories])

  const handleSaveClick = useCallback(() => {
    if (isEditActive) {
      dispatch(triggerEditSave())
    }
    if (isReorderActive) {
      dispatch(fetchCategoriesReorder(map(categoriesList, getCategoryId)))
    }
  }, [dispatch, isReorderActive, isEditActive, categoriesList])

  const handleReorderClick = useCallback(() => {
    dispatch(setReorderActive())
  }, [dispatch])

  const handleReorder = useCallback(
    (from, to) => {
      setCategoriesList((prevState) => swap(prevState, from, to))
    },
    [setCategoriesList]
  )

  const renderLeftHeaderButton = useMemo(() => {
    if (isSomeTruthy(isEditActive, isReorderActive)) {
      return (
        <HeaderButton onClick={handleCancelClick}>
          {t('buttons:cancel')}
        </HeaderButton>
      )
    }

    return <BackButton relative to="/place/menu" />
  }, [isEditActive, handleCancelClick, isReorderActive, t])

  const renderRightHeaderButton = useMemo(() => {
    if (isSomeTruthy(isEditActive, isReorderActive)) {
      return (
        <HeaderButton onClick={handleSaveClick}>
          <CircularProgressWrapper
            isLoading={isSomeTruthy(isEditFetching, isReorderFetching)}
            dark
          >
            {t('buttons:save')}
          </CircularProgressWrapper>
        </HeaderButton>
      )
    }

    return (
      <HeaderButton
        disabled={lessThan(length(categoriesList), 2)}
        onClick={handleReorderClick}
      >
        {t('buttons:reorder')}
      </HeaderButton>
    )
  }, [
    categoriesList,
    isEditActive,
    handleSaveClick,
    isEditFetching,
    isReorderActive,
    isReorderFetching,
    handleReorderClick,
    t
  ])

  return (
    <PlacePageContainer
      title={t('edit_menu_categories:heading')}
      headerLeftComponent={renderLeftHeaderButton}
      headerRightComponent={renderRightHeaderButton}
      footerActiveLeft
    >
      <Snackbar
        open={snackbar.show}
        message={snackbar.message}
        onClose={handleSnackbarClose}
        variant="success"
      />
      <RemoveModal />
      <Grid container direction="column" className={classes.root}>
        {!isEmpty(categoriesList) ? (
          <List className={classes.list}>
            {map(
              categoriesList,
              ({ id, name, menu_items: menuItems, priority }, index) => {
                const isEdit = isEqual(id, editCategoryId)
                const triggerSave = isTruthy(isEdit, isSaveClicked)
                const isFirst = isEqual(index, 0)
                const isLast = isEqual(index, decrement(length(menuCategories)))
                return (
                  <CategoryListItem
                    key={id}
                    id={id}
                    name={name}
                    priority={priority}
                    menuItems={menuItems}
                    isEdit={isEdit}
                    isReorder={isReorderActive}
                    triggerSave={triggerSave}
                    isFirst={isFirst}
                    isLast={isLast}
                    index={index}
                    handleReorder={handleReorder}
                  />
                )
              }
            )}
          </List>
        ) : (
          <Typography className={classes.empty}>
            {t('edit_menu_categories:empty-list')}
          </Typography>
        )}
      </Grid>
    </PlacePageContainer>
  )
}

export default PlaceCategories
