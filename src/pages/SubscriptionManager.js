import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'
import IconButton from 'components/buttons/IconButton'
import SubscriptionCard from 'components/cards/SubscriptionCard'
import Agreement from 'components/typographies/Agreement'
import ButtonPrimary from 'components/buttons/ButtonPrimary'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import useSubscriptionPlans from 'hooks/useSubscriptionPlans'
import { map } from 'utils/common'
import usePaymentsInfo from 'hooks/usePaymentsInfo'
import { isPlanActive } from 'utils/subscription'

const useStyles = makeStyles({
  root: {
    padding: '30px 21px 78px 21px'
  },
  method: {
    marginBottom: 16
  },
  methodText: {
    fontSize: 12,
    color: '#a9a9a9'
  },
  addMethodButton: {
    width: 34,
    height: 34,
    fontSize: 12,
    border: '1px solid #000',
    '& i': {
      '&::before': {
        color: '#000'
      }
    }
  },
  divider: {
    marginBottom: 21
  },
  title: {
    fontSize: 14,
    marginBottom: 17
  },
  cards: {
    marginBottom: 24
  },
  button: {
    marginBottom: 20
  }
})

function SubscriptionManager() {
  const { t } = useTranslation([
    'subscription_manager',
    'subscription_plans',
    'buttons'
  ])
  const classes = useStyles()
  const plans = useSubscriptionPlans()
  const { isActiveTrial } = usePaymentsInfo()

  return (
    <PlacePageContainer title={t('subscription_manager:title')}>
      <Grid container direction="column" className={classes.root}>
        <Grid
          container
          justify="space-between"
          alignItems="center"
          className={classes.method}
        >
          <Typography className={classes.methodText}>
            {t('subscription_manager:no_payment_method')}
          </Typography>
          <IconButton className={classes.addMethodButton}>
            <i className="icon-Plus" />
          </IconButton>
        </Grid>
        <Divider className={classes.divider} />
        <Typography className={classes.title} align="center">
          {t('subscription_plans:title')}
        </Typography>
        <Grid container direction="column" className={classes.cards}>
          {map(plans, ({ id, terms_count: termsCount, code, ...plan }) => {
            return (
              <SubscriptionCard
                key={id}
                code={code}
                termsCount={termsCount}
                active={isPlanActive(id, code, isActiveTrial)}
                {...plan}
              />
            )
          })}
        </Grid>
        <ButtonPrimary
          className={classes.button}
          component={Link}
          to="/subscription/compare"
        >
          {t('buttons:compare_plans')}
        </ButtonPrimary>
        <Agreement />
      </Grid>
    </PlacePageContainer>
  )
}

export default SubscriptionManager
