import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { createSelector } from 'reselect'
import { Redirect, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { useFormik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Typography from '@material-ui/core/Typography'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import BackButton from 'components/buttons/BackButton'
import HeaderButton from 'components/containers/placePageContainer/header/HeaderButton'
import FormControlMuiInput from 'components/form/FormControlMuiInput'
import MediaUpload from './addEditItem/MediaUpload'
import Form from 'components/form/Form'
import FormControlsBuilder from 'components/builders/FormControlsBuilder'
import CircularProgressWrapper from 'components/progresses/CircularProgressWrapper'
import Snackbar from 'components/notifications/Snackbar'
import Category from './addEditItem/Category'
import { buildForm, createControlNames } from 'utils/forms'
import {
  editMenuItemItemSelector,
  menuItemMediaSelector,
  menuItemSelector
} from 'modules/selectors/menuItem'
import {
  clearMenuItem,
  fetchCreateMenuItem,
  fetchEditMenuItem,
  fetchGetMenuItem
} from 'modules/actions/menuItem'
import { createPostMenuItemBody, createPutMenuItemBody } from 'utils/api'
import {
  map,
  assign,
  isFalsy,
  isSomeTruthy,
  concat,
  isTruthy,
  isEmpty,
  find,
  isEqual,
  isNotEmpty
} from 'utils/common'
import {
  errorCode,
  errorMessages,
  getMenuCategoryId,
  isFetching,
  isMediaFailure,
  isSuccess,
  serverErrors
} from 'utils/menuItem'
import {
  ACTIVE,
  DESCRIPTION,
  MENU_CATEGORY,
  MENU_ITEM_NAME,
  PLACEHOLDER_MENU_ITEM_DESCRIPTION,
  PLACEHOLDER_MENU_ITEM_NAME,
  PRICE
} from 'constants/forms'
import { is422 } from 'utils/errors'
import { placeDataSelector } from 'modules/selectors/place'
import { parseCategory } from 'utils/menuView'
import { getName } from '../utils/object'
import LoadingOverlay from '../components/progresses/LoadingOverlay'

const useStyles = makeStyles(() => ({
  root: {
    padding: '10px 30px'
  },
  inputContainer: {
    marginBottom: 27
  }
}))

const controlNames = createControlNames(
  ACTIVE,
  MENU_ITEM_NAME,
  DESCRIPTION,
  PRICE,
  MENU_CATEGORY
)
const {
  controls,
  initialValues,
  validationSchema,
  validateOnMount
} = buildForm(controlNames)
const overrides = {
  disabledLabels: true,
  placeholders: {
    [MENU_ITEM_NAME]: PLACEHOLDER_MENU_ITEM_NAME,
    [DESCRIPTION]: PLACEHOLDER_MENU_ITEM_DESCRIPTION
  }
}

const getMenuItemStatus = createSelector(
  menuItemSelector,
  ({ getItem, createItem, editItem, media }) => {
    const { isFetching: isGetFetching } = getItem
    const { error: createError } = createItem
    const { isEdit, error: editError } = editItem
    return {
      isEdit,
      isGetFetching,
      isSuccess: isSuccess({ createItem, editItem, media }),
      isFetching: isFetching({ createItem, editItem, media }),
      isMediaFailure: isMediaFailure(media),
      serverErrors: serverErrors(createError, editError),
      errorMessage: errorMessages(createError, editError),
      errorCode: errorCode(createError, editError)
    }
  }
)

const getMenuItemMedia = createSelector(menuItemMediaSelector, ({ files }) => ({
  files: map(files, ({ file, id }) => ({ file, id }))
}))

const getEditMenuItem = createSelector(
  editMenuItemItemSelector,
  ({ id, ...item }) => ({
    item,
    itemId: id
  })
)

const getCategoriesItems = createSelector(
  placeDataSelector,
  ({ menu_categories: menuCategories }) => {
    const parsedCategories = map(menuCategories, parseCategory)
    return {
      categories: parsedCategories,
      isEmptyCategories: isEmpty(parsedCategories)
    }
  }
)

const TIMER_TO_CLOSE = 1000

function AddEditItem() {
  const { t } = useTranslation(['add_edit_item_page', 'alerts', 'buttons'])
  const classes = useStyles()
  const { id } = useParams()
  const dispatch = useDispatch()
  const {
    isEdit,
    isGetFetching,
    isSuccess,
    isFetching,
    isMediaFailure,
    serverErrors,
    errorMessage,
    errorCode
  } = useSelector(getMenuItemStatus)
  const { files } = useSelector(getMenuItemMedia)
  const { itemId, item } = useSelector(getEditMenuItem)
  const { categories, isEmptyCategories } = useSelector(getCategoriesItems)
  const [errorSnackbar, setErrorSnackbar] = useState(false)
  const [errorSnackbarMessage, setErrorSnackbarMessage] = useState('')
  const [isRedirect, setRedirect] = useState(false)
  const [isCategoryOpen, setCategoryOpen] = useState(false)

  useEffect(() => {
    if (id) {
      dispatch(fetchGetMenuItem(id))
    }
  }, [id, dispatch])

  const showErrorSnackbar = useCallback(
    (message) => {
      setErrorSnackbar(true)
      setErrorSnackbarMessage(message)
    },
    [setErrorSnackbar, setErrorSnackbarMessage]
  )

  const toggleCategoryOpen = useCallback(() => {
    setCategoryOpen(isFalsy)
  }, [setCategoryOpen])

  const onSubmit = useCallback(
    (values) => {
      if (isMediaFailure) {
        return showErrorSnackbar(t('alerts:media_upload_fail'))
      }
      if (isEdit) {
        return dispatch(
          fetchEditMenuItem(
            itemId,
            createPutMenuItemBody(values),
            getMenuCategoryId(item)
          )
        )
      }
      return dispatch(
        fetchCreateMenuItem(createPostMenuItemBody(values), files)
      )
    },
    [
      t,
      item,
      files,
      isEdit,
      itemId,
      dispatch,
      isMediaFailure,
      showErrorSnackbar
    ]
  )

  useEffect(() => {
    if (isTruthy(errorMessage, isFalsy(is422(errorCode)))) {
      showErrorSnackbar(errorMessage)
    }
  }, [errorMessage, showErrorSnackbar, errorCode])

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    submitForm,
    setValues,
    isValid
  } = useFormik({
    initialValues,
    validationSchema,
    validateOnMount,
    onSubmit
  })

  const { menu_category_id: categoryId } = values

  const categoryName = useMemo(() => {
    if (isTruthy(isNotEmpty(categories), categoryId)) {
      return getName(find(categories, ({ id }) => isEqual(id, categoryId)))
    }
    return ''
  }, [categories, categoryId])

  useEffect(() => {
    if (isMediaFailure) {
      showErrorSnackbar(t('alerts:media_upload_fail'))
    }
  }, [isMediaFailure, showErrorSnackbar, t])

  useEffect(() => {
    setValues(assign(initialValues, item))
  }, [setValues, item])

  useEffect(() => {
    if (isSuccess) {
      const timer = setTimeout(() => {
        setRedirect(true)
      }, TIMER_TO_CLOSE)

      return () => {
        clearInterval(timer)
      }
    }
  }, [isSuccess, setRedirect])

  useEffect(() => {
    return () => dispatch(clearMenuItem())
  }, [dispatch])

  const handleSnackbarClose = useCallback(() => {
    setErrorSnackbar(false)
  }, [setErrorSnackbar])

  const disabled = useMemo(() => {
    return isSomeTruthy(isFalsy(isValid), isFetching, isSuccess)
  }, [isValid, isFetching, isSuccess])

  const renderCategory = useMemo(() => {
    return (
      isCategoryOpen && (
        <Category
          categories={categories}
          isEmptyCategories={isEmptyCategories}
          onClose={toggleCategoryOpen}
          onChange={handleChange}
        />
      )
    )
  }, [
    isCategoryOpen,
    handleChange,
    toggleCategoryOpen,
    categories,
    isEmptyCategories
  ])

  const contentZIndex = useMemo(() => {
    return isCategoryOpen ? 10 : 1
  }, [isCategoryOpen])

  const redirectUrl = useMemo(() => {
    return concat('/place/menu#', itemId)
  }, [itemId])

  if (isSomeTruthy(isRedirect)) {
    return <Redirect to={redirectUrl} />
  }

  return (
    <PlacePageContainer
      title={t('add_edit_item_page:title')}
      footer={false}
      headerDivider
      backgroundColor="secondary"
      contentZIndex={contentZIndex}
      headerLeftComponent={<BackButton relative to={redirectUrl} />}
      headerRightComponent={
        <HeaderButton onClick={submitForm} disabled={disabled}>
          <CircularProgressWrapper isLoading={isFetching} dark>
            <Typography>{t('buttons:save')}</Typography>
          </CircularProgressWrapper>
        </HeaderButton>
      }
    >
      {isGetFetching && <LoadingOverlay independent />}
      <Snackbar
        variant="error"
        open={errorSnackbar}
        message={errorSnackbarMessage}
        onClose={handleSnackbarClose}
      />
      <div className={classes.root}>
        <MediaUpload />
        <Form onSubmit={handleSubmit}>
          <FormControlsBuilder
            groupName="add-edit-item"
            controls={controls}
            inputTextComponent={FormControlMuiInput}
            overrides={overrides}
            values={values}
            errors={assign(serverErrors, errors)}
            touched={touched}
            onChange={handleChange}
            onBlur={handleBlur}
            formControlInputProps={{
              containerClassName: classes.inputContainer
            }}
            formControlCategoryProps={{
              onClick: toggleCategoryOpen,
              value: categoryName
            }}
          />
        </Form>
      </div>
      {renderCategory}
    </PlacePageContainer>
  )
}

export default AddEditItem
