import React from 'react'
import sortBy from 'lodash.sortby'
import { useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import Grid from '@material-ui/core/Grid'
import Categories from 'components/menu/Categories'
import Placeholder from './Placeholder'
import MenuView from 'components/menu/MenuView'
import { placeDataSelector } from 'modules/selectors/place'
import { isEmpty, map } from 'utils/common'
import { filterCategories } from 'utils/visitorMenu'
import { parseHeaderCategories } from 'utils/menuView'

const getMenu = createSelector(
  placeDataSelector,
  ({ menu_categories: menuCategories }) => {
    const filteredMenu = filterCategories(menuCategories)
    const sortedCategories = sortBy(filteredMenu, 'priority')
    return {
      isEmptyMenu: isEmpty(filteredMenu),
      categories: sortedCategories,
      headerCategories: map(sortedCategories, parseHeaderCategories)
    }
  }
)

function Menu() {
  const { isEmptyMenu, categories, headerCategories } = useSelector(getMenu)

  if (isEmptyMenu) {
    return <Placeholder />
  }

  return (
    <Grid container direction="column">
      <Categories categories={headerCategories} />
      <MenuView categories={categories} />
    </Grid>
  )
}

export default Menu
