import React, { useMemo } from 'react'
import Grid from '@material-ui/core/Grid'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import classNames from 'classnames'
import { percent } from 'utils/styles'
import HeaderButton from 'components/containers/placePageContainer/header/HeaderButton'
import Language from 'components/language/Language'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { placeDataSelector } from 'modules/selectors/place'
import { concat } from '../../utils/common'

const useStyles = makeStyles({
  root: {
    height: percent(100)
  },
  button: {
    '& i': {
      '&::before': {
        color: '#000'
      }
    }
  },
  leftButton: {
    '& i': {
      fontSize: 8
    }
  },
  rightButton: {
    '& i': {
      fontSize: 12
    }
  },
  buttonText: {
    fontWeight: 500,
    marginRight: 5
  }
})

const getPlaceData = createSelector(placeDataSelector, ({ name, slug }) => ({
  name,
  slug
}))

function Header() {
  const classes = useStyles()
  const { name, slug } = useSelector(getPlaceData)

  const redirectLink = useMemo(() => {
    return concat('/', slug, '/info')
  }, [slug])

  return (
    <Grid
      container
      justify="space-between"
      alignItems="center"
      className={classes.root}
    >
      <Grid>
        <HeaderButton
          className={classNames(classes.button, classes.leftButton)}
          component={Link}
          to={redirectLink}
        >
          <Typography className={classes.buttonText}>{name}</Typography>
          <i className="icon-CaretDown" />
        </HeaderButton>
      </Grid>
      <Grid>
        <Language />
      </Grid>
    </Grid>
  )
}

export default Header
