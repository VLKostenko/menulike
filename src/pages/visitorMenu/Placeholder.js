import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import React from 'react'
import { useTranslation } from 'react-i18next'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { rgba } from 'utils/styles'

const useStyles = makeStyles({
  root: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0
  },
  text: {
    color: rgba(0, 0, 0, 0.6)
  }
})

function Placeholder() {
  const { t } = useTranslation('visitor_menu')
  const classes = useStyles()

  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.root}
    >
      <Typography className={classes.text}>
        {t('visitor_menu:menu_is_empty')}
      </Typography>
    </Grid>
  )
}

export default Placeholder
