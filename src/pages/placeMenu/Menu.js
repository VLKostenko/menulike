import React from 'react'
import { createSelector } from 'reselect'
import { useSelector } from 'react-redux'
import sortBy from 'lodash.sortby'
import Grid from '@material-ui/core/Grid'
import Categories from 'components/menu/Categories'
import MenuView from 'components/menu/MenuView'
import RemoveItemModal from 'components/menu/RemoveItemModal'
import { placeDataSelector } from 'modules/selectors/place'
import { filter, map } from 'utils/common'
import { filterCategories, parseHeaderCategories } from 'utils/menuView'

const getMenuCategories = createSelector(
  placeDataSelector,
  ({ menu_categories: menuCategories }) => {
    const filteredCategories = filter(menuCategories, filterCategories)
    const sortedCategories = sortBy(filteredCategories, 'priority')
    return {
      categories: sortedCategories,
      headerCategories: map(sortedCategories, parseHeaderCategories)
    }
  }
)

function Menu() {
  const { categories, headerCategories } = useSelector(getMenuCategories)
  return (
    <Grid container direction="column">
      <RemoveItemModal />
      <Categories categories={headerCategories} />
      <MenuView categories={categories} />
    </Grid>
  )
}

export default Menu
