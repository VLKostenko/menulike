import React, { memo } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { Link } from 'react-router-dom'
import { percent, px } from 'utils/styles'
import usePaymentsInfo from '../../hooks/usePaymentsInfo'

const useStyles = makeStyles(({ palette }) => ({
  title: {
    fontWeight: 500,
    fontSize: 17,
    lineHeight: px(20),
    position: 'relative',
    textDecoration: 'none',
    color: '#000'
  },
  titleIcon: {
    position: 'absolute',
    top: 0,
    width: 18,
    height: 18
  },
  iconLeft: {
    background: ({ variant }) => palette.fab.warning.background[variant],
    left: -25,
    borderRadius: percent(50),
    fontSize: 10
  },
  iconRight: {
    right: -25,
    top: 3,
    fontSize: 10,
    '& i': {
      '&::before': {
        color: '#000'
      }
    }
  }
}))

function HeaderTitle({ name }) {
  const { variant } = usePaymentsInfo()
  const classes = useStyles({ variant })
  return (
    <Grid container justify="center">
      <Typography className={classes.title} component={Link} to="/place/edit">
        <Grid
          container
          justify="center"
          alignItems="center"
          className={classNames(classes.titleIcon, classes.iconLeft)}
        >
          <i className="icon-Alert" />
        </Grid>
        {name}
        <Grid
          container
          justify="center"
          alignItems="center"
          className={classNames(classes.titleIcon, classes.iconRight)}
        >
          <i className="icon-CaretDown" />
        </Grid>
      </Typography>
    </Grid>
  )
}

HeaderTitle.propTypes = {
  name: PropTypes.string
}

export default memo(HeaderTitle)
