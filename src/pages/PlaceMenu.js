import React, { useCallback, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import { useTranslation } from 'react-i18next'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import Snackbar from 'components/notifications/Snackbar'
import IconButton from 'components/buttons/IconButton'
import Menu from './placeMenu/Menu'
import Placeholder from './placeMenu/Placeholder'
import { placeDataSelector } from 'modules/selectors/place'
import { isEmptyCategories } from 'utils/menuView'
import { clearRemoveModal } from 'modules/actions/menuItem'
import { assign } from 'utils/common'
import { removeModalSelector } from 'modules/selectors/menuItem'
import HeaderTitle from './placeMenu/HederTitle'

const getMenuData = createSelector(
  placeDataSelector,
  ({ name, menu_categories }) => ({
    name,
    isEmptyCategories: isEmptyCategories(menu_categories)
  })
)

const getRemoveModal = createSelector(
  removeModalSelector,
  ({ isSuccess, itemName }) => ({ isSuccess, itemName })
)

function PlaceMenu() {
  const { t } = useTranslation(['place_menu_page', 'alerts'])
  const dispatch = useDispatch()
  const { name, isEmptyCategories } = useSelector(getMenuData)
  const { isSuccess, itemName } = useSelector(getRemoveModal)
  const [snackbar, setSnackbar] = useState({ show: false, message: '' })

  useEffect(() => {
    if (isSuccess) {
      setSnackbar({
        show: true,
        message: t('alerts:menu_item_remove_success', { itemName })
      })
      dispatch(clearRemoveModal())
    }
  }, [isSuccess, itemName, setSnackbar, t, dispatch])

  const handleSnackbarClose = useCallback(() => {
    setSnackbar((prevState) => assign(prevState, { show: false }))
  }, [setSnackbar])

  return (
    <PlacePageContainer
      footerActiveLeft
      headerTitleComponent={<HeaderTitle name={name} />}
      headerLeftComponent={
        <IconButton component={Link} to="/place/categories">
          <i className="icon-Pencil" />
        </IconButton>
      }
      headerRightComponent={
        <IconButton component={Link} to="/place/share">
          <i className="icon-Share" />
        </IconButton>
      }
      contentContainerId="place-menu_container"
    >
      <Snackbar
        open={snackbar.show}
        message={snackbar.message}
        variant="success"
        onClose={handleSnackbarClose}
      />
      {isEmptyCategories ? <Placeholder /> : <Menu />}
    </PlacePageContainer>
  )
}

export default PlaceMenu
