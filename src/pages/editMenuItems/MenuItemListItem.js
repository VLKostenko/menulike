import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import ListItem from '@material-ui/core/ListItem'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import IconButton from 'components/buttons/IconButton'
import { concat, decrement, increment } from 'utils/common'
import { openRemoveModal } from 'modules/actions/menuItem'
import { toPrice } from '../../utils/price'

const useStyles = makeStyles(({ shape }) => ({
  root: {
    ...shape.listItem.root
  },
  side: {
    ...shape.listItem.side
  },
  leftButton: {
    ...shape.listItem.leftButton
  },
  reorderButton: {
    ...shape.listItem.reorderButton
  },
  text: {
    ...shape.listItem.text
  },
  price: {
    ...shape.listItem.counter
  },
  rightButton: {
    ...shape.listItem.rightButton
  },
  input: {
    ...shape.listItem.input
  },
  buttonHidden: {
    ...shape.listItem.buttonHidden
  }
}))

function MenuItemListItem({
  id,
  name,
  price,
  currencySymbol,
  categoryId,
  isReorder,
  isFirst,
  isLast,
  handleReorder,
  index
}) {
  const classes = useStyles()
  const dispatch = useDispatch()

  const handleRemoveClick = useCallback(() => {
    dispatch(openRemoveModal(id, name, categoryId))
  }, [dispatch, id, name, categoryId])

  const handleReorderUpClick = useCallback(() => {
    handleReorder(index, decrement(index))
  }, [handleReorder, index])

  const handleReorderDownClick = useCallback(() => {
    handleReorder(index, increment(index))
  }, [handleReorder, index])

  if (isReorder) {
    return (
      <ListItem className={classes.root}>
        <Grid className={classes.side}>
          <IconButton
            disableRipple
            className={classNames(classes.reorderButton, {
              [classes.buttonHidden]: isFirst
            })}
            onClick={handleReorderUpClick}
          >
            <i className="icon-ArrowUp" />
          </IconButton>
          <Typography className={classes.text}>{name}</Typography>
        </Grid>
        <Grid className={classes.side}>
          <IconButton
            disableRipple
            className={classNames(classes.reorderButton, {
              [classes.buttonHidden]: isLast
            })}
            onClick={handleReorderDownClick}
          >
            <i className="icon-ArrowDown" />
          </IconButton>
        </Grid>
      </ListItem>
    )
  }

  return (
    <ListItem className={classes.root}>
      <Grid className={classes.side}>
        <IconButton
          className={classes.leftButton}
          component={Link}
          to={concat('/place/menu/item/edit/', id)}
        >
          <i className="icon-Pencil" />
        </IconButton>
        <Typography className={classes.text}>{name}</Typography>
      </Grid>
      <Grid className={classes.side}>
        <Typography className={classNames(classes.text, classes.price)}>
          {concat(currencySymbol, toPrice(price))}
        </Typography>
        <IconButton className={classes.rightButton} onClick={handleRemoveClick}>
          <i className="icon-Cross" />
        </IconButton>
      </Grid>
    </ListItem>
  )
}

MenuItemListItem.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  currencySymbol: PropTypes.string,
  categoryId: PropTypes.string,
  isReorder: PropTypes.bool,
  isFirst: PropTypes.bool,
  isLast: PropTypes.bool,
  handleReorder: PropTypes.func,
  index: PropTypes.number
}

MenuItemListItem.defaultProps = {
  handleReorder: (f) => f
}

export default MenuItemListItem
