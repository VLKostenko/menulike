import React from 'react'
import { useTranslation } from 'react-i18next'
import makeStyles from '@material-ui/core/styles/makeStyles'

import PlacePageContainer from 'components/containers/PlacePageContainer'
import BackButton from 'components/buttons/BackButton'
import Buttons from './placeShare/Buttons'
import QR from './placeShare/QR'

const useStyles = makeStyles(({ shape }) => ({
  container: {
    padding: '25px 50px 35px 50px',
    ...shape.container
  }
}))

function PlaceShare() {
  const { t } = useTranslation(['place_share_page', 'alerts'])
  const classes = useStyles()

  return (
    <PlacePageContainer
      title={t('place_share_page:heading')}
      headerLeftComponent={<BackButton relative to="/places/menu" />}
      footerActiveRight
    >
      <div className={classes.container}>
        <QR />
        <Buttons />
      </div>
    </PlacePageContainer>
  )
}
export default PlaceShare
