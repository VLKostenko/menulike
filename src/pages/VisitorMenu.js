import React, { useEffect } from 'react'
import { createSelector } from 'reselect'
import { useParams, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import Header from './visitorMenu/Header'
import Menu from './visitorMenu/Menu'
import { fetchPlaceBySlug } from 'modules/actions/places'
import { setIsVisitor } from 'modules/actions/app'
import { placeBySlugSelector } from 'modules/selectors/places'
import { isFalsy, isTruthy } from 'utils/common'
import { placeDataSelector } from 'modules/selectors/place'
import NotFound from './NotFound'

const getPlaceBySlug = createSelector(
  placeBySlugSelector,
  ({ isFetching, isSuccess, isFailure }) => ({
    isFetching,
    isSuccess,
    isFailure
  })
)

const getPlaceSlug = createSelector(placeDataSelector, ({ slug }) => slug)

function VisitorMenu() {
  const { slug } = useParams()
  const { replace: replaceUrl } = useHistory()
  const dispatch = useDispatch()
  const { isFetching, isSuccess, isFailure } = useSelector(getPlaceBySlug)
  const placeSlug = useSelector(getPlaceSlug)

  useEffect(() => {
    if (placeSlug) {
      replaceUrl(placeSlug)
    }
  }, [replaceUrl, placeSlug])

  useEffect(() => {
    if (isTruthy(slug, isFalsy(isFetching, isSuccess, isFailure))) {
      dispatch(setIsVisitor())
      dispatch(fetchPlaceBySlug(slug))
    }
  }, [dispatch, slug, isFetching, isSuccess, isFailure])

  if (isFailure) {
    return <NotFound />
  }

  return (
    <PlacePageContainer headerContent={<Header />} footer={false}>
      <Menu />
    </PlacePageContainer>
  )
}

export default VisitorMenu
