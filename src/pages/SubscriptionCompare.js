import React from 'react'
import { ScrollSync } from 'react-scroll-sync'
import { useTranslation } from 'react-i18next'
import { createSelector } from 'reselect'
import { useSelector } from 'react-redux'
import sortBy from 'lodash.sortby'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import ButtonPrimary from 'components/buttons/ButtonPrimary'
import SubscriptionFeaturesCard from 'components/cards/SubscriptionFeaturesCard'
import FeaturesPack from './subscriptionCompare/FeaturesPack'
import { isEqual, map } from 'utils/common'
import {
  subscriptionFeaturesSelector,
  subscriptionPlansSelector
} from 'modules/selectors/subscription'
import useGoBack from 'hooks/useGoBack'

const useStyles = makeStyles({
  cardsContainer: {
    // width: 'calc(100% + 20px)',
    marginBottom: 16
  },
  button: {
    marginBottom: 16
  }
})

const getPlans = createSelector(subscriptionPlansSelector, (plans) => {
  return sortBy(plans, 'priority')
})

const getFeatures = createSelector(subscriptionFeaturesSelector, (features) => {
  return sortBy(features, 'priority')
})

function SubscriptionCompare() {
  const { t } = useTranslation('buttons')
  const classes = useStyles()
  const plans = useSelector(getPlans)
  const allFeatures = useSelector(getFeatures)
  const goBack = useGoBack()

  return (
    <Grid container direction="column">
      <ScrollSync>
        <Grid container direction="column" className={classes.cardsContainer}>
          <FeaturesPack features={allFeatures} />
          {map(
            plans,
            ({
              id,
              name,
              price,
              term,
              terms_count: termsCount,
              currency,
              code,
              features
            }) =>
              !isEqual(code, 'TRIAL1') ? (
                <SubscriptionFeaturesCard
                  key={id}
                  code={code}
                  name={name}
                  price={price}
                  term={term}
                  termsCount={termsCount}
                  currency={currency}
                  allFeatures={allFeatures}
                  features={features}
                />
              ) : (
                ''
              )
          )}
        </Grid>
      </ScrollSync>
      <ButtonPrimary className={classes.button} onClick={goBack}>
        {t('buttons:purchase_plan')}
      </ButtonPrimary>
    </Grid>
  )
}

export default SubscriptionCompare
