import React from 'react'
import PropTypes from 'prop-types'
import { ScrollSyncPane } from 'react-scroll-sync'
import Grid from '@material-ui/core/Grid'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { px, rgba } from 'utils/styles'
import { map } from 'utils/common'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles({
  root: {
    maxWidth: 'calc(100% - 91px)',
    height: 70,
    overflow: 'auto',
    marginBottom: 5,
    '&::-webkit-scrollbar': {
      display: 'none'
    }
  },
  feature: {
    width: 100,
    minWidth: 100,
    height: 70,
    paddingLeft: 5,
    paddingRight: 5,
    background: rgba(255, 255, 255, 0.5),
    '&:nth-child(2n)': {
      background: rgba(255, 255, 255, 0.3)
    }
  },
  text: {
    fontSize: 12,
    transformOrigin: '0',
    textAlign: 'center',
    lineHeight: px(12)
  }
})

function FeaturesPack({ features }) {
  const classes = useStyles()
  return (
    <Grid container justify="flex-end">
      <ScrollSyncPane>
        <Grid container className={classes.root} wrap="nowrap">
          {map(features, ({ name, id }) => (
            <Grid
              container
              justify="center"
              alignItems="center"
              key={id}
              className={classes.feature}
            >
              <Typography className={classes.text}>{name}</Typography>
            </Grid>
          ))}
        </Grid>
      </ScrollSyncPane>
    </Grid>
  )
}

FeaturesPack.propTypes = {
  features: PropTypes.array
}

export default FeaturesPack
