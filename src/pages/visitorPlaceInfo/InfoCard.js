import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'

const useStyles = makeStyles({
  root: {
    marginBottom: 8
  },
  title: {
    fontWeight: 500
  },
  rightContainer: {
    paddingLeft: 10
  },
  description: {
    color: '#000'
  }
})

function InfoCard({ title, description, link }) {
  const classes = useStyles()
  return (
    <Grid container className={classes.root}>
      <Grid item xs={3} container justify="flex-end">
        <Typography className={classes.title}>{title}:</Typography>
      </Grid>
      <Grid item xs={9} className={classes.rightContainer}>
        {link ? (
          <Typography
            className={classes.description}
            component="a"
            href={link}
            target="_blank"
          >
            {description}
          </Typography>
        ) : (
          <Typography className={classes.description}>{description}</Typography>
        )}
      </Grid>
    </Grid>
  )
}

InfoCard.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  link: PropTypes.string
}

export default InfoCard
