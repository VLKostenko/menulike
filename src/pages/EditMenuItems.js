import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'
import sortBy from 'lodash.sortby'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { createSelector } from 'reselect'
import { useTranslation } from 'react-i18next'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import BackButton from 'components/buttons/BackButton'
import HeaderButton from 'components/containers/placePageContainer/header/HeaderButton'
import RemoveItemModal from 'components/menu/RemoveItemModal'
import {
  clearCategoryEditItems,
  clearEditItemsReorder,
  fetchEditItemsReorder,
  setEditItemsReorderActive
} from 'modules/actions/categories'
import MenuItemListItem from './editMenuItems/MenuItemListItem'
import {
  find,
  isEqual,
  map,
  isEmpty,
  assign,
  decrement,
  length,
  swap,
  lessThan
} from 'utils/common'
import { getMenuItems } from 'utils/menuView'
import {
  menuCategoriesSelector,
  placeDataSelector
} from 'modules/selectors/place'
import { removeModalSelector } from 'modules/selectors/menuItem'
import {
  categoryEditItemsSelector,
  categoryIdEditItemsSelector,
  editItemsReorderSelector
} from 'modules/selectors/categories'
import { clearRemoveModal } from 'modules/actions/menuItem'
import Snackbar from 'components/notifications/Snackbar'
import CircularProgressWrapper from 'components/progresses/CircularProgressWrapper'
import { getId } from 'utils/object'

const useStyles = makeStyles(({ shape }) => ({
  root: {
    ...shape.container,
    padding: '0 20px 70px 20px'
  },
  list: {
    padding: 0
  }
}))

const getCategoryEditItems = createSelector(
  categoryIdEditItemsSelector,
  menuCategoriesSelector,
  (categoryId, menuCategories) => {
    if (categoryId) {
      const menuItems = getMenuItems(
        find(menuCategories, (category) => {
          return isEqual(category.id, categoryId)
        })
      )
      return sortBy(menuItems, 'priority')
    }
    return []
  }
)

const getCategoryId = createSelector(
  categoryEditItemsSelector,
  ({ categoryId }) => categoryId
)

const getCurrencySymbol = createSelector(
  placeDataSelector,
  ({ currency_symbol: currencySymbol }) => currencySymbol
)

const getRemoveModal = createSelector(
  removeModalSelector,
  ({ isSuccess, itemName }) => ({ isSuccess, itemName })
)

const getEditItemsReorder = createSelector(
  editItemsReorderSelector,
  ({
    isActive: isReorderActive,
    isFetching: isReorderFetching,
    isSuccess: isReorderSuccess
  }) => ({
    isReorderActive,
    isReorderFetching,
    isReorderSuccess
  })
)

function EditMenuItems() {
  const { t } = useTranslation(['edit_menu_items', 'buttons'])
  const classes = useStyles()
  const menuItems = useSelector(getCategoryEditItems)
  const categoryId = useSelector(getCategoryId)
  const currencySymbol = useSelector(getCurrencySymbol)
  const { isSuccess, itemName } = useSelector(getRemoveModal)
  const { isReorderActive, isReorderFetching, isReorderSuccess } = useSelector(
    getEditItemsReorder
  )
  const dispatch = useDispatch()
  const [snackbar, setSnackbar] = useState({ show: false, message: '' })
  const [menuItemsList, setMenuItemsList] = useState([])

  useEffect(() => {
    setMenuItemsList(menuItems)
  }, [menuItems])

  useEffect(() => {
    return () => dispatch(clearCategoryEditItems())
  }, [dispatch])

  useEffect(() => {
    if (isReorderSuccess) {
      setSnackbar({
        show: true,
        message: t('alerts:successfully_updated')
      })
      dispatch(clearEditItemsReorder())
    }
  }, [isReorderSuccess, dispatch, t])

  useEffect(() => {
    if (isSuccess) {
      setSnackbar({
        show: true,
        message: t('alerts:menu_item_remove_success', { itemName })
      })
      dispatch(clearRemoveModal())
    }
  }, [isSuccess, itemName, setSnackbar, t, dispatch])

  const handleSnackbarClose = useCallback(() => {
    setSnackbar((prevState) => assign(prevState, { show: false }))
  }, [setSnackbar])

  const handleReorderClick = useCallback(() => {
    dispatch(setEditItemsReorderActive())
  }, [dispatch])

  const handleCancelClick = useCallback(() => {
    dispatch(clearEditItemsReorder())
    setMenuItemsList(menuItems)
  }, [dispatch, menuItems])

  const handleSaveClick = useCallback(() => {
    dispatch(fetchEditItemsReorder(categoryId, map(menuItemsList, getId)))
  }, [dispatch, menuItemsList, categoryId])

  const handleReorder = useCallback(
    (from, to) => {
      setMenuItemsList((prevState) => swap(prevState, from, to))
    },
    [setMenuItemsList]
  )

  const renderLeftHeaderButton = useMemo(() => {
    if (isReorderActive) {
      return (
        <HeaderButton onClick={handleCancelClick}>
          {t('buttons:cancel')}
        </HeaderButton>
      )
    }

    return <BackButton relative to="/place/categories" />
  }, [isReorderActive, handleCancelClick, t])

  const renderRightHeaderButton = useMemo(() => {
    if (isReorderActive) {
      return (
        <HeaderButton onClick={handleSaveClick}>
          <CircularProgressWrapper isLoading={isReorderFetching} dark>
            {t('buttons:save')}
          </CircularProgressWrapper>
        </HeaderButton>
      )
    }

    return (
      <HeaderButton
        disabled={lessThan(length(menuItemsList), 2)}
        onClick={handleReorderClick}
      >
        {t('buttons:reorder')}
      </HeaderButton>
    )
  }, [
    menuItemsList,
    isReorderActive,
    handleReorderClick,
    handleSaveClick,
    isReorderFetching,
    t
  ])

  if (isEmpty(menuItems)) {
    return <Redirect to="/place/categories" />
  }

  return (
    <PlacePageContainer
      title={t('edit_menu_items:title')}
      headerLeftComponent={renderLeftHeaderButton}
      headerRightComponent={renderRightHeaderButton}
      footerActiveLeft
    >
      <Snackbar
        open={snackbar.show}
        message={snackbar.message}
        variant="success"
        onClose={handleSnackbarClose}
      />
      <RemoveItemModal />
      <Grid container direction="column" className={classes.root}>
        <List className={classes.list}>
          {map(menuItemsList, ({ id, name, price }, index) => {
            const isFirst = isEqual(index, 0)
            const isLast = isEqual(index, decrement(length(menuItemsList)))
            return (
              <MenuItemListItem
                key={id}
                id={id}
                price={price}
                name={name}
                currencySymbol={currencySymbol}
                categoryId={categoryId}
                isReorder={isReorderActive}
                isFirst={isFirst}
                isLast={isLast}
                handleReorder={handleReorder}
                index={index}
              />
            )
          })}
        </List>
      </Grid>
    </PlacePageContainer>
  )
}

export default EditMenuItems
