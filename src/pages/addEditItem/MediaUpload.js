import React, { useCallback, memo, useState, useMemo } from 'react'
import { createSelector } from 'reselect'
import { useDispatch, useSelector } from 'react-redux'
import { useDropzone } from 'react-dropzone'
import { useTranslation } from 'react-i18next'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Snackbar from 'components/notifications/Snackbar'
import File from './mediaUpload/File'
import { add, isFalsy, length, map, moreThan, slice } from 'utils/common'
import { createObjectUrl } from 'utils/files'
import {
  addMenuItemMedia,
  fetchAddMenuItemMedia
} from 'modules/actions/menuItem'
import {
  editMenuItemSelector,
  menuItemMediaSelector
} from 'modules/selectors/menuItem'
import { createMediaObject } from '../../utils/menuItem'

const useStyles = makeStyles(({ breakpoints }) => ({
  media: {
    height: 168,
    overflowX: 'auto',
    marginBottom: 25,
    '&::-webkit-scrollbar': {
      display: 'none'
    },
    [breakpoints.only('xs')]: {
      width: '100vw',
      transform: 'translateX(-30px)'
    }
  },
  mediaContainer: {
    minWidth: 'fit-content',
    [breakpoints.only('xs')]: {
      padding: '0 10px'
    }
  },
  mediaDrop: {
    minWidth: 168,
    minHeight: 168,
    border: '1px dashed #000',
    borderRadius: 8,
    opacity: 0.5,
    fontSize: 24,
    fontWeight: 400,
    '&:not(:last-child)': {
      marginRight: 8
    }
  }
}))

const getMenuItemMedia = createSelector(
  menuItemMediaSelector,
  ({ files, nextId, available, max }) => ({
    files,
    nextId,
    available,
    unavailable: isFalsy(available),
    max
  })
)

const getEditMenuItem = createSelector(
  editMenuItemSelector,
  ({ isEdit, item }) => {
    const { id: itemId, menu_category_id: categoryId } = item
    return { isEdit, itemId, categoryId }
  }
)

function MediaUpload() {
  const { t } = useTranslation('alerts')
  const classes = useStyles()
  const dispatch = useDispatch()
  const { files, nextId, available, max, unavailable } = useSelector(
    getMenuItemMedia
  )
  const { isEdit, itemId, categoryId } = useSelector(getEditMenuItem)
  const [errorSnackbar, setErrorSnackbar] = useState(false)
  const [errorSnackbarMessage, setErrorSnackbarMessage] = useState('')

  const onDrop = useCallback(
    (files) => {
      if (moreThan(length(files), available)) {
        setErrorSnackbar(true)
        setErrorSnackbarMessage(t('alerts:media_upload_max_size', { max }))
      }

      const filesList = map(slice(files, 0, available), (file, index) =>
        createMediaObject(file, createObjectUrl(file), add(nextId, index))
      )

      dispatch(addMenuItemMedia(filesList))
      if (isEdit) {
        map(filesList, (file) => {
          dispatch(fetchAddMenuItemMedia(itemId, file, categoryId))
        })
      }
    },
    [
      t,
      max,
      isEdit,
      itemId,
      nextId,
      dispatch,
      available,
      categoryId,
      setErrorSnackbar,
      setErrorSnackbarMessage
    ]
  )

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accept: '.png,.jpg,.jpeg'
  })

  const handleSnackbarClose = useCallback(() => {
    setErrorSnackbar(false)
  }, [setErrorSnackbar])

  const renderUploadButton = useMemo(() => {
    if (unavailable) return null
    return (
      <Button className={classes.mediaDrop} {...getRootProps()}>
        <input {...getInputProps()} />
        <Typography>+</Typography>
      </Button>
    )
  }, [unavailable, classes, getRootProps, getInputProps])

  return (
    <Grid container className={classes.media} wrap="nowrap">
      <Snackbar
        variant="error"
        open={errorSnackbar}
        message={errorSnackbarMessage}
        onClose={handleSnackbarClose}
      />
      <Grid container wrap="nowrap" className={classes.mediaContainer}>
        {map(files, ({ id, ...file }) => (
          <File key={id} id={id} {...file} />
        ))}
        {renderUploadButton}
      </Grid>
    </Grid>
  )
}

export default memo(MediaUpload)
