import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import Grid from '@material-ui/core/Grid'
import ButtonPrimary from 'components/buttons/ButtonPrimary'
import makeStyles from '@material-ui/core/styles/makeStyles'
import CircularProgressWrapper from 'components/progresses/CircularProgressWrapper'

const useStyles = makeStyles(({ breakpoints }) => ({
  buttonContainer: {
    paddingTop: 39,
    [breakpoints.only('xs')]: {
      paddingLeft: 52,
      paddingRight: 52
    }
  }
}))

function CreateButton({ disabled, isFetching, onClick }) {
  const { t } = useTranslation('buttons')
  const classes = useStyles()
  return (
    <Grid container className={classes.buttonContainer}>
      <ButtonPrimary disabled={disabled} onClick={onClick}>
        <CircularProgressWrapper isLoading={isFetching}>
          {t('buttons:create_category')}
        </CircularProgressWrapper>
      </ButtonPrimary>
    </Grid>
  )
}

CreateButton.propTypes = {
  disabled: PropTypes.bool,
  isFetching: PropTypes.bool,
  onClick: PropTypes.func
}

export default memo(CreateButton)
