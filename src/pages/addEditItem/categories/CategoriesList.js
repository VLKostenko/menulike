import React, { memo, useCallback } from 'react'
import PropTypes from 'prop-types'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import makeStyles from '@material-ui/core/styles/makeStyles'
import { map } from 'utils/common'

const useStyles = makeStyles({
  list: {
    height: 'calc(100% - 32px)',
    overflow: 'auto'
  },
  listItem: {
    paddingLeft: 32,
    paddingRight: 32
  }
})

function CategoryListItem({ id, name, onClick }) {
  const classes = useStyles()

  const handleClick = useCallback(() => {
    onClick({ id, name })
  }, [onClick, id, name])

  return (
    <ListItem button className={classes.listItem} onClick={handleClick}>
      <ListItemText>{name}</ListItemText>
    </ListItem>
  )
}

CategoryListItem.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func
}

const CategoryListItemMemoized = memo(CategoryListItem)

function CategoriesList({ categories, onChange }) {
  const classes = useStyles()
  return (
    <List className={classes.list}>
      {map(categories, ({ id, name }) => (
        <CategoryListItemMemoized
          key={id}
          id={id}
          name={name}
          onClick={onChange}
        />
      ))}
    </List>
  )
}

CategoriesList.propTypes = {
  categories: PropTypes.array,
  filterString: PropTypes.string,
  onChange: PropTypes.func
}

export default CategoriesList
