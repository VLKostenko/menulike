import React, { useCallback, useEffect, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { createSelector } from 'reselect'
import { useDispatch, useSelector } from 'react-redux'
import { useTranslation } from 'react-i18next'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Grid from '@material-ui/core/Grid'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import BackButton from 'components/buttons/BackButton'
import FormControlMuiInput from 'components/form/FormControlMuiInput'
import CreateButton from './categories/CreateButton'
import CategoriesList from './categories/CategoriesList'
import {
  createCategoriesErrorSelector,
  createCategoriesSelector
} from 'modules/selectors/categories'
import {
  clearCreateCategories,
  fetchCreateCategory
} from 'modules/actions/categories'
import {
  filter,
  isEmpty,
  isFalsy,
  isSomeTruthy,
  startsWith,
  toLower
} from 'utils/common'
import {
  MENU_CATEGORY,
  NAME,
  PLACEHOLDER_MENU_ITEM_CATEGORY
} from 'constants/forms'
import { simulateEvent } from 'utils/formik'
import { createPostMenuCategoriesBody } from 'utils/api'
import { percent } from 'utils/styles'

const useStyles = makeStyles(() => ({
  pageContainerRoot: {
    position: 'fixed',
    zIndex: 2,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  root: {
    paddingTop: 20,
    height: percent(100)
  },
  inputRoot: {
    '& input': {
      paddingLeft: 32,
      paddingRight: 32
    }
  }
}))

const getCreateCategories = createSelector(
  createCategoriesSelector,
  ({ isFetching, isSuccess, createdCategory }) => ({
    isCreateFetching: isFetching,
    isCreateSuccess: isSuccess,
    createdCategory
  })
)

const getCreateCategoriesError = createSelector(
  createCategoriesErrorSelector,
  ({ errors }) => ({ errors })
)

function Category({ categories, isEmptyCategories, onClose, onChange }) {
  const { t } = useTranslation('add_edit_item_category')
  const classes = useStyles()
  const dispatch = useDispatch()
  const { isCreateFetching, isCreateSuccess, createdCategory } = useSelector(
    getCreateCategories
  )
  const { errors } = useSelector(getCreateCategoriesError)
  const [filterString, setFilterString] = useState('')

  const handleCategoryChange = useCallback(
    ({ id }) => {
      onChange(simulateEvent(MENU_CATEGORY, id))
      onClose()
    },
    [onChange, onClose]
  )

  useEffect(() => {
    if (isCreateSuccess) {
      handleCategoryChange(createdCategory)

      return () => dispatch(clearCreateCategories())
    }
  }, [isCreateSuccess, createdCategory, handleCategoryChange, dispatch])

  const handleClick = useCallback(() => {
    dispatch(
      fetchCreateCategory(createPostMenuCategoriesBody({ name: filterString }))
    )
  }, [dispatch, filterString])

  const handleChange = useCallback(
    ({ target }) => {
      const { value } = target
      setFilterString(value)
    },
    [setFilterString]
  )

  const filteredCategories = useMemo(() => {
    return filter(categories, ({ name }) => {
      return startsWith(toLower(name), toLower(filterString))
    })
  }, [categories, filterString])

  const renderCategories = useMemo(() => {
    if (isSomeTruthy(isEmptyCategories, isEmpty(filteredCategories))) {
      return (
        <CreateButton
          disabled={isFalsy(filterString)}
          isFetching={isCreateFetching}
          onClick={handleClick}
        />
      )
    }
    return (
      <CategoriesList
        categories={filteredCategories}
        onChange={handleCategoryChange}
      />
    )
  }, [
    filteredCategories,
    isEmptyCategories,
    filterString,
    handleCategoryChange,
    handleClick,
    isCreateFetching
  ])

  return (
    <PlacePageContainer
      headerColor="secondary"
      title={t('add_edit_item_category:title')}
      footer={false}
      headerLeftComponent={
        <BackButton relative component={false} onClick={onClose} />
      }
      rootClassName={classes.pageContainerRoot}
    >
      <Grid className={classes.root}>
        <FormControlMuiInput
          id="menu-item_category"
          name={NAME}
          placeholder={PLACEHOLDER_MENU_ITEM_CATEGORY}
          inputRootClassName={classes.inputRoot}
          value={filterString}
          error={errors[NAME]}
          onChange={handleChange}
          autoComplete="off"
          autoFocus
          touched
        />
        {renderCategories}
      </Grid>
    </PlacePageContainer>
  )
}

Category.propTypes = {
  categories: PropTypes.array,
  isEmptyCategories: PropTypes.bool,
  onClose: PropTypes.func,
  onChange: PropTypes.func
}

export default Category
