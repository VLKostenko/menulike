import React from 'react'
import PropTypes from 'prop-types'
import FabSuccess from 'components/fabs/FabSuccess'
import FabRefresh from 'components/fabs/FabRefresh'
import CircularProgress from 'components/progresses/CircularProgress'

function Progress({ isSuccess, isFailure, isFetching, onReUploadClick }) {
  if (isFetching) {
    return <CircularProgress />
  }
  if (isSuccess) {
    return <FabSuccess />
  }
  if (isFailure) {
    return <FabRefresh onClick={onReUploadClick} />
  }
  return null
}

Progress.propTypes = {
  isSuccess: PropTypes.bool,
  isFailure: PropTypes.bool,
  isFetching: PropTypes.bool,
  onReUploadClick: PropTypes.func
}

export default Progress
