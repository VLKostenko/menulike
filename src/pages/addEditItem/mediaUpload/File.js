import React, { useCallback, memo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import PropTypes from 'prop-types'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Progress from './Progress'
import { isFalsy, isSomeTruthy, isTruthy } from 'utils/common'
import { backgroundImage, percent, rgba } from 'utils/styles'
import {
  fetchAddMenuItemMedia,
  fetchRemoveMenuItemMedia,
  removeMenuItemMedia
} from 'modules/actions/menuItem'
import { editMenuItemSelector } from 'modules/selectors/menuItem'
import { getErrorMessage } from 'utils/errors'

const useStyles = makeStyles(() => ({
  root: {
    minWidth: 168,
    minHeight: 168,
    width: 168,
    height: 168,
    borderRadius: 8,
    overflow: 'hidden',
    position: 'relative',
    '&:not(:last-child)': {
      marginRight: 8
    }
  },
  image: {
    width: percent(100),
    height: percent(100),
    backgroundImage: ({ url }) => backgroundImage(url),
    backgroundPosition: 'center center',
    backgroundSize: 'cover'
  },
  button: {
    width: 40,
    minWidth: 40,
    height: 40,
    backgroundColor: rgba(0, 0, 0, 0.5),
    borderRadius: 8,
    position: 'absolute',
    bottom: 0,
    right: 0,
    color: '#fff',
    '&:hover': {
      backgroundColor: rgba(0, 0, 0, 0.5)
    }
  },
  loadingOverlay: {
    width: percent(100),
    height: percent(100),
    position: 'absolute',
    backgroundColor: rgba(255, 255, 255, 0.4),
    left: 0,
    top: 0
  },
  imageErrorMessage: {
    width: percent(93),
    position: 'absolute',
    top: 4,
    color: '#fff',
    background: rgba(227, 28, 28, 0.5),
    borderRadius: 7,
    fontSize: 11,
    padding: 2
  }
}))

function Overlay({ isFetching, isSuccess, isFailure, onReUploadClick, error }) {
  const classes = useStyles()

  const errorMessage = getErrorMessage(error)

  if (isFalsy(isFetching, isSuccess, isFailure)) {
    return null
  }

  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.loadingOverlay}
    >
      {errorMessage && (
        <Typography className={classes.imageErrorMessage} align="center">
          {errorMessage}
        </Typography>
      )}
      <Progress
        isSuccess={isSuccess}
        isFailure={isFailure}
        isFetching={isFetching}
        onReUploadClick={onReUploadClick}
      />
    </Grid>
  )
}

Overlay.propTypes = {
  isFetching: PropTypes.bool,
  isSuccess: PropTypes.bool,
  isFailure: PropTypes.bool,
  onReUploadClick: PropTypes.func,
  error: PropTypes.object
}

function RemoveButton({ isFetching, onClick }) {
  const classes = useStyles()

  if (isSomeTruthy(isFetching)) {
    return null
  }

  return (
    <Button className={classes.button} onClick={onClick}>
      <i className="icon-Cross" />
    </Button>
  )
}

RemoveButton.propTypes = {
  isFetching: PropTypes.bool,
  isSuccess: PropTypes.bool,
  isFailure: PropTypes.bool,
  onClick: PropTypes.func
}

const getEditMenuItem = createSelector(
  editMenuItemSelector,
  ({ item, isEdit }) => {
    const { id: editItemId, menu_category_id: categoryId } = item
    return { editItemId, isEdit, categoryId }
  }
)

function File({ url, id, isFetching, isSuccess, isFailure, error, file }) {
  const classes = useStyles({ url })
  const dispatch = useDispatch()
  const { editItemId, isEdit, categoryId } = useSelector(getEditMenuItem)

  const handleRemove = useCallback(() => {
    if (isTruthy(isEdit, isFalsy(isFailure))) {
      return dispatch(fetchRemoveMenuItemMedia(id, editItemId, categoryId))
    }
    dispatch(removeMenuItemMedia(id))
  }, [id, dispatch, isEdit, editItemId, categoryId, isFailure])

  const handleReUpload = useCallback(() => {
    dispatch(fetchAddMenuItemMedia(editItemId, { file, id }))
  }, [dispatch, editItemId, file, id])

  return (
    <div className={classes.root}>
      <div className={classes.image} />
      <Overlay
        isFetching={isFetching}
        isSuccess={isSuccess}
        isFailure={isFailure}
        onReUploadClick={handleReUpload}
        error={error}
      />
      <RemoveButton isFetching={isFetching} onClick={handleRemove} />
    </div>
  )
}

File.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  url: PropTypes.string,
  isFetching: PropTypes.bool,
  isSuccess: PropTypes.bool,
  isFailure: PropTypes.bool,
  file: PropTypes.object,
  error: PropTypes.object
}

export default memo(File)
