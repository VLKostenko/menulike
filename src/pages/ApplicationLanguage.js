import React, { useCallback, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'
import List from '@material-ui/core/List'
import makeStyles from '@material-ui/core/styles/makeStyles'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import PlacePageContainer from 'components/containers/PlacePageContainer'
import BackButton from 'components/buttons/BackButton'
import ListItem from 'components/list/ListItem'
import FormControlMuiInput from 'components/form/FormControlMuiInput'
import { percent } from 'utils/styles'
import { LANGUAGE, PLACEHOLDER_LANGUAGE } from 'constants/forms'
import { languages } from 'utils/countries'
import { filter, map, startsWith, toLower } from 'utils/common'
import { setItem } from '../utils/localStorage'

const useStyles = makeStyles({
  root: {
    paddingTop: 20,
    height: percent(100)
  },
  inputRoot: {
    '& input': {
      paddingLeft: 32,
      paddingRight: 32
    }
  },
  list: {
    width: percent(100)
  },
  listItemIcon: {
    minWidth: 'auto',
    marginRight: 10,
    color: 'inherit'
  }
})

function LanguageListItem({ value, emoji, title, onChange }) {
  const classes = useStyles()

  const handleChange = useCallback(() => {
    onChange(value)
  }, [onChange, value])

  return (
    <ListItem button onClick={handleChange}>
      <ListItemIcon className={classes.listItemIcon}>{emoji}</ListItemIcon>
      <ListItemText>{title}</ListItemText>
    </ListItem>
  )
}

LanguageListItem.propTypes = {
  value: PropTypes.string,
  emoji: PropTypes.string,
  title: PropTypes.string,
  onChange: PropTypes.func
}

function ApplicationLanguage() {
  const { t, i18n } = useTranslation('application_language')
  const classes = useStyles()
  const { goBack } = useHistory()
  const [filterString, setFilterString] = useState('')

  const handleChange = useCallback(
    ({ target }) => {
      const { value } = target
      setFilterString(value)
    },
    [setFilterString]
  )

  const filteredLanguages = useMemo(() => {
    return filter(languages, ({ title }) => {
      return startsWith(toLower(title), toLower(filterString))
    })
  }, [filterString])

  const handleLngChange = useCallback(
    (lng) => {
      i18n.changeLanguage(lng).then(() => {
        setItem('lng', lng)
        goBack()
      })
    },
    [i18n, goBack]
  )

  return (
    <PlacePageContainer
      headerColor="secondary"
      title={t('application_language:title')}
      footer={false}
      headerLeftComponent={
        <BackButton relative component={false} onClick={goBack} />
      }
    >
      <Grid container className={classes.root}>
        <FormControlMuiInput
          id="application-language_search"
          name={LANGUAGE}
          placeholder={PLACEHOLDER_LANGUAGE}
          inputRootClassName={classes.inputRoot}
          value={filterString}
          onChange={handleChange}
          autoComplete="off"
          autoFocus
        />
        <List className={classes.list}>
          {map(filteredLanguages, ({ value, title, emoji }) => (
            <LanguageListItem
              value={value}
              title={title}
              emoji={emoji}
              key={value}
              onChange={handleLngChange}
            />
          ))}
        </List>
      </Grid>
    </PlacePageContainer>
  )
}

export default ApplicationLanguage
