import api from './api'
import { concat } from 'utils/common'
import { getId, getPin } from 'utils/localStorage'
import { parseError } from 'utils/errors'

export async function getPaymentInfoService() {
  try {
    const { data } = await api({
      method: 'GET',
      url: concat('/places/', getId(), '/payments/information'),
      headers: {
        placePin: getPin()
      }
    })

    return data.data
  } catch (e) {
    throw parseError(e)
  }
}
