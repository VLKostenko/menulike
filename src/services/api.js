import axios from 'axios'
import { API_URL } from 'config'
import { getCurrentLanguage } from 'utils/countries'
import { concat } from 'utils/common'

const instance = axios.create({
  baseURL: concat(API_URL, '/api'),
  responseType: 'json'
})

instance.defaults.headers.post['Content-Type'] = 'application/json'

instance.interceptors.request.use(
  (requestConfig) => {
    requestConfig.headers['X-locale'] = getCurrentLanguage()
    return requestConfig
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default instance
