import api from './api'
import { parseError } from '../utils/errors'

export async function getSubscriptionsPlansService() {
  try {
    const { data } = await api({
      method: 'GET',
      url: '/subscription-plans'
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}
