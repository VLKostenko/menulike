import api from './api'
import { concat } from 'utils/common'
import { getId, getPin } from 'utils/localStorage'
import { parseError } from 'utils/errors'

export async function addMenuItemMediaService(menuItemId, data) {
  try {
    const { data: responseData } = await api({
      method: 'POST',
      url: concat(
        '/places/',
        getId(),
        '/menu-items/',
        menuItemId,
        '/media/images'
      ),
      headers: {
        'Content-Type': 'multipart/form-data',
        placePin: getPin()
      },
      data
    })

    return responseData
  } catch (e) {
    throw parseError(e)
  }
}

export async function removeMenuItemMediaService(menuItemId, mediaId) {
  try {
    const { data } = await api({
      method: 'DELETE',
      url: concat(
        '/places/',
        getId(),
        '/menu-items/',
        menuItemId,
        '/media/',
        mediaId
      ),
      headers: {
        placePin: getPin()
      }
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}
