import api from './api'
import { concat } from 'utils/common'
import { getId, getPin } from 'utils/localStorage'
import { parseError } from 'utils/errors'

export async function getCategoriesService() {
  try {
    const { data } = await api({
      method: 'GET',
      url: concat('/places/', getId(), '/menu-categories'),
      headers: {
        placePin: getPin()
      }
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}

export async function createCategoriesService(data) {
  try {
    const { data: responseData } = await api({
      method: 'POST',
      url: concat('/places/', getId(), '/menu-categories'),
      headers: {
        placePin: getPin()
      },
      data
    })

    return responseData
  } catch (e) {
    throw parseError(e)
  }
}

export async function removeCategoryService(categoryId) {
  try {
    const { data } = await api({
      method: 'DELETE',
      url: concat('/places/', getId(), '/menu-categories/', categoryId),
      headers: {
        placePin: getPin()
      }
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}

export async function editCategoryService(categoryId, data) {
  try {
    const { data: responseData } = await api({
      method: 'PUT',
      url: concat('/places/', getId(), '/menu-categories/', categoryId),
      headers: {
        placePin: getPin()
      },
      data
    })

    return responseData
  } catch (e) {
    throw parseError(e)
  }
}

export async function orderCategoriesService(list) {
  try {
    const { data } = await api({
      method: 'POST',
      url: concat('/places/', getId(), '/menu-categories/order'),
      headers: {
        placePin: getPin()
      },
      data: {
        list
      }
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}

export async function editItemsOrderService(list) {
  try {
    const { data } = await api({
      method: 'POST',
      url: concat('/places/', getId(), '/menu-items/order'),
      headers: {
        placePin: getPin()
      },
      data: {
        list
      }
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}
