import api from './api'
import { concat } from 'utils/common'
import { getId, getPin } from 'utils/localStorage'
import { parseError } from 'utils/errors'

export async function createMenuItemService(data) {
  try {
    const { data: responseData } = await api({
      method: 'POST',
      url: concat('/places/', getId(), '/menu-items'),
      headers: {
        placePin: getPin()
      },
      data
    })

    return responseData
  } catch (e) {
    throw parseError(e)
  }
}

export async function editMenuItemService(itemId, data) {
  try {
    const { data: responseData } = await api({
      method: 'PUT',
      url: concat('/places/', getId(), '/menu-items/', itemId),
      headers: {
        placePin: getPin()
      },
      data
    })

    return responseData
  } catch (e) {
    throw parseError(e)
  }
}

export async function toggleActivityService(itemId, value) {
  try {
    const { data } = await api({
      method: 'POST',
      url: concat(
        '/places/',
        getId(),
        '/menu-items/',
        itemId,
        '/active/',
        value
      ),
      headers: {
        placePin: getPin()
      }
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}

export async function removeMenuItemService(itemId) {
  try {
    const { data } = await api({
      method: 'DELETE',
      url: concat('/places/', getId(), '/menu-items/', itemId),
      headers: {
        placePin: getPin()
      }
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}

export async function getMenuItemByIdService(itemId) {
  try {
    const { data } = await api({
      method: 'GET',
      url: concat('/places/', getId(), '/menu-items/', itemId),
      headers: {
        placePin: getPin()
      }
    })

    return data
  } catch (e) {
    throw parseError(e)
  }
}
