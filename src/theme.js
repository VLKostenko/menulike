import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import { important, percent, px, rgba } from './utils/styles'

export default function createTheme() {
  return createMuiTheme({
    maxWidth: 600,
    palette: {
      background: {
        primary: '#ebe8eb',
        secondary: '#fff',
        third: '#f6f6f6'
      },
      headerBackground: {
        primary: '#fff',
        secondary: '#ebe8eb'
      },
      button: {
        primary: '#000',
        secondary: 'transparent',
        border: {
          primary: '#000',
          secondary: '#000'
        },
        label: {
          primary: '#fff',
          secondary: '#000'
        }
      },
      heading: {
        primary: '#000'
      },
      subHeading: {
        primary: 'rgba(0, 0, 0, 0.6)'
      },
      formControl: {
        border: {
          primary: '#000'
        },
        focused: {
          shadow: {
            primary: 'rgba(0, 0, 0, 0.5)'
          }
        },
        error: {
          shadow: {
            primary: 'rgb(244, 67, 54, 0.5)'
          }
        },
        placeholder: {
          primary: 'rgba(0, 0, 0, 0.5)'
        }
      },
      link: {
        primary: '#666666'
      },
      fab: {
        warning: {
          background: {
            success: '#000',
            warning: '#ffb800',
            error: '#ff8585'
          }
        }
      }
    },
    lineHeight: {
      primary: 24
    },
    transitions: {
      primary: 350
    },
    fontFamily: {
      primary: 'Roboto',
      secondary: 'Times'
    },
    shape: {
      container: {
        width: '100%',
        maxWidth: 600
      },
      button: {
        height: 56,
        borderRadius: 28,
        borderWidth: 2,
        borderStyle: 'solid',
        label: {
          textTransform: 'uppercase',
          fontWeight: 600
        }
      },
      heading: {
        fontSize: 24,
        marginBottom: 40,
        lineHeight: '29px',
        textAlign: 'center',
        fontWeight: 'bold'
      },
      subHeading: {
        fontSize: 18,
        marginBottom: 33,
        lineHeight: '24px',
        textAlign: 'center',
        whiteSpace: 'normal'
      },
      formControl: {
        height: 56,
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 28,
        boxSizing: 'border-box',
        textAlign: 'center'
      },
      formControlFocused: {
        borderWidth: 2
      },
      formControlMui: {
        '& input': {
          fontSize: 16
        },
        '& label': {
          color: rgba(0, 0, 0, 0.5),
          fontSize: 14,
          transform: 'none'
        },
        '& > div': {
          '&::after': {
            borderBottomColor: '#000'
          }
        }
      },
      formControlMuiLabel: {
        fontSize: 14,
        transform: 'none',
        marginBottom: 2
      },
      formControlMuiLabelFocused: {
        color: important(rgba(0, 0, 0, 0.5))
      },
      fab: {
        width: 40,
        height: 40,
        color: '#fff',
        fontSize: 20,
        borderRadius: percent(50)
      },
      listItem: {
        root: {
          height: 56,
          padding: 0,
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          borderBottom: '1px solid rgba(0, 0, 0, 0.2)'
        },
        side: {
          display: 'flex',
          alignItems: 'center'
        },
        leftButton: {
          fontSize: 16,
          marginRight: 10
        },
        reorderButton: {
          fontSize: 20,
          marginRight: 10
        },
        text: {
          fontSize: 16
        },
        counter: {
          marginRight: 10
        },
        rightButton: {
          width: 40,
          fontSize: 14,
          '& i': {
            opacity: 0.2
          }
        },
        input: {
          background: 'transparent',
          border: 'none'
        },
        buttonHidden: {
          pointerEvents: 'none',
          opacity: 0
        }
      },
      cards: {
        subscription: {
          root: {
            background: '#fff',
            borderRadius: 9,
            boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)'
          },
          priceText: {
            fontSize: 20,
            fontWeight: 500,
            lineHeight: px(23)
          },
          textThin: {
            fontWeight: 300
          },
          divider: {
            height: 'inherit',
            background: '#000'
          }
        }
      }
    }
  })
}
