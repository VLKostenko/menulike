export function toPrice(value, commaForThousands = true) {
  var price = Number.parseFloat(value)
  if (price < 1000 || !commaForThousands) {
    return price.toFixed(2).toString()
  } else {
    return price
      .toFixed(0)
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
  }
}
