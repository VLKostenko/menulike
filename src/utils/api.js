import {
  createPostPlacesInterface,
  createGetPlaceByEmailInterface,
  createPostReplacePlaceByEmailInterface,
  createGetPlaceByIdInterface,
  createPutPlacesInterface,
  createPostMenuItemInterface,
  createPostMenuItemMediaInterface,
  createPutMenuItemInterface,
  createPostMenuCategoriesInterface
} from './interfaces'
import { forEach, pickByTruth, reduce, toUpper } from './common'
import { getCurrencyByISO } from './countries'
import {
  COUNTRY,
  CURRENCY,
  DESCRIPTION,
  PHONE_CODE,
  PHONE_NUMBER,
  PRIORITY
} from 'constants/forms'
import { cutDialCode } from './phone'

export function createPostPlacesBody(values) {
  return pickByTruth(
    reduce(
      createPostPlacesInterface(),
      (acc, cur) => {
        switch (cur) {
          case COUNTRY:
            return { ...acc, [cur]: toUpper(values[cur]) }
          case CURRENCY:
            return { ...acc, [cur]: getCurrencyByISO(values[cur]) }
          case PHONE_NUMBER:
            return {
              ...acc,
              [cur]: cutDialCode(values[cur], values[PHONE_CODE])
            }
          default:
            return { ...acc, [cur]: values[cur] }
        }
      },
      {}
    )
  )
}

export function createGetPlaceByEmailBody(values) {
  return pickByTruth(
    reduce(
      createGetPlaceByEmailInterface(),
      (acc, cur) => {
        return { ...acc, [cur]: values[cur] }
      },
      {}
    )
  )
}

export function createGetPlaceByIdBody(values) {
  return pickByTruth(
    reduce(
      createGetPlaceByIdInterface(),
      (acc, cur) => {
        return { ...acc, [cur]: values[cur] }
      },
      {}
    )
  )
}

export function createPostReplacePlaceByEmailBody(values) {
  return pickByTruth(
    reduce(
      createPostReplacePlaceByEmailInterface(),
      (acc, cur) => {
        return { ...acc, [cur]: values[cur] }
      },
      {}
    )
  )
}

export function createPutPlacesBody(values) {
  return pickByTruth(
    reduce(
      createPutPlacesInterface(),
      (acc, cur) => {
        return { ...acc, [cur]: values[cur] }
      },
      {}
    ),
    DESCRIPTION
  )
}

export function createPostMenuItemBody(values) {
  return pickByTruth(
    reduce(
      createPostMenuItemInterface(),
      (acc, cur) => {
        switch (cur) {
          case PRIORITY:
            return { ...acc, [cur]: -1 }
          default:
            return { ...acc, [cur]: values[cur] }
        }
      },
      {}
    )
  )
}

export function createPutMenuItemBody(values) {
  return pickByTruth(
    reduce(
      createPutMenuItemInterface(),
      (acc, cur) => {
        return { ...acc, [cur]: values[cur] }
      },
      {}
    ),
    DESCRIPTION
  )
}

export function createPostMenuItemMediaBody(values) {
  const formData = new FormData()
  forEach(createPostMenuItemMediaInterface(), (cur) => {
    switch (cur) {
      case PRIORITY:
        formData.append(cur, '-1')
        break
      default:
        formData.append(cur, values[cur])
    }
  })
  return formData
}

export function createPostMenuCategoriesBody(values) {
  return pickByTruth(
    reduce(
      createPostMenuCategoriesInterface(),
      (acc, cur) => {
        switch (cur) {
          case PRIORITY:
            return { ...acc, [cur]: -1 }
          default:
            return { ...acc, [cur]: values[cur] }
        }
      },
      {}
    )
  )
}
