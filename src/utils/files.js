export function createObjectUrl(file) {
  return URL.createObjectURL(file)
}
