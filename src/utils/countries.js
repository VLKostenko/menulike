import { countries } from 'countries-list'
import getSymbol from 'currency-symbol-map'
import {
  concat,
  cut,
  filter,
  head,
  includes,
  isFalsy,
  isTruthy,
  keys,
  map,
  sortByKey,
  split,
  takeTruth,
  toUpper
} from './common'
import { createSelectOption } from './select'
import { getItem } from './localStorage'

/**
 * Helper functions
 */

const DUPLICATED_CURRENCIES = [
  'ANG',
  'AUD',
  'DKK',
  'EUR',
  'GBP',
  'ILS',
  'MAD',
  'NOK',
  'NZD',
  'USD',
  'XAF',
  'XCD',
  'XOF'
]

const DUPLICATES_CURRENCIES_ISO = [
  'SX',
  'HM',
  'DK',
  'IT',
  'GB',
  'IL',
  'EH',
  'SJ',
  'NZ',
  'US',
  'RD',
  'KN',
  'TG'
]
export const DEFAULT_LANGUAGE = 'en'

const AVAILABLE_LANGUAGES = {
  en: 'ENG',
  ru: 'РУС',
  uk: 'УКР'
}

function isoToNativeLanguage(iso) {
  switch (iso) {
    case 'UA':
      return 'Український'
    case 'RU':
      return 'Русский'
    case 'GB':
      return 'English'
    default:
      return ''
  }
}

function lngToTitle(lng) {
  if (lng in AVAILABLE_LANGUAGES) {
    return AVAILABLE_LANGUAGES[lng]
  } else {
    return ''
  }
}

function getNavigatorLanguage() {
  var navigatorLang = cut(navigator.language, 0, 2)
  if (navigatorLang in AVAILABLE_LANGUAGES) {
    return navigatorLang
  } else {
    return DEFAULT_LANGUAGE
  }
}

export function getCurrentLanguage() {
  return takeTruth(getItem('lng'), getNavigatorLanguage())
}

export function getCurrentLanguageTitle(lng) {
  return lngToTitle(lng)
}

function filterCurrencies({ iso, currency }) {
  if (isFalsy(currency)) {
    return false
  }
  if (includes(DUPLICATED_CURRENCIES, currency)) {
    return isTruthy(includes(DUPLICATES_CURRENCIES_ISO, iso))
  }
  return true
}

function cutCurrency(currency) {
  return head(split(currency, ','))
}

function cutPhone(phone) {
  return head(split(phone, ','))
}

function createCountry(iso) {
  const { emoji, native, phone, currency, languages } = countries[iso]
  return {
    iso,
    emoji,
    native,
    phone,
    currency,
    languages
  }
}

function createNative({ iso, emoji, native }) {
  return {
    iso,
    native,
    emoji
  }
}

function createPhone({ iso, emoji, phone }) {
  return {
    iso,
    emoji,
    phone: cutPhone(phone)
  }
}

function createCurrency({ iso, emoji, currency: cur }) {
  const currency = cutCurrency(cur)
  const symbol = getSymbol(currency) ? getSymbol(currency) : ''
  return {
    iso,
    emoji,
    currency,
    symbol
  }
}

function filterSupportedLanguages({ iso }) {
  return includes(['GB', 'UA', 'RU'], iso)
}

function createLanguage({ languages, emoji, iso }) {
  return {
    value: head(languages),
    title: isoToNativeLanguage(iso),
    emoji
  }
}

/**
 * Arrays for creating select options
 *
 * @isoCodes: ["US", "GB", "UA", "RU"]
 *
 * @countriesArray:
 * [
 *  {
 *    iso: "GB",
 *    native: "United Kingdom",
 *    emoji: emoji,
 *    phone: "44",
 *    currency: GBP
 *  }
 * ]
 *
 * @natives:
 * [
 *  {
 *    iso: "GB",
 *    emoji: emoji,
 *    native: "United Kingdom"
 *  }
 * ]
 *
 * @phones:
 * [
 *  {
 *    iso: "GB",
 *    emoji: emoji,
 *    phone: "44"
 *  }
 * ]
 *
 * @currencies:
 * [
 *  {
 *    iso: "GB",
 *    emoji: emoji,
 *    currency: "GBP"
 *  }
 * ]
 */

const isoCodes = keys(countries)
const countriesArray = map(keys(countries), createCountry)
const natives = sortByKey(map(countriesArray, createNative), 'native')
const phones = sortByKey(map(countriesArray, createPhone), 'phone')
const currencies = filter(
  sortByKey(map(countriesArray, createCurrency), 'currency'),
  filterCurrencies
)
export const languages = map(
  filter(countriesArray, filterSupportedLanguages),
  createLanguage
)

/**
 * Temporary helper functions functions for Create Place page
 * Will remove them lately
 */

function getNative({ native }) {
  return native
}

function getCurrency({ currency }) {
  return currency
}

function getDialCode({ phone }) {
  return phone
}

function getPhone({ phone }) {
  return phone
}

export function getCountryByISO(iso) {
  return countries[toUpper(iso)]
}

export function getPhoneByISO(iso) {
  return getPhone(getCountryByISO(iso))
}

export function getCurrencyByISO(iso) {
  return getCurrency(getCountryByISO(iso))
}

export function getDialCodeByISO(iso) {
  return getDialCode(getCountryByISO(iso))
}

/**
 * Functions for creating select options
 */

export function getCountriesOptions() {
  return map(natives, ({ iso, native, emoji }) => {
    return createSelectOption(iso, concat(emoji, ' ', native))
  })
}

export function getCurrencyOptions() {
  return map(currencies, ({ currency, symbol }) => {
    return createSelectOption(currency, concat(currency, ' ', symbol))
  })
}

export function getPhoneCodeOptions() {
  return map(phones, ({ phone, emoji }) => {
    return createSelectOption(phone, concat(emoji, ' +', phone))
  })
}

/**
 * Deprecates functions for creating select options
 * Currently used on the CreatePlace page
 * will remove them later
 */

export function getCurrencyOptions__DEPRECATED() {
  return map(isoCodes, (iso) => {
    return createSelectOption(iso, getCurrencyByISO(iso))
  })
}

export function getCountriesOptions__DEPRECATED() {
  return map(isoCodes, (iso) => {
    return createSelectOption(iso, getNative(getCountryByISO(iso)))
  })
}
