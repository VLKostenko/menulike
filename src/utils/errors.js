import { head, isEqual, keys, reduce, values } from './common'

function parseErrors(errors = {}) {
  return reduce(
    keys(errors),
    (acc, cur) => {
      return { ...acc, [cur]: head(errors[cur]) }
    },
    {}
  )
}

function createErrorObject(message, errors, code) {
  return {
    message,
    errors,
    code
  }
}

export function parseError({ response }) {
  const { errors, status_code, message } = response.data
  return createErrorObject(message, parseErrors(errors), status_code)
}

export function getErrorMessage({ errors, message }) {
  if (errors) {
    return head(values(errors))
  }
  return message
}

export function required() {
  return 'forms:error_is_required'
}

export function email() {
  return 'forms:error_is_not_valid'
}

// export function max({ max, path: controlName }) {
export function max() {
  return 'forms:error_to_long'
}

// export function min({ min, path: controlName }) {
export function min() {
  return 'forms:error_to_short'
}

export function is429(code) {
  return isEqual(code, 429)
}

export function is404(code) {
  return isEqual(code, 404)
}

export function is422(code) {
  return isEqual(code, 422)
}
