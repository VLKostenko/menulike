import {
  assign,
  every,
  isFalsy,
  isSomeTruthy,
  isTruthy,
  some,
  takeTruth
} from './common'

export function convertActivityStatus(active) {
  return active ? 0 : 1
}

export function createEditMenuItem({
  id,
  priority,
  name,
  menu_category_id,
  menu_category_name,
  active,
  price,
  description
}) {
  return {
    id,
    priority,
    name,
    menu_category_id,
    menu_category_name,
    active,
    price,
    description
  }
}

export function createMediaObject(file, url, id, isSuccess = false) {
  return {
    file,
    url,
    id,
    isSuccess
  }
}

export function getMenuCategoryId({ menu_category_id: menuCategoryId }) {
  return menuCategoryId
}

export function isSuccess({ createItem, editItem, media }) {
  const { isSuccess: isCreateSuccess } = createItem
  const { isEdit, isSuccess: isEditSuccess } = editItem

  if (isFalsy(isMediaSuccess(media))) {
    return false
  }
  if (isTruthy(isEdit, isFalsy(isEditSuccess))) {
    return false
  }
  if (isFalsy(isEdit, isCreateSuccess)) {
    return false
  }
  return true
}

export function isFetching({ createItem, editItem, media }) {
  const { isFetching: isCreateFetching } = createItem
  const { isFetching: isEditFetching } = editItem
  return isSomeTruthy(isCreateFetching, isEditFetching, isMediaFetching(media))
}

export function serverErrors(createError, editError) {
  const { errors: createErrors } = createError
  const { errors: editErrors } = editError
  return assign(createErrors, editErrors)
}

export function isMediaSuccess({ files }) {
  return every(files, ({ isSuccess }) => isSuccess)
}

export function isMediaFetching({ files }) {
  return some(files, ({ isFetching }) => isFetching)
}

export function isMediaFailure({ files }) {
  return some(files, ({ isFailure }) => isFailure)
}

export function errorMessages(createError, editError) {
  const { message: createMessage } = createError
  const { message: editMessage } = editError
  return takeTruth(createMessage, editMessage)
}

export function errorCode(createError, editError) {
  const { code: createCode } = createError
  const { code: editCode } = editError
  return takeTruth(createCode, editCode)
}
