import toNumber from 'lodash.tonumber'
import multiply from 'lodash.multiply'
import divide from 'lodash.divide'
import {
  concat,
  filter,
  find,
  isEqual,
  isFalsy,
  moreThan,
  reduce
} from './common'

export function isTrial(code) {
  return isEqual(code, 'TRIAL1')
}

export function isFree(code) {
  return isEqual(code, 'FREE')
}

function termToMonths(term) {
  switch (term) {
    case 'month':
      return 1
    case 'year':
      return 12
    default:
      return 1
  }
}

export function calculatePrice(price, term, termsCount) {
  return divide(toNumber(price), multiply(termToMonths(term), termsCount))
}

export function calculateTermTitle(term, termCount, code) {
  if (isTrial(code)) {
    return 'subscription_plans:first_month'
  }
  if (isFree(code)) {
    return 'subscription_plans:limited'
  }
  if (isEqual(term, 'month')) {
    if (isEqual(termCount, 1)) {
      return 'subscription_plans:billed_monthly'
    }
    if (moreThan(termCount, 1)) {
      return 'subscription_plans:billed_every_months'
    }
  }
  if (isEqual(term, 'year')) {
    if (isEqual(termCount, 1)) {
      return 'subscription_plans:billed_annually'
    }
    if (moreThan(termCount, 1)) {
      return concat('subscription_plans:billed_every_years')
    }
  }
}

export function parseFeatures(data) {
  return reduce(
    data,
    (acc, { features }) => {
      return [
        ...acc,
        ...filter(features, ({ id }) => {
          return isFalsy(
            find(acc, ({ id: existingId }) => isEqual(id, existingId))
          )
        })
      ]
    },
    []
  )
}

export function isPlanActive(id, code, isTrialActive, subscriptionPlanId) {
  if (isTrialActive) {
    return isTrial(code)
  }
  return isEqual(id, subscriptionPlanId)
}

export function getCardVariant(isActiveTrial, isActiveSubscription) {
  return isActiveSubscription ? 'success' : isActiveTrial ? 'warning' : 'error'
}
