export function isTruthy(...params) {
  return params.every((param) => !!param)
}

export function isFalsy(...params) {
  return params.every((param) => !param)
}

export function isSomeTruthy(...params) {
  return params.some((param) => !!param)
}

export function concat(...params) {
  return params.reduce((acc, cur) => {
    return acc + cur
  }, '')
}

export function length(param) {
  return param.length
}

export function match(string, regExp) {
  return string.match(regExp)
}

export function startsWith(string, regExp) {
  return string.startsWith(regExp)
}

export function moreThan(param, number) {
  return param > number
}

export function lessThan(param, number) {
  return param < number
}

export function takeTruth(...params) {
  return params.find((param) => !!param)
}

export function join(separator, ...params) {
  return params.join(separator)
}

export function slice(array, from = 0, to = length(array)) {
  return array.slice(from, to)
}

export function toString(param) {
  return param.toString()
}

export function head(array) {
  return array[0]
}

export function reduce(array, cb, initialValue) {
  return array.reduce(cb, initialValue)
}

export function find(array, cb) {
  return array.find(cb)
}

export function replace(value, regexp, newValue) {
  return value.replace(regexp, newValue)
}

export function map(array, cb) {
  return array.map(cb)
}

export function filter(array, cb) {
  return array.filter(cb)
}

export function some(array, cb) {
  return array.some(cb)
}

export function every(array, cb) {
  return array.every(cb)
}

export function forEach(array, cb) {
  return array.forEach(cb)
}

export function sort(array, cb) {
  return array.sort(cb)
}

export function swap(array, from, to) {
  const copy = [...array]
  const temp = copy[from]

  copy[from] = copy[to]
  copy[to] = temp

  return copy
}

export function push(array, ...params) {
  return [...array, ...params]
}

export function sortByKey(array, key) {
  return array.sort((a, b) => (a[key] > b[key] ? 1 : -1))
}

export function includes(array, ...options) {
  return some(array, (item) => {
    return options.includes(item)
  })
}

export function split(str, separator) {
  return str.split(separator)
}

export function uniq(array) {
  return [...new Set(sort(array))]
}

export function toUpper(param) {
  return param.toUpperCase()
}

export function toLower(param) {
  return param.toLowerCase()
}

export function cut(str, start = 0, end = length(str)) {
  return str.slice(start, end)
}

export function assign(...objects) {
  return reduce(
    objects,
    (acc, cur) => {
      return { ...acc, ...cur }
    },
    {}
  )
}

export function keys(obj) {
  return Object.keys(obj)
}

export function values(obj) {
  return Object.values(obj)
}

export function add(...params) {
  return reduce(
    params,
    (acc, cur) => {
      return acc + cur
    },
    0
  )
}

export function sub(a, b) {
  return a - b
}

export function multiply(a, b) {
  return a * b
}

export function pickByTruth(object, ...excludes) {
  return reduce(
    keys(object),
    (acc, cur) => {
      return isTruthy(
        includes([null, undefined, ''], object[cur]),
        isFalsy(includes(excludes, cur))
      )
        ? { ...acc }
        : { ...acc, [cur]: object[cur] }
    },
    {}
  )
}

export function pickKeys(object, ...keys) {
  return pickByTruth(
    reduce(
      keys,
      (acc, cur) => {
        return { ...acc, [cur]: object[cur] }
      },
      {}
    )
  )
}

export function isNotEmpty(param) {
  if (isFalsy(param)) return false
  if (Object.prototype.toString.call(param) === '[object Object]') {
    return isTruthy(length(keys(param)))
  }
  return isTruthy(length(param))
}

export function isEmpty(param) {
  if (isFalsy(param)) return true
  if (Object.prototype.toString.call(param) === '[object Object]') {
    return isFalsy(length(keys(param)))
  }
  return isFalsy(length(param))
}

export function increment(value, number = 1) {
  return value + number
}

export function decrement(value, number = 1) {
  return value - number
}

export function substring(str, start = 0, end = length(str)) {
  return str.substring(start, end)
}

export function ellipsis(str, len) {
  return moreThan(length(str), len)
    ? concat(substring(str, 0, decrement(len, 3)), '...')
    : str
}

export function isEqual(value, other) {
  const type = Object.prototype.toString.call(value)
  if (type !== Object.prototype.toString.call(other)) return false
  if (
    [
      '[object String]',
      '[object Number]',
      '[object Boolean]',
      '[object Null]'
    ].includes(type)
  )
    return value === other
  if (type === '[object Function]') return toString(value) === toString(other)
  if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false
  const valueLen =
    type === '[object Array]' ? value.length : Object.keys(value).length
  const otherLen =
    type === '[object Array]' ? other.length : Object.keys(other).length
  if (valueLen !== otherLen) return false

  const compare = function (item1, item2) {
    const itemType = Object.prototype.toString.call(item1)
    if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
      if (!isEqual(item1, item2)) return false
    } else {
      if (itemType !== Object.prototype.toString.call(item2)) return false
      if (itemType === '[object Function]') {
        if (toString(item1) !== toString(item2)) return false
      } else {
        if (item1 !== item2) return false
      }
    }
  }
  if (type === '[object Array]') {
    for (let i = 0; i < valueLen; i++) {
      if (compare(value[i], other[i]) === false) return false
    }
  } else {
    for (const key in value) {
      // eslint-disable-next-line
      if (value.hasOwnProperty(key)) {
        if (compare(value[key], other[key]) === false) return false
      }
    }
  }
  return true
}

export function isNotEqual(value, other) {
  return isFalsy(isEqual(value, other))
}
