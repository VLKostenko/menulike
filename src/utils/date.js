import { monthNames } from 'constants/date'
import { concat } from './common'

export function dateToView(date) {
  const parsedDate = new Date(date)

  const day = parsedDate.getDate()
  const month = monthNames[parsedDate.getMonth()]
  const year = parsedDate.getFullYear()

  return concat(day, ' ', month, ' ', year)
}
