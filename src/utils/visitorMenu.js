import { filter, isNotEmpty, isTruthy } from './common'

function isActive({ active }) {
  return isTruthy(active)
}

function isNotEmptyMenuItems({ menu_items: menuItems }) {
  return isNotEmpty(filterMenuItems(menuItems))
}

export function filterCategories(categories) {
  return filter(categories, isNotEmptyMenuItems)
}

export function filterMenuItems(menuItems) {
  return filter(menuItems, isActive)
}
