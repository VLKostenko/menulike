import {
  ACTIVE,
  ADDRESS,
  CITY,
  COUNTRY,
  CURRENCY,
  DESCRIPTION,
  EMAIL,
  FILE,
  ID,
  MENU_CATEGORY,
  MENU_ITEM_NAME,
  PHONE_CODE,
  PHONE_NUMBER,
  PLACE_NAME,
  PLACE_PIN,
  PRICE,
  PRIORITY
} from 'constants/forms'
import { NAME } from '../constants/forms'

export function createPostPlacesInterface() {
  return [
    PLACE_NAME,
    EMAIL,
    CITY,
    COUNTRY,
    ADDRESS,
    PHONE_CODE,
    PLACE_PIN,
    PHONE_NUMBER,
    CURRENCY,
    DESCRIPTION
  ]
}

export function createGetPlaceByEmailInterface() {
  return [EMAIL, PLACE_PIN]
}

export function createGetPlaceByIdInterface() {
  return [ID, PLACE_PIN]
}

export function createPostReplacePlaceByEmailInterface() {
  return [EMAIL]
}

export function createPutPlacesInterface() {
  return [
    PLACE_NAME,
    EMAIL,
    COUNTRY,
    CITY,
    ADDRESS,
    PHONE_CODE,
    PHONE_NUMBER,
    CURRENCY,
    DESCRIPTION
  ]
}

export function createPostMenuItemInterface() {
  return [PRIORITY, MENU_ITEM_NAME, MENU_CATEGORY, ACTIVE, PRICE, DESCRIPTION]
}

export function createPutMenuItemInterface() {
  return [PRIORITY, MENU_ITEM_NAME, MENU_CATEGORY, ACTIVE, PRICE, DESCRIPTION]
}

export function createPostMenuItemMediaInterface() {
  return [PRIORITY, FILE]
}

export function createPostMenuCategoriesInterface() {
  return [PRIORITY, NAME]
}
