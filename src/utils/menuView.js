import {
  concat,
  filter,
  find,
  isEmpty,
  isEqual,
  isNotEmpty,
  replace
} from './common'

export function parseHeaderCategories({ id, name, priority }) {
  return { id: createScrollCategoryName(id), name, priority }
}

export function parseCategory({ id, name, priority }) {
  return { id, name, priority }
}

export function filterCategories({ menu_items: menuItems }) {
  return isNotEmpty(menuItems)
}

export function getCategoryId({ id }) {
  return id
}

export function getMenuItems({ menu_items: menuItems }) {
  return menuItems
}

export function getCategoryMenuItems({ menu_items }) {
  return menu_items
}

export function isEmptyCategories(menuCategories) {
  return isEmpty(filter(menuCategories, filterCategories))
}

export function getMenuItem(menuCategories, categoryId, itemId) {
  return find(
    getCategoryMenuItems(
      find(menuCategories, ({ id }) => isEqual(id, categoryId))
    ),
    ({ id }) => isEqual(id, itemId)
  )
}

export function createScrollCategoryName(id) {
  return concat('category-', id)
}

export function createScrollItemName(id) {
  return concat('item-', id)
}

export function getHashValue(hash) {
  return replace(hash, '#', '')
}
