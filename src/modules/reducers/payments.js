import { FETCH_PAYMENTS_INFO, PAYMENTS_INFO_SUCCESS } from '../actions/payments'
import { assign } from 'utils/common'

const initialState = {
  info: {
    genericTrial: {
      active: false,
      ends_at: new Date()
    },
    defaultPaymentMethod: {
      active: false
    },
    subscription: {
      active: false
    }
  },
  fetchInfo: {
    isSuccess: false,
    isFetching: false,
    isFailure: false
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_PAYMENTS_INFO:
      return assign(state, {
        fetchInfo: assign(state.fetchInfo, {
          isFetching: true
        })
      })
    case PAYMENTS_INFO_SUCCESS:
      return assign(state, {
        fetchInfo: assign(state.fetchInfo, {
          isFetching: false,
          isSuccess: true
        }),
        info: assign(state.info, {
          genericTrial: assign(
            state.info.genericTrial,
            action.data.genericTrial
          ),
          defaultPaymentMethod: assign(
            state.info.defaultPaymentMethod,
            action.data.defaultPaymentMethod
          ),
          subscription: assign(
            state.info.subscription,
            action.data.subscription
          )
        })
      })
    default:
      return state
  }
}
