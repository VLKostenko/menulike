import {
  ADD_MENU_ITEM_MEDIA,
  ADD_MENU_ITEM_MEDIA_FAILURE,
  ADD_MENU_ITEM_MEDIA_SUCCESS,
  CLEAR_MENU_ITEM,
  CLEAR_REMOVE_MODAL,
  CLOSE_REMOVE_MODAL,
  CREATE_MENU_ITEM_FAILURE,
  CREATE_MENU_ITEM_SUCCESS,
  EDIT_MENU_ITEM_FAILURE,
  EDIT_MENU_ITEM_SUCCESS,
  FETCH_ADD_MENU_ITEM_MEDIA,
  FETCH_CREATE_MENU_ITEM,
  FETCH_EDIT_MENU_ITEM,
  FETCH_GET_MENU_ITEM,
  FETCH_REMOVE_ITEM,
  FETCH_REMOVE_MENU_ITEM_MEDIA,
  GET_MENU_ITEM_SUCCESS,
  OPEN_REMOVE_MODAL,
  REMOVE_ITEM_SUCCESS,
  REMOVE_MENU_ITEM_MEDIA,
  REMOVE_MENU_ITEM_MEDIA_FAILURE,
  REMOVE_MENU_ITEM_MEDIA_SUCCESS,
  SET_EDIT,
  SET_EDIT_ITEM
} from '../actions/menuItem'
import {
  add,
  assign,
  filter,
  isEqual,
  isNotEqual,
  length,
  map,
  push,
  sub,
  increment
} from 'utils/common'
import {
  ID,
  PRICE,
  ACTIVE,
  PRIORITY,
  DESCRIPTION,
  MENU_CATEGORY,
  MENU_ITEM_NAME
} from 'constants/forms'
import { toPrice } from 'utils/price'

const initialMediaFileState = {
  isFetching: false,
  isSuccess: false,
  isFailure: false,
  error: {
    errors: {},
    message: '',
    code: null
  }
}

const initialState = {
  media: {
    files: [],
    nextId: 0,
    available: 10,
    max: 10
  },
  editItem: {
    isEdit: false,
    item: {
      [ID]: '',
      [PRIORITY]: '',
      [MENU_ITEM_NAME]: '',
      [MENU_CATEGORY]: '',
      [ACTIVE]: true,
      [PRICE]: '',
      [DESCRIPTION]: ''
    },
    isFetching: false,
    isSuccess: false,
    error: {
      errors: {},
      message: '',
      code: null
    }
  },
  getItem: {
    isFetching: false
  },
  createItem: {
    isFetching: false,
    isSuccess: false,
    error: {
      errors: {},
      message: '',
      code: null
    }
  },
  removeModal: {
    isOpen: false,
    itemId: null,
    itemName: '',
    categoryId: null,
    isFetching: false,
    isSuccess: false,
    isFailure: false
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_CREATE_MENU_ITEM:
      return assign(state, {
        createItem: assign(initialState.createItem, {
          isFetching: true,
          isSuccess: false,
          error: {
            errors: {},
            message: '',
            code: null
          }
        })
      })
    case CREATE_MENU_ITEM_SUCCESS:
      return assign(state, {
        createItem: assign(state.createItem, {
          isFetching: false,
          isSuccess: true
        })
      })
    case CREATE_MENU_ITEM_FAILURE:
      return assign(state, {
        createItem: assign(state.createItem, {
          isFetching: false,
          error: action.error
        })
      })
    case ADD_MENU_ITEM_MEDIA:
      return assign(state, {
        media: assign(state.media, {
          files: push(
            state.media.files,
            ...map(action.media, (file) => assign(initialMediaFileState, file))
          ),
          nextId: add(state.media.nextId, length(action.media)),
          available: sub(state.media.available, length(action.media))
        })
      })
    case REMOVE_MENU_ITEM_MEDIA:
      return assign(state, {
        media: assign(state.media, {
          files: filter(state.media.files, ({ id: fileId }) => {
            return isNotEqual(fileId, action.id)
          }),
          available: increment(state.media.available)
        })
      })
    case FETCH_ADD_MENU_ITEM_MEDIA:
      return assign(state, {
        media: assign(state.media, {
          files: map(state.media.files, (file) => {
            return isEqual(file.id, action.file.id)
              ? assign(file, { isFetching: true })
              : file
          })
        })
      })
    case ADD_MENU_ITEM_MEDIA_SUCCESS:
      return assign(state, {
        media: assign(state.media, {
          files: map(state.media.files, (file) => {
            return isEqual(file.id, action.id)
              ? assign(file, {
                  id: action.newId,
                  isFetching: false,
                  isSuccess: true
                })
              : file
          })
        })
      })
    case ADD_MENU_ITEM_MEDIA_FAILURE:
      return assign(state, {
        media: assign(state.media, {
          files: map(state.media.files, (file) => {
            return isEqual(file.id, action.id)
              ? assign(file, {
                  isFetching: false,
                  isFailure: true,
                  error: assign(file.error, action.error)
                })
              : file
          })
        })
      })
    case FETCH_REMOVE_MENU_ITEM_MEDIA:
      return assign(state, {
        media: assign(state.media, {
          files: map(state.media.files, (file) => {
            return isEqual(file.id, action.fileId)
              ? assign(file, {
                  isFetching: true,
                  isSuccess: false
                })
              : file
          })
        })
      })
    case REMOVE_MENU_ITEM_MEDIA_SUCCESS:
      return assign(state, {
        media: assign(state.media, {
          files: filter(state.media.files, (file) => {
            return isNotEqual(file.id, action.fileId)
          })
        })
      })
    case REMOVE_MENU_ITEM_MEDIA_FAILURE:
      return assign(state, {
        media: assign(state.media, {
          files: map(state.media.files, (file) => {
            return isEqual(file.id, action.fileId)
              ? assign(file, {
                  isFetching: false,
                  isFailure: true
                })
              : file
          })
        })
      })
    case SET_EDIT:
      return assign(state, {
        editItem: assign(state.editItem, {
          isEdit: true
        })
      })
    case SET_EDIT_ITEM:
      return assign(state, {
        editItem: assign(state.editItem, {
          item: assign(
            state.editItem.item,
            assign(action.item, { [PRICE]: toPrice(action.item.price, false) })
          )
        })
      })
    case FETCH_EDIT_MENU_ITEM:
      return assign(state, {
        editItem: assign(state.editItem, {
          isFetching: true,
          isSuccess: false,
          error: {
            errors: {},
            message: '',
            code: null
          }
        })
      })
    case EDIT_MENU_ITEM_SUCCESS:
      return assign(state, {
        editItem: assign(state.editItem, {
          isFetching: false,
          isSuccess: true
        })
      })
    case EDIT_MENU_ITEM_FAILURE:
      return assign(state, {
        editItem: assign(state.editItem, {
          isFetching: false,
          error: action.error
        })
      })
    case OPEN_REMOVE_MODAL:
      return assign(state, {
        removeModal: assign(state.removeModal, {
          isOpen: true,
          itemId: action.itemId,
          itemName: action.itemName,
          categoryId: action.categoryId
        })
      })
    case CLOSE_REMOVE_MODAL:
      return assign(state, {
        removeModal: assign(state.removeModal, {
          isOpen: false
        })
      })
    case CLEAR_REMOVE_MODAL:
      return assign(state, {
        removeModal: initialState.removeModal
      })
    case FETCH_REMOVE_ITEM:
      return assign(state, {
        removeModal: assign(state.removeModal, {
          isFetching: true
        })
      })
    case REMOVE_ITEM_SUCCESS:
      return assign(state, {
        removeModal: assign(state.removeModal, {
          isFetching: false,
          isSuccess: true,
          isOpen: false
        })
      })
    case FETCH_GET_MENU_ITEM:
      return assign(state, {
        getItem: assign(state.getItem, {
          isFetching: true
        })
      })
    case GET_MENU_ITEM_SUCCESS:
      return assign(state, {
        getItem: assign(state.getItem, {
          isFetching: false
        })
      })
    case CLEAR_MENU_ITEM:
      return initialState
    default:
      return state
  }
}
