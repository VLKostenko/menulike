import {
  CLEAR_UPDATE_PLACE,
  FETCH_UPDATE_PLACE,
  SET_PLACE_DATA,
  UPDATE_PLACE_FAILURE,
  UPDATE_PLACE_SUCCESS,
  FETCH_PLACE_SEND_QR,
  PLACE_SEND_QR_SUCCESS,
  PLACE_SEND_QR_FAILURE,
  CLEAR_SEND_QR,
  CLEAR_PLACE_DATA,
  UPDATE_MENU_CATEGORIES_ITEM,
  ADD_MENU_CATEGORIES_ITEM,
  ADD_MENU_CATEGORIES,
  ADD_MENU_CATEGORIES_ITEM_MEDIA,
  REMOVE_MENU_CATEGORIES_ITEM_MEDIA,
  FETCH_TOGGLE_ACTIVITY,
  TOGGLE_ACTIVITY_SUCCESS,
  TOGGLE_ACTIVITY_FAILURE,
  UPDATE_PLACE_DATA,
  UPDATE_CATEGORIES_ORDER,
  UPDATE_MENU_ITEMS_ORDER
} from '../actions/place'
import {
  assign,
  filter,
  find,
  isEqual,
  isNotEqual,
  map,
  push
} from 'utils/common'
import {
  ADDRESS,
  CITY,
  COUNTRY,
  CURRENCY,
  CURRENCY_SYMBOL,
  DESCRIPTION,
  EMAIL,
  ID,
  MEDIAS,
  MENU_CATEGORIES,
  MENU_ITEMS,
  PHONE_CODE,
  PHONE_NUMBER,
  PLACE_NAME,
  PUBLIC_ID_URL,
  PUBLIC_URL,
  QR_CODE_URL,
  SLUG
} from 'constants/forms'
import { getMenuItem } from 'utils/menuView'
import { REMOVE_ITEM_SUCCESS } from '../actions/menuItem'
import {
  EDIT_CATEGORY_SUCCESS,
  REMOVE_CATEGORY_SUCCESS
} from '../actions/categories'

const initialMenuItemState = {
  toggleActivity: {
    isFetching: false,
    isSuccess: false,
    isFailure: false
  }
}

const initialState = {
  data: {
    [ID]: '',
    [PLACE_NAME]: '',
    [SLUG]: '',
    [EMAIL]: '',
    [COUNTRY]: '',
    [CITY]: '',
    [ADDRESS]: '',
    [PHONE_CODE]: '',
    [PHONE_NUMBER]: '',
    [CURRENCY]: '',
    [CURRENCY_SYMBOL]: '',
    [DESCRIPTION]: '',
    [PUBLIC_ID_URL]: '',
    [PUBLIC_URL]: '',
    [QR_CODE_URL]: '',
    [MENU_CATEGORIES]: []
  },
  updatePlace: {
    isFetching: false,
    isSuccess: false,
    error: {
      errors: {},
      message: '',
      code: null
    }
  },
  placeQr: {
    isFetching: false,
    isSuccess: false,
    error: {
      errors: {},
      message: '',
      code: null
    }
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PLACE_DATA:
      return assign(state, {
        data: assign(state.data, action.data, {
          [MENU_CATEGORIES]: map(action.data[MENU_CATEGORIES], (category) => {
            return assign(category, {
              [MENU_ITEMS]: map(category[MENU_ITEMS], (item) => {
                return assign(initialMenuItemState, item)
              })
            })
          })
        })
      })
    case UPDATE_PLACE_DATA:
      return assign(state, {
        data: assign(state.data, action.data)
      })
    case CLEAR_PLACE_DATA:
      return assign(state, {
        data: initialState.data
      })
    case FETCH_PLACE_SEND_QR:
      return assign(state, {
        placeQr: assign(state.placeQr, {
          isFetching: true,
          isSuccess: false,
          error: {
            errors: {},
            message: '',
            code: null
          }
        })
      })
    case PLACE_SEND_QR_SUCCESS:
      return assign(state, {
        placeQr: assign(state.placeQr, {
          isFetching: false,
          isSuccess: true
        })
      })
    case PLACE_SEND_QR_FAILURE:
      return assign(state, {
        placeQr: assign(state.placeQr, {
          isFetching: false,
          error: action.error
        })
      })
    case CLEAR_SEND_QR:
      return assign(state, {
        placeQr: initialState.placeQr
      })
    case FETCH_UPDATE_PLACE:
      return assign(state, {
        updatePlace: assign(state.updatePlace, {
          isFetching: true
        })
      })
    case UPDATE_PLACE_SUCCESS:
      return assign(state, {
        updatePlace: assign(state.updatePlace, {
          isFetching: false,
          isSuccess: true,
          error: {
            errors: {},
            message: '',
            code: null
          }
        })
      })
    case UPDATE_PLACE_FAILURE:
      return assign(state, {
        updatePlace: assign(state.updatePlace, {
          isFetching: false,
          isSuccess: false,
          error: action.error
        })
      })
    case CLEAR_UPDATE_PLACE:
      return assign(state, {
        updatePlace: initialState.updatePlace
      })
    case UPDATE_MENU_CATEGORIES_ITEM:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.prevCategoryId)
              ? isEqual(action.prevCategoryId, action.nextCategoryId)
                ? assign(category, {
                    // map items and update if id is equal
                    [MENU_ITEMS]: map(category[MENU_ITEMS], (item) => {
                      return isEqual(item.id, action.item.id)
                        ? assign(item, action.item)
                        : item
                    })
                  })
                : assign(category, {
                    // filter items and remove if id is equal
                    [MENU_ITEMS]: filter(category[MENU_ITEMS], (item) => {
                      return isNotEqual(item.id, action.item.id)
                    })
                  })
              : isEqual(category.id, action.nextCategoryId)
              ? assign(category, {
                  // push item into items
                  [MENU_ITEMS]: push(
                    category[MENU_ITEMS],
                    assign(
                      getMenuItem(
                        state.data[MENU_CATEGORIES],
                        action.prevCategoryId,
                        action.item.id
                      ),
                      action.item
                    )
                  )
                })
              : category // return category as it is
          })
        })
      })
    case ADD_MENU_CATEGORIES_ITEM:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, {
                  [MENU_ITEMS]: push(
                    category[MENU_ITEMS],
                    assign(action.item, { [MEDIAS]: [] })
                  )
                })
              : category
          })
        })
      })
    case ADD_MENU_CATEGORIES:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: push(
            state.data[MENU_CATEGORIES],
            assign(action.category, { [MENU_ITEMS]: [] })
          )
        })
      })
    case ADD_MENU_CATEGORIES_ITEM_MEDIA:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, {
                  [MENU_ITEMS]: map(category[MENU_ITEMS], (item) => {
                    return isEqual(item.id, action.itemId)
                      ? assign(item, {
                          [MEDIAS]: push(item[MEDIAS], action.media)
                        })
                      : item
                  })
                })
              : category
          })
        })
      })
    case REMOVE_MENU_CATEGORIES_ITEM_MEDIA:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, {
                  [MENU_ITEMS]: map(category[MENU_ITEMS], (item) => {
                    return isEqual(item.id, action.itemId)
                      ? assign(item, {
                          [MEDIAS]: filter(item[MEDIAS], (media) => {
                            return isNotEqual(media.id, action.mediaId)
                          })
                        })
                      : item
                  })
                })
              : category
          })
        })
      })
    case FETCH_TOGGLE_ACTIVITY:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, {
                  [MENU_ITEMS]: map(category[MENU_ITEMS], (item) => {
                    return isEqual(item.id, action.itemId)
                      ? assign(item, {
                          toggleActivity: assign(item.toggleActivity, {
                            isFetching: true,
                            isSuccess: false,
                            isFailure: false
                          })
                        })
                      : item
                  })
                })
              : category
          })
        })
      })
    case TOGGLE_ACTIVITY_SUCCESS:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, {
                  [MENU_ITEMS]: map(category[MENU_ITEMS], (item) => {
                    return isEqual(item.id, action.itemId)
                      ? assign(item, action.item, {
                          toggleActivity: assign(item.toggleActivity, {
                            isFetching: false,
                            isSuccess: true
                          })
                        })
                      : item
                  })
                })
              : category
          })
        })
      })
    case TOGGLE_ACTIVITY_FAILURE:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, {
                  [MENU_ITEMS]: map(category[MENU_ITEMS], (item) => {
                    return isEqual(item.id, action.itemId)
                      ? assign(item, action.item, {
                          toggleActivity: assign(item.toggleActivity, {
                            isFetching: false,
                            isFailure: true
                          })
                        })
                      : item
                  })
                })
              : category
          })
        })
      })
    case REMOVE_ITEM_SUCCESS:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, {
                  [MENU_ITEMS]: filter(category[MENU_ITEMS], (item) => {
                    return isNotEqual(item.id, action.itemId)
                  })
                })
              : category
          })
        })
      })
    case REMOVE_CATEGORY_SUCCESS:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: filter(state.data[MENU_CATEGORIES], (category) => {
            return isNotEqual(category.id, action.categoryId)
          })
        })
      })
    case EDIT_CATEGORY_SUCCESS:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, action.data)
              : category
          })
        })
      })
    case UPDATE_CATEGORIES_ORDER:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return assign(
              category,
              find(action.categories, (newCategory) => {
                return isEqual(newCategory.id, category.id)
              })
            )
          })
        })
      })
    case UPDATE_MENU_ITEMS_ORDER:
      return assign(state, {
        data: assign(state.data, {
          [MENU_CATEGORIES]: map(state.data[MENU_CATEGORIES], (category) => {
            return isEqual(category.id, action.categoryId)
              ? assign(category, {
                  [MENU_ITEMS]: map(category[MENU_ITEMS], (item) => {
                    return assign(
                      item,
                      find(action.menuItems, (newItem) => {
                        return isEqual(newItem.id, item.id)
                      })
                    )
                  })
                })
              : category
          })
        })
      })
    default:
      return state
  }
}
