import {
  FETCH_SUBSCRIPTION_PLANS,
  SUBSCRIPTION_PLANS_FAILURE,
  SUBSCRIPTION_PLANS_SUCCESS
} from '../actions/subscription'
import { assign } from 'utils/common'

const initialState = {
  plans: [],
  features: [],
  fetchPlans: {
    isFetching: false,
    isSuccess: false,
    isFailure: false
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_SUBSCRIPTION_PLANS:
      return assign(state, {
        fetchPlans: assign(state.fetchPlans, {
          isFetching: true
        })
      })
    case SUBSCRIPTION_PLANS_SUCCESS:
      return assign(state, {
        fetchPlans: assign(state.fetchPlans, {
          isFetching: false,
          isSuccess: true
        }),
        plans: action.data,
        features: action.features
      })
    case SUBSCRIPTION_PLANS_FAILURE:
      return assign(state, {
        fetchPlans: assign(state.fetchPlans, {
          isFetching: false,
          isFailure: true
        })
      })
    default:
      return state
  }
}
