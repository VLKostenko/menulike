import {
  FETCH_CREATE_CATEGORIES,
  CREATE_CATEGORIES_SUCCESS,
  CREATE_CATEGORIES_FAILURE,
  CATEGORIES_FAILURE,
  CATEGORIES_SUCCESS,
  FETCH_CATEGORIES,
  CLEAR_CREATE_CATEGORIES,
  OPEN_REMOVE_MODAL,
  CLOSE_REMOVE_MODAL,
  CLEAR_REMOVE_MODAL,
  FETCH_REMOVE_CATEGORY,
  REMOVE_CATEGORY_SUCCESS,
  SET_EDIT_CATEGORY_ACTIVE,
  CLEAR_EDIT_CATEGORY,
  TRIGGER_EDIT_SAVE,
  EDIT_CATEGORY_SUCCESS,
  FETCH_EDIT_CATEGORY,
  SET_REORDER_ACTIVE,
  CLEAR_REORDER,
  FETCH_CATEGORIES_REORDER,
  CATEGORIES_REORDER_SUCCESS,
  CATEGORIES_REORDER_FAILURE,
  SET_CATEGORY_EDIT_ITEMS,
  CLEAR_CATEGORY_EDIT_ITEMS,
  SET_EDIT_ITEMS_REORDER_ACTIVE,
  CLEAR_EDIT_ITEMS_REORDER,
  FETCH_EDIT_ITEMS_REORDER,
  EDIT_ITEMS_REORDER_SUCCESS,
  EDIT_ITEMS_REORDER_FAILURE
} from '../actions/categories'
import { assign, push } from 'utils/common'
import { ID, NAME } from 'constants/forms'

const initialState = {
  items: [],
  getCategories: {
    isFetching: false,
    isSuccess: false,
    error: {
      errors: {},
      message: '',
      code: null
    }
  },
  createCategories: {
    isFetching: false,
    isSuccess: false,
    createdCategory: {
      [ID]: '',
      [NAME]: ''
    },
    error: {
      errors: {},
      message: '',
      code: null
    }
  },
  removeModal: {
    isOpen: false,
    categoryId: null,
    categoryName: '',
    isFetching: false,
    isSuccess: false,
    isFailure: false
  },
  editCategory: {
    isActive: false,
    categoryId: null,
    isFetching: false,
    isSuccess: false,
    isFailure: false,
    isSaveClicked: false
  },
  reorder: {
    isActive: false,
    isFetching: false,
    isSuccess: false,
    isFailure: false
  },
  editItems: {
    categoryId: null
  },
  editItemsReorder: {
    isActive: false,
    isFetching: false,
    isSuccess: false,
    isFailure: false
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case FETCH_CATEGORIES:
      return assign(state, {
        getCategories: assign(state.getCategories, {
          isFetching: true
        })
      })
    case CATEGORIES_SUCCESS:
      return assign(state, {
        getCategories: assign(state.getCategories, {
          isFetching: false,
          isSuccess: true
        }),
        items: action.data
      })
    case CATEGORIES_FAILURE:
      return assign(state, {
        getCategories: assign(state.getCategories, {
          isFetching: false,
          error: action.error
        })
      })
    case FETCH_CREATE_CATEGORIES:
      return assign(state, {
        createCategories: assign(state.createCategories, {
          isFetching: true
        })
      })
    case CREATE_CATEGORIES_SUCCESS:
      return assign(state, {
        createCategories: assign(state.createCategories, {
          isFetching: false,
          isSuccess: true,
          createdCategory: action.data
        }),
        items: push(state.items, action.data)
      })
    case CREATE_CATEGORIES_FAILURE:
      return assign(state, {
        createCategories: assign(state.createCategories, {
          isFetching: false,
          error: action.error
        })
      })
    case CLEAR_CREATE_CATEGORIES:
      return assign(state, {
        createCategories: initialState.createCategories
      })
    case OPEN_REMOVE_MODAL:
      return assign(state, {
        removeModal: assign(state.removeModal, {
          isOpen: true,
          categoryId: action.categoryId,
          categoryName: action.categoryName
        })
      })
    case CLOSE_REMOVE_MODAL:
      return assign(state, {
        removeModal: assign(state.removeModal, {
          isOpen: false
        })
      })
    case CLEAR_REMOVE_MODAL:
      return assign(state, {
        removeModal: initialState.removeModal
      })
    case FETCH_REMOVE_CATEGORY:
      return assign(state, {
        removeModal: assign(state.removeModal, {
          isFetching: true
        })
      })
    case REMOVE_CATEGORY_SUCCESS:
      return assign(state, {
        removeModal: assign(state.removeModal, {
          isFetching: false,
          isSuccess: true,
          isOpen: false
        })
      })
    case SET_EDIT_CATEGORY_ACTIVE:
      return assign(state, {
        editCategory: assign(state.editCategory, {
          isActive: true,
          categoryId: action.categoryId
        })
      })
    case CLEAR_EDIT_CATEGORY:
      return assign(state, {
        editCategory: initialState.editCategory
      })
    case TRIGGER_EDIT_SAVE:
      return assign(state, {
        editCategory: assign(state.editCategory, {
          isSaveClicked: true
        })
      })
    case FETCH_EDIT_CATEGORY:
      return assign(state, {
        editCategory: assign(state.editCategory, {
          isFetching: true
        })
      })
    case EDIT_CATEGORY_SUCCESS:
      return assign(state, {
        editCategory: assign(state.editCategory, {
          isFetching: false,
          isSuccess: true,
          isActive: false,
          categoryId: null,
          isSaveClicked: false
        })
      })
    case SET_REORDER_ACTIVE:
      return assign(state, {
        reorder: assign(state.reorder, {
          isActive: true
        })
      })
    case CLEAR_REORDER:
      return assign(state, {
        reorder: initialState.reorder
      })
    case FETCH_CATEGORIES_REORDER:
      return assign(state, {
        reorder: assign(state.reorder, {
          isFetching: true
        })
      })
    case CATEGORIES_REORDER_SUCCESS:
      return assign(state, {
        reorder: assign(state.reorder, {
          isFetching: false,
          isSuccess: true
        })
      })
    case CATEGORIES_REORDER_FAILURE:
      return assign(state, {
        reorder: assign(state.reorder, {
          isFetching: false,
          isFailure: true
        })
      })
    case SET_CATEGORY_EDIT_ITEMS:
      return assign(state, {
        editItems: assign(state.editItems, {
          categoryId: action.categoryId
        })
      })
    case CLEAR_CATEGORY_EDIT_ITEMS:
      return assign(state, {
        editItems: initialState.editItems
      })
    case SET_EDIT_ITEMS_REORDER_ACTIVE:
      return assign(state, {
        editItemsReorder: assign(state.editItemsReorder, {
          isActive: true
        })
      })
    case CLEAR_EDIT_ITEMS_REORDER:
      return assign(state, {
        editItemsReorder: initialState.editItemsReorder
      })
    case FETCH_EDIT_ITEMS_REORDER:
      return assign(state, {
        editItemsReorder: assign(state.editItemsReorder, {
          isFetching: true
        })
      })
    case EDIT_ITEMS_REORDER_SUCCESS:
      return assign(state, {
        editItemsReorder: assign(state.editItemsReorder, {
          isFetching: false,
          isSuccess: true
        })
      })
    case EDIT_ITEMS_REORDER_FAILURE:
      return assign(state, {
        editItemsReorder: assign(state.editItemsReorder, {
          isFetching: false,
          isFailure: true
        })
      })
    default:
      return state
  }
}
