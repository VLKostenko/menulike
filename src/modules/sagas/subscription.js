import { call, put, takeEvery, all } from 'redux-saga/effects'
import {
  FETCH_SUBSCRIPTION_PLANS,
  subscriptionPlansFailure,
  subscriptionPlansSuccess
} from '../actions/subscription'
import { getSubscriptionsPlansService } from 'services/subscriptions'
import { parseFeatures } from 'utils/subscription'

function* subscriptionWatcher() {
  yield all([takeEvery(FETCH_SUBSCRIPTION_PLANS, getSubscriptionPlansWorker)])
}

function* getSubscriptionPlansWorker() {
  try {
    const { data } = yield call(getSubscriptionsPlansService)
    yield put(subscriptionPlansSuccess(data, parseFeatures(data)))
  } catch (e) {
    yield put(subscriptionPlansFailure(e))
  }
}

export default subscriptionWatcher
