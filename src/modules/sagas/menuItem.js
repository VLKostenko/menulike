import { call, put, takeEvery, all } from 'redux-saga/effects'
import {
  addMenuItemMedia,
  addMenuItemMediaFailure,
  addMenuItemMediaSuccess,
  editMenuItemFailure,
  editMenuItemSuccess,
  FETCH_ADD_MENU_ITEM_MEDIA,
  FETCH_CREATE_MENU_ITEM,
  FETCH_EDIT_MENU_ITEM,
  FETCH_GET_MENU_ITEM,
  FETCH_REMOVE_ITEM,
  FETCH_REMOVE_MENU_ITEM_MEDIA,
  fetchAddMenuItemMedia,
  getMenuItemFailure,
  getMenuItemSuccess,
  removeItemFailure,
  removeItemSuccess,
  removeMenuItemMediaFailure,
  removeMenuItemMediaSuccess,
  setEdit,
  setEditMenuItem
} from '../actions/menuItem'
import {
  createMenuItemService,
  editMenuItemService,
  getMenuItemByIdService,
  removeMenuItemService,
  toggleActivityService
} from 'services/menuItem'
import {
  createMenuItemFailure,
  createMenuItemSuccess
} from '../actions/menuItem'
import { createPostMenuItemMediaBody } from 'utils/api'
import { map, pickByTruth } from 'utils/common'
import {
  addMenuItemMediaService,
  removeMenuItemMediaService
} from 'services/menuItemMedia'
import { createEditMenuItem, getMenuCategoryId } from 'utils/menuItem'
import {
  addMenuCategoriesItem,
  addMenuCategoriesItemMedia,
  FETCH_TOGGLE_ACTIVITY,
  removeMenuCategoriesItemMedia,
  toggleActivityFailure,
  toggleActivitySuccess,
  updateMenuCategoriesItem
} from '../actions/place'
import { createMediaObject } from 'utils/menuItem'

function* menuItemWatcher() {
  yield all([
    takeEvery(FETCH_CREATE_MENU_ITEM, createMenuItemWorker),
    takeEvery(FETCH_ADD_MENU_ITEM_MEDIA, addMenuItemMediaWorker),
    takeEvery(FETCH_EDIT_MENU_ITEM, editMenuItemWorker),
    takeEvery(FETCH_REMOVE_MENU_ITEM_MEDIA, removeMenuItemMediaWorker),
    takeEvery(FETCH_TOGGLE_ACTIVITY, toggleActivityWorker),
    takeEvery(FETCH_REMOVE_ITEM, removeMenuItemWorker),
    takeEvery(FETCH_GET_MENU_ITEM, getMenuItemWorker)
  ])
}

function* createMenuItemWorker({ data: requestData, files }) {
  try {
    const { data } = yield call(createMenuItemService, requestData)
    yield put(createMenuItemSuccess())

    const { id } = data

    const categoryId = getMenuCategoryId(data)

    yield put(addMenuCategoriesItem(data, categoryId))

    yield put(setEditMenuItem(pickByTruth(createEditMenuItem(data))))
    yield all(
      map(files, (file) => {
        return put(fetchAddMenuItemMedia(id, file, categoryId))
      })
    )
  } catch (e) {
    yield put(createMenuItemFailure(e))
  }
}

function* addMenuItemMediaWorker({ itemId, file, categoryId }) {
  const { id, ...requestData } = file
  try {
    const { data } = yield call(
      addMenuItemMediaService,
      itemId,
      createPostMenuItemMediaBody(requestData)
    )

    const { id: newId } = data

    yield put(addMenuItemMediaSuccess(id, newId))
    yield put(addMenuCategoriesItemMedia(data, itemId, categoryId))
  } catch (e) {
    yield put(setEdit())
    yield put(addMenuItemMediaFailure(e, id))
  }
}

function* removeMenuItemMediaWorker({ fileId, itemId, categoryId }) {
  try {
    yield call(removeMenuItemMediaService, itemId, fileId)
    yield put(removeMenuItemMediaSuccess(fileId))
    yield put(removeMenuCategoriesItemMedia(fileId, itemId, categoryId))
  } catch (e) {
    yield put(removeMenuItemMediaFailure(fileId))
  }
}

function* editMenuItemWorker({ itemId, data: requestData, prevCategoryId }) {
  try {
    const { data } = yield call(editMenuItemService, itemId, requestData)
    yield put(editMenuItemSuccess())

    const nextCategoryId = getMenuCategoryId(data)

    yield put(updateMenuCategoriesItem(data, prevCategoryId, nextCategoryId))
  } catch (e) {
    yield put(editMenuItemFailure(e))
  }
}

function* toggleActivityWorker({ itemId, categoryId, value }) {
  try {
    const { data } = yield call(toggleActivityService, itemId, value)
    yield put(toggleActivitySuccess(data, itemId, categoryId))
  } catch (e) {
    yield put(toggleActivityFailure(e, itemId, categoryId))
  }
}

function* removeMenuItemWorker({ itemId, categoryId }) {
  try {
    yield call(removeMenuItemService, itemId)
    yield put(removeItemSuccess(itemId, categoryId))
  } catch (e) {
    yield put(removeItemFailure(e))
  }
}

function* getMenuItemWorker({ itemId }) {
  try {
    const { data } = yield call(getMenuItemByIdService, itemId)
    const { medias, ...item } = data
    yield put(getMenuItemSuccess())
    yield put(setEdit())
    yield put(setEditMenuItem(pickByTruth(item)))
    yield put(
      addMenuItemMedia(
        map(medias, ({ url, id }) => createMediaObject({}, url, id, true))
      )
    )
  } catch (e) {
    yield put(getMenuItemFailure(e))
  }
}

export default menuItemWatcher
