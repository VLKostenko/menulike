import { all, put, call, takeEvery } from 'redux-saga/effects'
import getSymbol from 'currency-symbol-map'
import {
  FETCH_UPDATE_PLACE,
  updatePlaceFailure,
  updatePlaceSuccess,
  placeSendQrSuccess,
  placeSendQrFailure,
  FETCH_PLACE_SEND_QR,
  updatePlaceData
} from '../actions/place'
import { updatePlaceService, postPlaceQREmail } from 'services/place'
import { CURRENCY_SYMBOL } from 'constants/forms'
import { assign } from 'utils/common'
import { replaceNull } from 'utils/forms'

function* placeWatcher() {
  yield all([
    takeEvery(FETCH_UPDATE_PLACE, updatePlaceWorker),
    takeEvery(FETCH_PLACE_SEND_QR, getPlaceQRWorker)
  ])
}

function* updatePlaceWorker({ data: requestData }) {
  try {
    const { data } = yield call(updatePlaceService, requestData)

    const { currency } = data

    yield put(updatePlaceSuccess())
    yield put(
      updatePlaceData(
        assign(replaceNull(data), { [CURRENCY_SYMBOL]: getSymbol(currency) })
      )
    )
  } catch (e) {
    yield put(updatePlaceFailure(e))
  }
}

function* getPlaceQRWorker() {
  try {
    yield call(postPlaceQREmail)
    yield put(placeSendQrSuccess())
  } catch (e) {
    yield put(placeSendQrFailure(e))
  }
}

export default placeWatcher
