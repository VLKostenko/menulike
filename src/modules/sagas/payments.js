import { call, all, put, takeEvery } from 'redux-saga/effects'
import { getPaymentInfoService } from 'services/payments'
import {
  FETCH_PAYMENTS_INFO,
  paymentsInfoFailure,
  paymentsInfoSuccess
} from '../actions/payments'

function* paymentsWatcher() {
  yield all([takeEvery(FETCH_PAYMENTS_INFO, fetchPaymentsInfoWorker)])
}

function* fetchPaymentsInfoWorker() {
  try {
    const {
      default_payment_method: defaultPaymentMethod,
      generic_trial: genericTrial,
      subscription
    } = yield call(getPaymentInfoService)
    yield put(
      paymentsInfoSuccess({ defaultPaymentMethod, genericTrial, subscription })
    )
  } catch (e) {
    yield put(paymentsInfoFailure(e))
  }
}

export default paymentsWatcher
