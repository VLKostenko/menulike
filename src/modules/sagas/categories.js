import { call, put, takeEvery, all } from 'redux-saga/effects'
import {
  createCategoriesService,
  editCategoryService,
  editItemsOrderService,
  getCategoriesService,
  orderCategoriesService,
  removeCategoryService
} from 'services/categories'
import {
  categoriesFailure,
  categoriesReorderFailure,
  categoriesReorderSuccess,
  categoriesSuccess,
  createCategoryFailure,
  createCategorySuccess,
  editCategoryFailure,
  editCategorySuccess,
  editItemsReorderFailure,
  editItemsReorderSuccess,
  FETCH_CATEGORIES,
  FETCH_CATEGORIES_REORDER,
  FETCH_CREATE_CATEGORIES,
  FETCH_EDIT_CATEGORY,
  FETCH_EDIT_ITEMS_REORDER,
  FETCH_REMOVE_CATEGORY,
  removeCategoryFailure,
  removeCategorySuccess
} from '../actions/categories'
import {
  addMenuCategories,
  updateCategoriesOrder,
  updateMenuItemsOrder
} from '../actions/place'

function* categoriesWatcher() {
  yield all([
    takeEvery(FETCH_CATEGORIES, getCategoriesWorker),
    takeEvery(FETCH_CREATE_CATEGORIES, createCategoriesWorker),
    takeEvery(FETCH_REMOVE_CATEGORY, removeCategoryWorker),
    takeEvery(FETCH_EDIT_CATEGORY, editCategoryWorker),
    takeEvery(FETCH_CATEGORIES_REORDER, categoriesReorderWorker),
    takeEvery(FETCH_EDIT_ITEMS_REORDER, editItemsReorderWorker)
  ])
}

function* getCategoriesWorker() {
  try {
    const { data } = yield call(getCategoriesService)
    yield put(categoriesSuccess(data))
  } catch (e) {
    yield put(categoriesFailure(e))
  }
}

function* createCategoriesWorker({ data: requestData }) {
  try {
    const { data } = yield call(createCategoriesService, requestData)
    yield put(createCategorySuccess(data))
    yield put(addMenuCategories(data))
  } catch (e) {
    yield put(createCategoryFailure(e))
  }
}

function* removeCategoryWorker({ categoryId }) {
  try {
    yield call(removeCategoryService, categoryId)
    yield put(removeCategorySuccess(categoryId))
  } catch (e) {
    yield put(removeCategoryFailure(e))
  }
}

function* editCategoryWorker({ categoryId, data }) {
  try {
    const { data: responseData } = yield call(
      editCategoryService,
      categoryId,
      data
    )
    yield put(editCategorySuccess(categoryId, responseData))
  } catch (e) {
    yield put(editCategoryFailure(categoryId, e))
  }
}

function* categoriesReorderWorker({ list }) {
  try {
    const data = yield call(orderCategoriesService, list)
    yield put(categoriesReorderSuccess())
    yield put(updateCategoriesOrder(data))
  } catch (e) {
    yield put(categoriesReorderFailure(e))
  }
}

function* editItemsReorderWorker({ categoryId, list }) {
  try {
    const data = yield call(editItemsOrderService, list)
    yield put(editItemsReorderSuccess())
    yield put(updateMenuItemsOrder(categoryId, data))
  } catch (e) {
    yield put(editItemsReorderFailure(e))
  }
}

export default categoriesWatcher
