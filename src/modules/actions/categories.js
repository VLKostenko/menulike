export const FETCH_CATEGORIES = 'CATEGORIES/FETCH_CATEGORIES'
export const CATEGORIES_SUCCESS = 'CATEGORIES/CATEGORIES_SUCCESS'
export const CATEGORIES_FAILURE = 'CATEGORIES/CATEGORIES_FAILURE'
export const FETCH_CREATE_CATEGORIES = 'CATEGORIES/FETCH_CREATE_CATEGORIES'
export const CREATE_CATEGORIES_SUCCESS = 'CATEGORIES/CREATE_CATEGORIES_SUCCESS'
export const CREATE_CATEGORIES_FAILURE = 'CATEGORIES/CREATE_CATEGORIES_FAILURE'
export const CLEAR_CREATE_CATEGORIES = 'CATEGORIES/CLEAR_CREATE_CATEGORIES'
export const OPEN_REMOVE_MODAL = 'CATEGORIES/OPEN_REMOVE_MODAL'
export const CLOSE_REMOVE_MODAL = 'CATEGORIES/CLOSE_REMOVE_MODAL'
export const CLEAR_REMOVE_MODAL = 'CATEGORIES/CLEAR_REMOVE_MODAL'
export const FETCH_REMOVE_CATEGORY = 'CATEGORIES/FETCH_REMOVE_CATEGORY'
export const REMOVE_CATEGORY_SUCCESS = 'CATEGORIES/REMOVE_CATEGORY_SUCCESS'
export const REMOVE_CATEGORY_FAILURE = 'CATEGORIES/REMOVE_CATEGORY_FAILURE'
export const SET_EDIT_CATEGORY_ACTIVE = 'CATEGORIES/SET_EDIT_CATEGORY_ACTIVE'
export const CLEAR_EDIT_CATEGORY = 'CATEGORIES/CLEAR_EDIT_CATEGORY'
export const FETCH_EDIT_CATEGORY = 'CATEGORIES/FETCH_EDIT_CATEGORY'
export const EDIT_CATEGORY_SUCCESS = 'CATEGORIES/EDIT_CATEGORY_SUCCESS'
export const EDIT_CATEGORY_FAILURE = 'CATEGORIES/EDIT_CATEGORY_FAILURE'
export const TRIGGER_EDIT_SAVE = 'CATEGORIES/TRIGGER_EDIT_SAVE'
export const SET_REORDER_ACTIVE = 'CATEGORIES/SET_REORDER_ACTIVE'
export const CLEAR_REORDER = 'CATEGORIES/CLEAR_REORDER'
export const FETCH_CATEGORIES_REORDER = 'CATEGORIES/FETCH_CATEGORIES_REORDER'
export const CATEGORIES_REORDER_SUCCESS =
  'CATEGORIES/CATEGORIES_REORDER_SUCCESS'
export const CATEGORIES_REORDER_FAILURE =
  'CATEGORIES/CATEGORIES_REORDER_FAILURE'
export const SET_CATEGORY_EDIT_ITEMS = 'CATEGORIES/SET_CATEGORY_EDIT_ITEMS'
export const CLEAR_CATEGORY_EDIT_ITEMS = 'CATEGORIES/CLEAR_CATEGORY_EDIT_ITEMS'
export const SET_EDIT_ITEMS_REORDER_ACTIVE =
  'CATEGORIES/SET_EDIT_ITEMS_REORDER_ACTIVE'
export const CLEAR_EDIT_ITEMS_REORDER = 'CATEGORIES/CLEAR_EDIT_ITEMS_REORDER'
export const FETCH_EDIT_ITEMS_REORDER = 'CATEGORIES/FETCH_EDIT_ITEMS_REORDER'
export const EDIT_ITEMS_REORDER_SUCCESS =
  'CATEGORIES/EDIT_ITEMS_REORDER_SUCCESS'
export const EDIT_ITEMS_REORDER_FAILURE =
  'CATEGORIES/EDIT_ITEMS_REORDER_FAILURE'

export function fetchCategories() {
  return {
    type: FETCH_CATEGORIES
  }
}

export function categoriesSuccess(data) {
  return {
    type: CATEGORIES_SUCCESS,
    data
  }
}

export function categoriesFailure(error) {
  return {
    type: CATEGORIES_FAILURE,
    error
  }
}

export function fetchCreateCategory(data) {
  return {
    type: FETCH_CREATE_CATEGORIES,
    data
  }
}

export function createCategorySuccess(data) {
  return {
    type: CREATE_CATEGORIES_SUCCESS,
    data
  }
}

export function createCategoryFailure(error) {
  return {
    type: CREATE_CATEGORIES_FAILURE,
    error
  }
}

export function clearCreateCategories() {
  return {
    type: CLEAR_CREATE_CATEGORIES
  }
}

export function openRemoveModal(categoryId, categoryName) {
  return {
    type: OPEN_REMOVE_MODAL,
    categoryId,
    categoryName
  }
}

export function closeRemoveModal() {
  return {
    type: CLOSE_REMOVE_MODAL
  }
}

export function clearRemoveModal() {
  return {
    type: CLEAR_REMOVE_MODAL
  }
}

export function fetchRemoveCategory(categoryId) {
  return {
    type: FETCH_REMOVE_CATEGORY,
    categoryId
  }
}

export function removeCategorySuccess(categoryId) {
  return {
    type: REMOVE_CATEGORY_SUCCESS,
    categoryId
  }
}

export function removeCategoryFailure(error) {
  return {
    type: REMOVE_CATEGORY_FAILURE,
    error
  }
}

export function setEditCategoryActive(categoryId) {
  return {
    type: SET_EDIT_CATEGORY_ACTIVE,
    categoryId
  }
}

export function clearEditCategory() {
  return {
    type: CLEAR_EDIT_CATEGORY
  }
}

export function fetchEditCategory(categoryId, data) {
  return {
    type: FETCH_EDIT_CATEGORY,
    categoryId,
    data
  }
}

export function editCategorySuccess(categoryId, data) {
  return {
    type: EDIT_CATEGORY_SUCCESS,
    categoryId,
    data
  }
}

export function editCategoryFailure(categoryId, error) {
  return {
    type: EDIT_CATEGORY_FAILURE,
    categoryId,
    error
  }
}

export function triggerEditSave() {
  return {
    type: TRIGGER_EDIT_SAVE
  }
}

export function setReorderActive() {
  return {
    type: SET_REORDER_ACTIVE
  }
}

export function clearReorder() {
  return {
    type: CLEAR_REORDER
  }
}

export function fetchCategoriesReorder(list) {
  return {
    type: FETCH_CATEGORIES_REORDER,
    list
  }
}

export function categoriesReorderSuccess() {
  return {
    type: CATEGORIES_REORDER_SUCCESS
  }
}

export function categoriesReorderFailure(error) {
  return {
    type: CATEGORIES_REORDER_FAILURE,
    error
  }
}

export function setCategoryEditItems(categoryId) {
  return {
    type: SET_CATEGORY_EDIT_ITEMS,
    categoryId
  }
}

export function clearCategoryEditItems() {
  return {
    type: CLEAR_CATEGORY_EDIT_ITEMS
  }
}

export function setEditItemsReorderActive() {
  return {
    type: SET_EDIT_ITEMS_REORDER_ACTIVE
  }
}

export function clearEditItemsReorder() {
  return {
    type: CLEAR_EDIT_ITEMS_REORDER
  }
}

export function fetchEditItemsReorder(categoryId, list) {
  return {
    type: FETCH_EDIT_ITEMS_REORDER,
    categoryId,
    list
  }
}

export function editItemsReorderSuccess() {
  return {
    type: EDIT_ITEMS_REORDER_SUCCESS
  }
}

export function editItemsReorderFailure(error) {
  return {
    type: EDIT_ITEMS_REORDER_FAILURE,
    error
  }
}
