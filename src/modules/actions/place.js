export const SET_PLACE_DATA = 'PLACE/SET_PLACE_DATA'
export const UPDATE_PLACE_DATA = 'PLACE/UPDATE_PLACE_DATA'
export const CLEAR_PLACE_DATA = 'PLACE/CLEAR_PLACE_DATA'
export const FETCH_PLACE_SEND_QR = 'PLACE/FETCH_PLACE_SEND_QR'
export const PLACE_SEND_QR_SUCCESS = 'PLACE/PLACE_SEND_QR_SUCCESS'
export const PLACE_SEND_QR_FAILURE = 'PLACE/PLACE_QR_FAILURE'
export const CLEAR_SEND_QR = 'PLACE/CLEAR_SEND_QR'
export const FETCH_UPDATE_PLACE = 'PLACE/FETCH_UPDATE_PLACE'
export const UPDATE_PLACE_SUCCESS = 'PLACE/UPDATE_PLACE_SUCCESS'
export const UPDATE_PLACE_FAILURE = 'PLACE/UPDATE_PLACE_FAILURE'
export const CLEAR_UPDATE_PLACE = 'PLACE/CLEAR_UPDATE_PLACE'
export const UPDATE_MENU_CATEGORIES_ITEM = 'PLACE/UPDATE_MENU_CATEGORIES_ITEM'
export const ADD_MENU_CATEGORIES_ITEM = 'PLACE/ADD_MENU_CATEGORIES_ITEM'
export const ADD_MENU_CATEGORIES = 'PLACE/ADD_MENU_CATEGORIES'
export const ADD_MENU_CATEGORIES_ITEM_MEDIA =
  'PLACE/ADD_MENU_CATEGORIES_ITEM_MEDIA'
export const REMOVE_MENU_CATEGORIES_ITEM_MEDIA =
  'REMOVE_MENU_CATEGORIES_ITEM_MEDIA'
export const FETCH_TOGGLE_ACTIVITY = 'PLACE/FETCH_TOGGLE_ACTIVITY'
export const TOGGLE_ACTIVITY_SUCCESS = 'PLACE/TOGGLE_ACTIVITY_SUCCESS'
export const TOGGLE_ACTIVITY_FAILURE = 'PLACE/TOGGLE_ACTIVITY_FAILURE'
export const UPDATE_CATEGORIES_ORDER = 'PLACE/UPDATE_CATEGORIES_ORDER'
export const UPDATE_MENU_ITEMS_ORDER = 'PLACE/UPDATE_MENU_ITEMS_ORDER'

export function setPlaceData(data) {
  return {
    type: SET_PLACE_DATA,
    data
  }
}

export function updatePlaceData(data) {
  return {
    type: UPDATE_PLACE_DATA,
    data
  }
}

export function clearPlaceData() {
  return {
    type: CLEAR_PLACE_DATA
  }
}

export function fetchPlaceSendQr(data) {
  return {
    type: FETCH_PLACE_SEND_QR,
    data
  }
}

export function placeSendQrSuccess(data) {
  return {
    type: PLACE_SEND_QR_SUCCESS,
    data
  }
}

export function placeSendQrFailure(error) {
  return {
    type: PLACE_SEND_QR_FAILURE,
    error
  }
}

export function clearSendQr() {
  return {
    type: CLEAR_SEND_QR
  }
}

export function fetchUpdatePlace(data) {
  return {
    type: FETCH_UPDATE_PLACE,
    data
  }
}

export function updatePlaceSuccess() {
  return {
    type: UPDATE_PLACE_SUCCESS
  }
}

export function updatePlaceFailure(error) {
  return {
    type: UPDATE_PLACE_FAILURE,
    error
  }
}

export function clearUpdatePlace() {
  return {
    type: CLEAR_UPDATE_PLACE
  }
}

export function updateMenuCategoriesItem(item, prevCategoryId, nextCategoryId) {
  return {
    type: UPDATE_MENU_CATEGORIES_ITEM,
    item,
    prevCategoryId,
    nextCategoryId
  }
}

export function addMenuCategoriesItem(item, categoryId) {
  return {
    type: ADD_MENU_CATEGORIES_ITEM,
    item,
    categoryId
  }
}

export function addMenuCategories(category) {
  return {
    type: ADD_MENU_CATEGORIES,
    category
  }
}

export function addMenuCategoriesItemMedia(media, itemId, categoryId) {
  return {
    type: ADD_MENU_CATEGORIES_ITEM_MEDIA,
    media,
    itemId,
    categoryId
  }
}

export function removeMenuCategoriesItemMedia(mediaId, itemId, categoryId) {
  return {
    type: REMOVE_MENU_CATEGORIES_ITEM_MEDIA,
    mediaId,
    itemId,
    categoryId
  }
}

export function fetchToggleActivity(itemId, categoryId, value) {
  return {
    type: FETCH_TOGGLE_ACTIVITY,
    itemId,
    categoryId,
    value
  }
}

export function toggleActivitySuccess(item, itemId, categoryId) {
  return {
    type: TOGGLE_ACTIVITY_SUCCESS,
    item,
    itemId,
    categoryId
  }
}

export function toggleActivityFailure(error, itemId, categoryId) {
  return {
    type: TOGGLE_ACTIVITY_FAILURE,
    error,
    itemId,
    categoryId
  }
}

export function updateCategoriesOrder(categories) {
  return {
    type: UPDATE_CATEGORIES_ORDER,
    categories
  }
}

export function updateMenuItemsOrder(categoryId, menuItems) {
  return {
    type: UPDATE_MENU_ITEMS_ORDER,
    categoryId,
    menuItems
  }
}
