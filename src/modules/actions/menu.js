export const SET_SELECTED_CATEGORY_ID = 'MENU/SET_SELECTED_CATEGORY_ID'

export function setSelectedCategoryId(id) {
  return {
    type: SET_SELECTED_CATEGORY_ID,
    id
  }
}
