export const FETCH_CREATE_MENU_ITEM = 'MENU_ITEM/FETCH_CREATE_MENU_ITEM'
export const CREATE_MENU_ITEM_SUCCESS = 'MENU_ITEM/CREATE_MENU_ITEM_SUCCESS'
export const CREATE_MENU_ITEM_FAILURE = 'MENU_ITEM/CREATE_MENU_ITEM_FAILURE'
export const ADD_MENU_ITEM_MEDIA = 'MENU_ITEM/ADD_MENU_ITEM_MEDIA'
export const REMOVE_MENU_ITEM_MEDIA = 'MENU_ITEM/REMOVE_MENU_ITEM_MEDIA'
export const FETCH_ADD_MENU_ITEM_MEDIA = 'MENU_ITEM/FETCH_ADD_MENU_ITEM_MEDIA'
export const ADD_MENU_ITEM_MEDIA_SUCCESS =
  'MENU_ITEM/ADD_MENU_ITEM_MEDIA_SUCCESS'
export const ADD_MENU_ITEM_MEDIA_FAILURE =
  'MENU_ITEM/ADD_MENU_ITEM_MEDIA_FAILURE'
export const CLEAR_MENU_ITEM = 'MENU_ITEM/CLEAR_MENU_ITEM'
export const SET_EDIT = 'MENU_ITEM/SET_EDIT'
export const SET_EDIT_ITEM = 'MENU_ITEM/SET_EDIT_ITEM'
export const FETCH_EDIT_MENU_ITEM = 'MENU_ITEM/FETCH_EDIT_MENU_ITEM'
export const EDIT_MENU_ITEM_SUCCESS = 'MENU_ITEM/EDIT_MENU_ITEM_SUCCESS'
export const EDIT_MENU_ITEM_FAILURE = 'MENU_ITEM/EDIT_MENU_ITEM_FAILURE'
export const FETCH_REMOVE_MENU_ITEM_MEDIA =
  'MENU_ITEM/FETCH_REMOVE_MENU_ITEM_MEDIA'
export const REMOVE_MENU_ITEM_MEDIA_SUCCESS =
  'MENU_ITEM/REMOVE_MENU_ITEM_MEDIA_SUCCESS'
export const REMOVE_MENU_ITEM_MEDIA_FAILURE =
  'MENU_ITEM/REMOVE_MENU_ITEM_MEDIA_FAILURE'
export const OPEN_REMOVE_MODAL = 'MENU_ITEM/OPEN_REMOVE_MODAL'
export const CLOSE_REMOVE_MODAL = 'MENU_ITEM/CLOSE_REMOVE_MODAL'
export const CLEAR_REMOVE_MODAL = 'MENU_ITEM/CLEAR_REMOVE_MODAL'
export const FETCH_REMOVE_ITEM = 'MENU_ITEM/FETCH_REMOVE_ITEM'
export const REMOVE_ITEM_SUCCESS = 'MENU_ITEM/REMOVE_ITEM_SUCCESS'
export const REMOVE_ITEM_FAILURE = 'MENU_ITEM/REMOVE_ITEM_FAILURE'
export const FETCH_GET_MENU_ITEM = 'MENU_ITEM/FETCH_GET_MENU_ITEM'
export const GET_MENU_ITEM_SUCCESS = 'MENU_ITEM/GET_MENU_ITEM_SUCCESS'
export const GET_MENU_ITEM_FAILURE = 'MENU_ITEM/GET_MENU_ITEM_FAILURE'

export function fetchCreateMenuItem(data, files) {
  return {
    type: FETCH_CREATE_MENU_ITEM,
    data,
    files
  }
}

export function createMenuItemSuccess() {
  return {
    type: CREATE_MENU_ITEM_SUCCESS
  }
}

export function createMenuItemFailure(error) {
  return {
    type: CREATE_MENU_ITEM_FAILURE,
    error
  }
}

export function addMenuItemMedia(media) {
  return {
    type: ADD_MENU_ITEM_MEDIA,
    media
  }
}

export function removeMenuItemMedia(id) {
  return {
    type: REMOVE_MENU_ITEM_MEDIA,
    id
  }
}

export function fetchAddMenuItemMedia(itemId, file, categoryId) {
  return {
    type: FETCH_ADD_MENU_ITEM_MEDIA,
    itemId,
    file,
    categoryId
  }
}

export function addMenuItemMediaSuccess(id, newId) {
  return {
    type: ADD_MENU_ITEM_MEDIA_SUCCESS,
    id,
    newId
  }
}

export function addMenuItemMediaFailure(error, id) {
  return {
    type: ADD_MENU_ITEM_MEDIA_FAILURE,
    error,
    id
  }
}

export function clearMenuItem() {
  return {
    type: CLEAR_MENU_ITEM
  }
}

export function setEdit() {
  return {
    type: SET_EDIT
  }
}

export function setEditMenuItem(item) {
  return {
    type: SET_EDIT_ITEM,
    item
  }
}

export function fetchEditMenuItem(itemId, data, prevCategoryId) {
  return {
    type: FETCH_EDIT_MENU_ITEM,
    itemId,
    data,
    prevCategoryId
  }
}

export function editMenuItemSuccess() {
  return {
    type: EDIT_MENU_ITEM_SUCCESS
  }
}

export function editMenuItemFailure(error) {
  return {
    type: EDIT_MENU_ITEM_FAILURE,
    error
  }
}

export function fetchRemoveMenuItemMedia(fileId, itemId, categoryId) {
  return {
    type: FETCH_REMOVE_MENU_ITEM_MEDIA,
    fileId,
    itemId,
    categoryId
  }
}

export function removeMenuItemMediaSuccess(fileId) {
  return {
    type: REMOVE_MENU_ITEM_MEDIA_SUCCESS,
    fileId
  }
}

export function removeMenuItemMediaFailure(fileId) {
  return {
    type: REMOVE_MENU_ITEM_MEDIA_FAILURE,
    fileId
  }
}

export function openRemoveModal(itemId, itemName, categoryId) {
  return {
    type: OPEN_REMOVE_MODAL,
    itemId,
    itemName,
    categoryId
  }
}

export function closeRemoveModal() {
  return {
    type: CLOSE_REMOVE_MODAL
  }
}

export function clearRemoveModal() {
  return {
    type: CLEAR_REMOVE_MODAL
  }
}

export function fetchRemoveItem(itemId, categoryId) {
  return {
    type: FETCH_REMOVE_ITEM,
    itemId,
    categoryId
  }
}

export function removeItemSuccess(itemId, categoryId) {
  return {
    type: REMOVE_ITEM_SUCCESS,
    itemId,
    categoryId
  }
}

export function removeItemFailure(error) {
  return {
    type: REMOVE_ITEM_FAILURE,
    error
  }
}

export function fetchGetMenuItem(itemId) {
  return {
    type: FETCH_GET_MENU_ITEM,
    itemId
  }
}

export function getMenuItemSuccess() {
  return {
    type: GET_MENU_ITEM_SUCCESS
  }
}

export function getMenuItemFailure(error) {
  return {
    type: GET_MENU_ITEM_FAILURE,
    error
  }
}
