export const SET_IS_LOGGED = 'APP/SET_IS_LOGGED'
export const FETCH_GEOLOCATION = 'APP/FETCH_GEOLOCATION'
export const GEOLOCATION_SUCCESS = 'APP/GEOLOCATION_SUCCESS'
export const GEOLOCATION_FAILURE = 'APP/GEOLOCATION_FAILURE'
export const SET_IS_VISITOR = 'APP/SET_IS_VISITOR'
export const FETCH_LANGUAGE = 'APP/FETCH_LANGUAGE'
export const LANGUAGE_SUCCESS = 'APP/LANGUAGE_SUCCESS'

export function setLogged(value) {
  return {
    type: SET_IS_LOGGED,
    value
  }
}

export function fetchGeolocation() {
  return {
    type: FETCH_GEOLOCATION
  }
}

export function geolocationSuccess(data) {
  return {
    type: GEOLOCATION_SUCCESS,
    data
  }
}

export function geolocationFailure(error) {
  return {
    type: GEOLOCATION_FAILURE,
    error
  }
}

export function setIsVisitor() {
  return {
    type: SET_IS_VISITOR
  }
}

export function fetchLanguage() {
  return {
    type: FETCH_LANGUAGE
  }
}

export function languageSuccess() {
  return {
    type: LANGUAGE_SUCCESS
  }
}
