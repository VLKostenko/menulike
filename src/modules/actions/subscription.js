export const FETCH_SUBSCRIPTION_PLANS = 'SUBSCRIPTION/FETCH_SUBSCRIPTION_PLANS'
export const SUBSCRIPTION_PLANS_SUCCESS =
  'SUBSCRIPTION/SUBSCRIPTION_PLANS_SUCCESS'
export const SUBSCRIPTION_PLANS_FAILURE =
  'SUBSCRIPTION/SUBSCRIPTION_PLANS_FAILURE'

export function fetchSubscriptionPlans() {
  return {
    type: FETCH_SUBSCRIPTION_PLANS
  }
}

export function subscriptionPlansSuccess(data, features) {
  return {
    type: SUBSCRIPTION_PLANS_SUCCESS,
    data,
    features
  }
}

export function subscriptionPlansFailure(error) {
  return {
    type: SUBSCRIPTION_PLANS_FAILURE,
    error
  }
}
