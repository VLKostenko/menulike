export const FETCH_PAYMENTS_INFO = 'PAYMENTS/FETCH_PAYMENTS_INFO'
export const PAYMENTS_INFO_SUCCESS = 'PAYMENTS/PAYMENTS_INFO_SUCCESS'
export const PAYMENTS_INFO_FAILURE = 'PAYMENTS/PAYMENTS_INFO_FAILURE'

export function fetchPaymentsInfo() {
  return {
    type: FETCH_PAYMENTS_INFO
  }
}

export function paymentsInfoSuccess(data) {
  return {
    type: PAYMENTS_INFO_SUCCESS,
    data
  }
}

export function paymentsInfoFailure(error) {
  return {
    type: PAYMENTS_INFO_FAILURE,
    error
  }
}
