export function fetchSubscriptionPlansSelector({ subscription }) {
  return subscription.fetchPlans
}

export function subscriptionPlansSelector({ subscription }) {
  return subscription.plans
}

export function subscriptionFeaturesSelector({ subscription }) {
  return subscription.features
}
