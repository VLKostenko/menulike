export function fetchPaymentsInfoSelector({ payments }) {
  return payments.fetchInfo
}

export function paymentsInfoSelector({ payments }) {
  return payments.info
}
