export function isLoggedSelector({ app }) {
  return app.isLogged
}

export function geolocationSelector({ app }) {
  return app.geolocation
}

export function isVisitorSelector({ app }) {
  return app.isVisitor
}

export function languageSelector({ app }) {
  return app.language
}
