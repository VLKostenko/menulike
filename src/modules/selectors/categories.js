export function getCategoriesSelector({ categories }) {
  return categories.getCategories
}

export function categoriesItemsSelector({ categories }) {
  return categories.items
}

export function createCategoriesSelector({ categories }) {
  return categories.createCategories
}

export function createCategoriesErrorSelector({ categories }) {
  return categories.createCategories.error
}

export function removeModalSelector({ categories }) {
  return categories.removeModal
}

export function editCategorySelector({ categories }) {
  return categories.editCategory
}

export function reorderCategoriesSelector({ categories }) {
  return categories.reorder
}

export function categoryEditItemsSelector({ categories }) {
  return categories.editItems
}

export function categoryIdEditItemsSelector({ categories }) {
  return categories.editItems.categoryId
}

export function editItemsReorderSelector({ categories }) {
  return categories.editItemsReorder
}
