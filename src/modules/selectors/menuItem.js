export function menuItemSelector({ menuItem }) {
  return menuItem
}

export function menuItemMediaSelector({ menuItem }) {
  return menuItem.media
}

export function createMenuItemSelector({ menuItem }) {
  return menuItem.createItem
}

export function createMenuItemErrorSelector({ menuItem }) {
  return menuItem.createItem.error
}

export function editMenuItemSelector({ menuItem }) {
  return menuItem.editItem
}

export function editMenuItemItemSelector({ menuItem }) {
  return menuItem.editItem.item
}

export function removeModalSelector({ menuItem }) {
  return menuItem.removeModal
}
