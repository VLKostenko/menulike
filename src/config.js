export const API_URL = process.env.REACT_APP_API_URL
export const GA_TRACKING_ID = 'UA-172650604-1'
export const DEFAULT_TITLE = 'Menulike.com - Digital Menu'
export const DEFAULT_DESCRIPTION =
  'Menulike.com - Digital Menu For all restaurants and all customers'
